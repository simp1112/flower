﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientCon3002158488.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRec2694504884.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes3468069089.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes3595945587.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTyp859870085.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context4285182719.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAl954949548.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1820731088.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgor1654661965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClie3823629320.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3166895267.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro1946181211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProt173216930.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSign389653629.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig1282301050.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC3722381418.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2290372928.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityPr155967584.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerCon3823737132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Validatio1782558132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3918817353.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1404755603.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh3044322977.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamB934199321.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1610391122.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherS396038680.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient2311449551.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcepti583514812.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerS403340211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream4089752859.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3938752374.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2540099417.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2537917473.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4150496570.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3939745042.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2939633944.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3808761250.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_905088469.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2187269356.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1869592958.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1289300668.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_530021076.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2172608670.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest572679901.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certificat989458295.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3318447433.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3721235490.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe1663566523.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1892466092.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2038352954.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2540610921.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778804.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2896841275.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2866209745.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778802.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1703410334.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1957337331.h"
#include "System_U3CModuleU3E3783534214.h"
#include "System_Locale4255929014.h"
#include "System_System_MonoTODOAttribute3487514019.h"
#include "System_System_MonoNotSupportedAttribute2197455930.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "System_System_ComponentModel_Design_DesignerTransa3880697857.h"
#include "System_System_ComponentModel_Design_RuntimeLicense1397748562.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr599180064.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "System_System_ComponentModel_AsyncOperation1185541675.h"
#include "System_System_ComponentModel_AsyncOperationManager3553158318.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_BackgroundWorker4230068110.h"
#include "System_System_ComponentModel_BackgroundWorker_Proce308732489.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (ClientContext_t3002158488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1000[2] = 
{
	ClientContext_t3002158488::get_offset_of_sslStream_30(),
	ClientContext_t3002158488::get_offset_of_clientHelloProtocol_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (ClientRecordProtocol_t2694504884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (ClientSessionInfo_t3468069089), -1, sizeof(ClientSessionInfo_t3468069089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1002[6] = 
{
	ClientSessionInfo_t3468069089_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t3468069089::get_offset_of_disposed_1(),
	ClientSessionInfo_t3468069089::get_offset_of_validuntil_2(),
	ClientSessionInfo_t3468069089::get_offset_of_host_3(),
	ClientSessionInfo_t3468069089::get_offset_of_sid_4(),
	ClientSessionInfo_t3468069089::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (ClientSessionCache_t3595945587), -1, sizeof(ClientSessionCache_t3595945587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1003[2] = 
{
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (ContentType_t859870085)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1004[5] = 
{
	ContentType_t859870085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (Context_t4285182719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1005[30] = 
{
	Context_t4285182719::get_offset_of_securityProtocol_0(),
	Context_t4285182719::get_offset_of_sessionId_1(),
	Context_t4285182719::get_offset_of_compressionMethod_2(),
	Context_t4285182719::get_offset_of_serverSettings_3(),
	Context_t4285182719::get_offset_of_clientSettings_4(),
	Context_t4285182719::get_offset_of_current_5(),
	Context_t4285182719::get_offset_of_negotiating_6(),
	Context_t4285182719::get_offset_of_read_7(),
	Context_t4285182719::get_offset_of_write_8(),
	Context_t4285182719::get_offset_of_supportedCiphers_9(),
	Context_t4285182719::get_offset_of_lastHandshakeMsg_10(),
	Context_t4285182719::get_offset_of_handshakeState_11(),
	Context_t4285182719::get_offset_of_abbreviatedHandshake_12(),
	Context_t4285182719::get_offset_of_receivedConnectionEnd_13(),
	Context_t4285182719::get_offset_of_sentConnectionEnd_14(),
	Context_t4285182719::get_offset_of_protocolNegotiated_15(),
	Context_t4285182719::get_offset_of_writeSequenceNumber_16(),
	Context_t4285182719::get_offset_of_readSequenceNumber_17(),
	Context_t4285182719::get_offset_of_clientRandom_18(),
	Context_t4285182719::get_offset_of_serverRandom_19(),
	Context_t4285182719::get_offset_of_randomCS_20(),
	Context_t4285182719::get_offset_of_randomSC_21(),
	Context_t4285182719::get_offset_of_masterSecret_22(),
	Context_t4285182719::get_offset_of_clientWriteKey_23(),
	Context_t4285182719::get_offset_of_serverWriteKey_24(),
	Context_t4285182719::get_offset_of_clientWriteIV_25(),
	Context_t4285182719::get_offset_of_serverWriteIV_26(),
	Context_t4285182719::get_offset_of_handshakeMessages_27(),
	Context_t4285182719::get_offset_of_random_28(),
	Context_t4285182719::get_offset_of_recordProtocol_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (ExchangeAlgorithmType_t954949548)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1006[6] = 
{
	ExchangeAlgorithmType_t954949548::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (HandshakeState_t1820731088)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1007[4] = 
{
	HandshakeState_t1820731088::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (HashAlgorithmType_t1654661965)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1008[4] = 
{
	HashAlgorithmType_t1654661965::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (HttpsClientStream_t3823629320), -1, sizeof(HttpsClientStream_t3823629320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1009[4] = 
{
	HttpsClientStream_t3823629320::get_offset_of__request_20(),
	HttpsClientStream_t3823629320::get_offset_of__status_21(),
	HttpsClientStream_t3823629320_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_22(),
	HttpsClientStream_t3823629320_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (RecordProtocol_t3166895267), -1, sizeof(RecordProtocol_t3166895267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1010[3] = 
{
	RecordProtocol_t3166895267_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3166895267::get_offset_of_innerStream_1(),
	RecordProtocol_t3166895267::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (ReceiveRecordAsyncResult_t1946181211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1011[9] = 
{
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { sizeof (SendRecordAsyncResult_t173216930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1012[7] = 
{
	SendRecordAsyncResult_t173216930::get_offset_of_locker_0(),
	SendRecordAsyncResult_t173216930::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t173216930::get_offset_of__userState_2(),
	SendRecordAsyncResult_t173216930::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t173216930::get_offset_of_handle_4(),
	SendRecordAsyncResult_t173216930::get_offset_of__message_5(),
	SendRecordAsyncResult_t173216930::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (RSASslSignatureDeformatter_t389653629), -1, sizeof(RSASslSignatureDeformatter_t389653629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1013[3] = 
{
	RSASslSignatureDeformatter_t389653629::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t389653629::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t389653629_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { sizeof (RSASslSignatureFormatter_t1282301050), -1, sizeof(RSASslSignatureFormatter_t1282301050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1014[3] = 
{
	RSASslSignatureFormatter_t1282301050::get_offset_of_key_0(),
	RSASslSignatureFormatter_t1282301050::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t1282301050_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (SecurityCompressionType_t3722381418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1015[3] = 
{
	SecurityCompressionType_t3722381418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (SecurityParameters_t2290372928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1016[3] = 
{
	SecurityParameters_t2290372928::get_offset_of_cipher_0(),
	SecurityParameters_t2290372928::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2290372928::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (SecurityProtocolType_t155967584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1017[5] = 
{
	SecurityProtocolType_t155967584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (ServerContext_t3823737132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (ValidationResult_t1782558132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1019[2] = 
{
	ValidationResult_t1782558132::get_offset_of_trusted_0(),
	ValidationResult_t1782558132::get_offset_of_error_code_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (SslClientStream_t3918817353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1020[4] = 
{
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation_16(),
	SslClientStream_t3918817353::get_offset_of_ClientCertSelection_17(),
	SslClientStream_t3918817353::get_offset_of_PrivateKeySelection_18(),
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (SslCipherSuite_t1404755603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1021[3] = 
{
	SslCipherSuite_t1404755603::get_offset_of_pad1_21(),
	SslCipherSuite_t1404755603::get_offset_of_pad2_22(),
	SslCipherSuite_t1404755603::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (SslHandshakeHash_t3044322977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1022[8] = 
{
	SslHandshakeHash_t3044322977::get_offset_of_md5_4(),
	SslHandshakeHash_t3044322977::get_offset_of_sha_5(),
	SslHandshakeHash_t3044322977::get_offset_of_hashing_6(),
	SslHandshakeHash_t3044322977::get_offset_of_secret_7(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (SslStreamBase_t934199321), -1, sizeof(SslStreamBase_t934199321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1023[15] = 
{
	0,
	SslStreamBase_t934199321_StaticFields::get_offset_of_record_processing_2(),
	SslStreamBase_t934199321::get_offset_of_innerStream_3(),
	SslStreamBase_t934199321::get_offset_of_inputBuffer_4(),
	SslStreamBase_t934199321::get_offset_of_context_5(),
	SslStreamBase_t934199321::get_offset_of_protocol_6(),
	SslStreamBase_t934199321::get_offset_of_ownsStream_7(),
	SslStreamBase_t934199321::get_offset_of_disposed_8(),
	SslStreamBase_t934199321::get_offset_of_checkCertRevocationStatus_9(),
	SslStreamBase_t934199321::get_offset_of_negotiate_10(),
	SslStreamBase_t934199321::get_offset_of_read_11(),
	SslStreamBase_t934199321::get_offset_of_write_12(),
	SslStreamBase_t934199321::get_offset_of_negotiationComplete_13(),
	SslStreamBase_t934199321::get_offset_of_recbuf_14(),
	SslStreamBase_t934199321::get_offset_of_recordStream_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (InternalAsyncResult_t1610391122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1024[12] = 
{
	InternalAsyncResult_t1610391122::get_offset_of_locker_0(),
	InternalAsyncResult_t1610391122::get_offset_of__userCallback_1(),
	InternalAsyncResult_t1610391122::get_offset_of__userState_2(),
	InternalAsyncResult_t1610391122::get_offset_of__asyncException_3(),
	InternalAsyncResult_t1610391122::get_offset_of_handle_4(),
	InternalAsyncResult_t1610391122::get_offset_of_completed_5(),
	InternalAsyncResult_t1610391122::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t1610391122::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t1610391122::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t1610391122::get_offset_of__buffer_9(),
	InternalAsyncResult_t1610391122::get_offset_of__offset_10(),
	InternalAsyncResult_t1610391122::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (TlsCipherSuite_t396038680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1025[2] = 
{
	TlsCipherSuite_t396038680::get_offset_of_header_21(),
	TlsCipherSuite_t396038680::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (TlsClientSettings_t2311449551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1026[4] = 
{
	TlsClientSettings_t2311449551::get_offset_of_targetHost_0(),
	TlsClientSettings_t2311449551::get_offset_of_certificates_1(),
	TlsClientSettings_t2311449551::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2311449551::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (TlsException_t583514812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1027[1] = 
{
	TlsException_t583514812::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (TlsServerSettings_t403340211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1028[8] = 
{
	TlsServerSettings_t403340211::get_offset_of_certificates_0(),
	TlsServerSettings_t403340211::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t403340211::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t403340211::get_offset_of_signedParams_3(),
	TlsServerSettings_t403340211::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t403340211::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t403340211::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t403340211::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (TlsStream_t4089752859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1029[4] = 
{
	TlsStream_t4089752859::get_offset_of_canRead_1(),
	TlsStream_t4089752859::get_offset_of_canWrite_2(),
	TlsStream_t4089752859::get_offset_of_buffer_3(),
	TlsStream_t4089752859::get_offset_of_temp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (ClientCertificateType_t4001384466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1030[6] = 
{
	ClientCertificateType_t4001384466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (HandshakeMessage_t3938752374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1031[4] = 
{
	HandshakeMessage_t3938752374::get_offset_of_context_5(),
	HandshakeMessage_t3938752374::get_offset_of_handshakeType_6(),
	HandshakeMessage_t3938752374::get_offset_of_contentType_7(),
	HandshakeMessage_t3938752374::get_offset_of_cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (HandshakeType_t2540099417)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1032[12] = 
{
	HandshakeType_t2540099417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { sizeof (TlsClientCertificate_t2537917473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1033[2] = 
{
	TlsClientCertificate_t2537917473::get_offset_of_clientCertSelected_9(),
	TlsClientCertificate_t2537917473::get_offset_of_clientCert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { sizeof (TlsClientCertificateVerify_t4150496570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { sizeof (TlsClientFinished_t3939745042), -1, sizeof(TlsClientFinished_t3939745042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1035[1] = 
{
	TlsClientFinished_t3939745042_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { sizeof (TlsClientHello_t2939633944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1036[1] = 
{
	TlsClientHello_t2939633944::get_offset_of_random_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (TlsClientKeyExchange_t3808761250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (TlsServerCertificate_t905088469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1038[1] = 
{
	TlsServerCertificate_t905088469::get_offset_of_certificates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { sizeof (TlsServerCertificateRequest_t2187269356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1039[2] = 
{
	TlsServerCertificateRequest_t2187269356::get_offset_of_certificateTypes_9(),
	TlsServerCertificateRequest_t2187269356::get_offset_of_distinguisedNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (TlsServerFinished_t1869592958), -1, sizeof(TlsServerFinished_t1869592958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1040[1] = 
{
	TlsServerFinished_t1869592958_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (TlsServerHello_t1289300668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1041[4] = 
{
	TlsServerHello_t1289300668::get_offset_of_compressionMethod_9(),
	TlsServerHello_t1289300668::get_offset_of_random_10(),
	TlsServerHello_t1289300668::get_offset_of_sessionId_11(),
	TlsServerHello_t1289300668::get_offset_of_cipherSuite_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { sizeof (TlsServerHelloDone_t530021076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { sizeof (TlsServerKeyExchange_t2172608670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1043[2] = 
{
	TlsServerKeyExchange_t2172608670::get_offset_of_rsaParams_9(),
	TlsServerKeyExchange_t2172608670::get_offset_of_signedParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (PrimalityTest_t572679902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (CertificateValidationCallback_t989458295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (CertificateValidationCallback2_t3318447433), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (CertificateSelectionCallback_t3721235490), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (PrivateKeySelectionCallback_t1663566523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305138), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1049[15] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (U24ArrayTypeU243132_t1892466093)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t1892466093 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (U24ArrayTypeU24256_t2038352955)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { sizeof (U24ArrayTypeU2420_t540610922)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t540610922 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (U24ArrayTypeU2432_t3672778805)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3672778805 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (U24ArrayTypeU2448_t896841276)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t896841276 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (U24ArrayTypeU2464_t2866209746)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209746 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (U24ArrayTypeU2412_t3672778803)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778803 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (U24ArrayTypeU2416_t1703410335)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410335 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (U24ArrayTypeU244_t1957337331)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1957337331 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (U3CModuleU3E_t3783534216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (Locale_t4255929016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (MonoTODOAttribute_t3487514020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1061[1] = 
{
	MonoTODOAttribute_t3487514020::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (MonoNotSupportedAttribute_t2197455930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1063[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1064[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1065[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1066[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1067[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1068[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1069[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1070[3] = 
{
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1071[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1072[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1073[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1074[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1075[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1076[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1077[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1078[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1079[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1080[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (InstanceDescriptor_t1404033120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1081[3] = 
{
	InstanceDescriptor_t1404033120::get_offset_of_member_0(),
	InstanceDescriptor_t1404033120::get_offset_of_arguments_1(),
	InstanceDescriptor_t1404033120::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (DesignerTransaction_t3880697857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1082[2] = 
{
	DesignerTransaction_t3880697857::get_offset_of_committed_0(),
	DesignerTransaction_t3880697857::get_offset_of_canceled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { sizeof (RuntimeLicenseContext_t1397748562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1091[2] = 
{
	RuntimeLicenseContext_t1397748562::get_offset_of_extraassemblies_0(),
	RuntimeLicenseContext_t1397748562::get_offset_of_keys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (ArrayPropertyDescriptor_t599180064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1093[2] = 
{
	ArrayPropertyDescriptor_t599180064::get_offset_of_index_6(),
	ArrayPropertyDescriptor_t599180064::get_offset_of_array_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (AsyncCompletedEventArgs_t83270938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1094[3] = 
{
	AsyncCompletedEventArgs_t83270938::get_offset_of__error_1(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__cancelled_2(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__userState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (AsyncOperation_t1185541675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1095[3] = 
{
	AsyncOperation_t1185541675::get_offset_of_ctx_0(),
	AsyncOperation_t1185541675::get_offset_of_state_1(),
	AsyncOperation_t1185541675::get_offset_of_done_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (AsyncOperationManager_t3553158318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1097[2] = 
{
	AttributeCollection_t1925812292::get_offset_of_attrList_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (BackgroundWorker_t4230068110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1098[7] = 
{
	BackgroundWorker_t4230068110::get_offset_of_async_0(),
	BackgroundWorker_t4230068110::get_offset_of_cancel_pending_1(),
	BackgroundWorker_t4230068110::get_offset_of_report_progress_2(),
	BackgroundWorker_t4230068110::get_offset_of_support_cancel_3(),
	BackgroundWorker_t4230068110::get_offset_of_DoWork_4(),
	BackgroundWorker_t4230068110::get_offset_of_ProgressChanged_5(),
	BackgroundWorker_t4230068110::get_offset_of_RunWorkerCompleted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (ProcessWorkerEventHandler_t308732489), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
