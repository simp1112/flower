﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.Threading.Thread>
struct List_1_t3905650040;
// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate
struct IntegerMillisecondsDelegate_t1507343911;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SupportClass
struct  SupportClass_t3717613383  : public Il2CppObject
{
public:

public:
};

struct SupportClass_t3717613383_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Threading.Thread> ExitGames.Client.Photon.SupportClass::threadList
	List_1_t3905650040 * ___threadList_0;
	// ExitGames.Client.Photon.SupportClass/IntegerMillisecondsDelegate ExitGames.Client.Photon.SupportClass::IntegerMilliseconds
	IntegerMillisecondsDelegate_t1507343911 * ___IntegerMilliseconds_1;

public:
	inline static int32_t get_offset_of_threadList_0() { return static_cast<int32_t>(offsetof(SupportClass_t3717613383_StaticFields, ___threadList_0)); }
	inline List_1_t3905650040 * get_threadList_0() const { return ___threadList_0; }
	inline List_1_t3905650040 ** get_address_of_threadList_0() { return &___threadList_0; }
	inline void set_threadList_0(List_1_t3905650040 * value)
	{
		___threadList_0 = value;
		Il2CppCodeGenWriteBarrier(&___threadList_0, value);
	}

	inline static int32_t get_offset_of_IntegerMilliseconds_1() { return static_cast<int32_t>(offsetof(SupportClass_t3717613383_StaticFields, ___IntegerMilliseconds_1)); }
	inline IntegerMillisecondsDelegate_t1507343911 * get_IntegerMilliseconds_1() const { return ___IntegerMilliseconds_1; }
	inline IntegerMillisecondsDelegate_t1507343911 ** get_address_of_IntegerMilliseconds_1() { return &___IntegerMilliseconds_1; }
	inline void set_IntegerMilliseconds_1(IntegerMillisecondsDelegate_t1507343911 * value)
	{
		___IntegerMilliseconds_1 = value;
		Il2CppCodeGenWriteBarrier(&___IntegerMilliseconds_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
