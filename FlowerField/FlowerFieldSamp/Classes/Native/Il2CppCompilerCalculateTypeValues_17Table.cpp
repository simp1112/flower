﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "UnityEngine_UnityEngine_TooltipAttribute4278647215.h"
#include "UnityEngine_UnityEngine_SpaceAttribute952253354.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute2454598508.h"
#include "UnityEngine_UnityEngine_RangeInt2323401134.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525.h"
#include "UnityEngine_UnityEngine_StackTraceUtility1881293839.h"
#include "UnityEngine_UnityEngine_UnityException2687879050.h"
#include "UnityEngine_UnityEngine_SystemClock104337557.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType875112366.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe4226409784.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttribu689702060.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttri3403607913.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttribu14323075.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJob987733588.h"
#include "UnityEngine_UnityEngine_Collections_NativeContainer269240268.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3267933728.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1288953595.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Frame658788566.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection301283622.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3517219175.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2252784345.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2167079021.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection536719976.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2291506050.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1899782350.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Rend984453155.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Photon3Unity3D_U3CModuleU3E3783534214.h"
#include "Photon3Unity3D_Photon_SocketServer_Security_OakleyG242709843.h"
#include "Photon3Unity3D_Photon_SocketServer_Security_Diffie4202550208.h"
#include "Photon3Unity3D_Photon_SocketServer_Numeric_BigInte3529225576.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Version4146437058.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Hashtable995404622.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas3717613383.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas1507343911.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas4155706194.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas2319454133.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClass151170629.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode2706638303.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StreamBuffe3747118964.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerStateVa1383826784.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionP3211808698.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel980888449.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeer2940878176.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Plane_t3727654732)+ sizeof (Il2CppObject), sizeof(Plane_t3727654732 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[2] = 
{
	Plane_t3727654732::get_offset_of_m_Normal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Plane_t3727654732::get_offset_of_m_Distance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (PropertyAttribute_t2606999759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (TooltipAttribute_t4278647215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[1] = 
{
	TooltipAttribute_t4278647215::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (SpaceAttribute_t952253354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	SpaceAttribute_t952253354::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (RangeAttribute_t3336560921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[2] = 
{
	RangeAttribute_t3336560921::get_offset_of_min_0(),
	RangeAttribute_t3336560921::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (TextAreaAttribute_t2454598508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[2] = 
{
	TextAreaAttribute_t2454598508::get_offset_of_minLines_0(),
	TextAreaAttribute_t2454598508::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (RangeInt_t2323401134)+ sizeof (Il2CppObject), sizeof(RangeInt_t2323401134 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	RangeInt_t2323401134::get_offset_of_start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RangeInt_t2323401134::get_offset_of_length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (Ray_t2469606224)+ sizeof (Il2CppObject), sizeof(Ray_t2469606224 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1708[2] = 
{
	Ray_t2469606224::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t2469606224::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (Rect_t3681755626)+ sizeof (Il2CppObject), sizeof(Rect_t3681755626 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1709[4] = 
{
	Rect_t3681755626::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t3681755626::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t3681755626::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t3681755626::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (SelectionBaseAttribute_t936505999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (SerializePrivateVariables_t2241034664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (SerializeField_t3073427462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (PreferBinarySerialization_t2472773525), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (StackTraceUtility_t1881293839), -1, sizeof(StackTraceUtility_t1881293839_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[1] = 
{
	StackTraceUtility_t1881293839_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (UnityException_t2687879050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[2] = 
{
	0,
	UnityException_t2687879050::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (SystemClock_t104337557), -1, sizeof(SystemClock_t104337557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1717[1] = 
{
	SystemClock_t104337557_StaticFields::get_offset_of_s_Epoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (TouchScreenKeyboardType_t875112366)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[10] = 
{
	TouchScreenKeyboardType_t875112366::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (TrackedReference_t1045890189), sizeof(TrackedReference_t1045890189_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	TrackedReference_t1045890189::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1745[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (ThreadAndSerializationSafeAttribute_t4226409784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (ReadOnlyAttribute_t689702060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (ReadWriteAttribute_t3403607913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (WriteOnlyAttribute_t14323075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (DeallocateOnJobCompletionAttribute_t987733588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (NativeContainerAttribute_t269240268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (NativeContainerSupportsAtomicWriteAttribute_t3267933728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1288953595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[6] = 
{
	FrameData_t1120735295::get_offset_of_m_FrameID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_DeltaTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Weight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_EffectiveWeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_EffectiveSpeed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Flags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (Flags_t658788566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1757[3] = 
{
	Flags_t658788566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (MessageEventArgs_t301283622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[2] = 
{
	MessageEventArgs_t301283622::get_offset_of_playerId_0(),
	MessageEventArgs_t301283622::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (PlayerConnection_t3517219175), -1, sizeof(PlayerConnection_t3517219175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1765[3] = 
{
	PlayerConnection_t3517219175::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3517219175::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3517219175_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (PlayerEditorConnectionEvents_t2252784345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[3] = 
{
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (MessageEvent_t2167079021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (ConnectionChangeEvent_t536719976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (MessageTypeSubscribers_t2291506050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[3] = 
{
	MessageTypeSubscribers_t2291506050::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t2291506050::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t2291506050::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (RenderPipelineManager_t984453155), -1, sizeof(RenderPipelineManager_t984453155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1773[2] = 
{
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1777[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (OakleyGroups_t242709843), -1, sizeof(OakleyGroups_t242709843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1782[4] = 
{
	OakleyGroups_t242709843_StaticFields::get_offset_of_Generator_0(),
	OakleyGroups_t242709843_StaticFields::get_offset_of_OakleyPrime768_1(),
	OakleyGroups_t242709843_StaticFields::get_offset_of_OakleyPrime1024_2(),
	OakleyGroups_t242709843_StaticFields::get_offset_of_OakleyPrime1536_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (DiffieHellmanCryptoProvider_t4202550208), -1, sizeof(DiffieHellmanCryptoProvider_t4202550208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1784[6] = 
{
	DiffieHellmanCryptoProvider_t4202550208_StaticFields::get_offset_of_primeRoot_0(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_prime_1(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_secret_2(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_publicKey_3(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_crypto_4(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_sharedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (BigInteger_t3529225576), -1, sizeof(BigInteger_t3529225576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1785[3] = 
{
	BigInteger_t3529225576_StaticFields::get_offset_of_primesBelow2000_0(),
	BigInteger_t3529225576::get_offset_of_data_1(),
	BigInteger_t3529225576::get_offset_of_dataLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (Version_t4146437058), -1, sizeof(Version_t4146437058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1786[1] = 
{
	Version_t4146437058_StaticFields::get_offset_of_clientVersion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (Hashtable_t995404622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (SupportClass_t3717613383), -1, sizeof(SupportClass_t3717613383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1788[2] = 
{
	SupportClass_t3717613383_StaticFields::get_offset_of_threadList_0(),
	SupportClass_t3717613383_StaticFields::get_offset_of_IntegerMilliseconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (IntegerMillisecondsDelegate_t1507343911), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (ThreadSafeRandom_t4155706194), -1, sizeof(ThreadSafeRandom_t4155706194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1790[1] = 
{
	ThreadSafeRandom_t4155706194_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (U3CU3Ec__DisplayClass7_0_t2319454133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[2] = 
{
	U3CU3Ec__DisplayClass7_0_t2319454133::get_offset_of_millisecondsInterval_0(),
	U3CU3Ec__DisplayClass7_0_t2319454133::get_offset_of_myThread_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (U3CU3Ec_t151170629), -1, sizeof(U3CU3Ec_t151170629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1792[1] = 
{
	U3CU3Ec_t151170629_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (StatusCode_t2706638303)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1793[20] = 
{
	StatusCode_t2706638303::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (StreamBuffer_t3747118964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[4] = 
{
	0,
	StreamBuffer_t3747118964::get_offset_of_pos_2(),
	StreamBuffer_t3747118964::get_offset_of_len_3(),
	StreamBuffer_t3747118964::get_offset_of_buf_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (PeerStateValue_t1383826784)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1796[6] = 
{
	PeerStateValue_t1383826784::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (ConnectionProtocol_t3211808698)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1797[5] = 
{
	ConnectionProtocol_t3211808698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (DebugLevel_t980888449)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1798[6] = 
{
	DebugLevel_t980888449::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (PhotonPeer_t2940878176), -1, sizeof(PhotonPeer_t2940878176_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1799[39] = 
{
	0,
	0,
	0,
	PhotonPeer_t2940878176::get_offset_of_ClientSdkId_3(),
	PhotonPeer_t2940878176_StaticFields::get_offset_of_AsyncKeyExchange_4(),
	PhotonPeer_t2940878176::get_offset_of_clientVersion_5(),
	PhotonPeer_t2940878176::get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6(),
	PhotonPeer_t2940878176::get_offset_of_SocketImplementationConfig_7(),
	PhotonPeer_t2940878176::get_offset_of_U3CSocketImplementationU3Ek__BackingField_8(),
	PhotonPeer_t2940878176::get_offset_of_DebugOut_9(),
	PhotonPeer_t2940878176::get_offset_of_U3CListenerU3Ek__BackingField_10(),
	PhotonPeer_t2940878176::get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_11(),
	PhotonPeer_t2940878176::get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12(),
	PhotonPeer_t2940878176::get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13(),
	PhotonPeer_t2940878176::get_offset_of_trafficStatsStopwatch_14(),
	PhotonPeer_t2940878176::get_offset_of_trafficStatsEnabled_15(),
	PhotonPeer_t2940878176::get_offset_of_commandLogSize_16(),
	PhotonPeer_t2940878176::get_offset_of_U3CEnableServerTracingU3Ek__BackingField_17(),
	PhotonPeer_t2940878176::get_offset_of_quickResendAttempts_18(),
	PhotonPeer_t2940878176::get_offset_of_RhttpMinConnections_19(),
	PhotonPeer_t2940878176::get_offset_of_RhttpMaxConnections_20(),
	PhotonPeer_t2940878176::get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21(),
	PhotonPeer_t2940878176::get_offset_of_ChannelCount_22(),
	PhotonPeer_t2940878176::get_offset_of_crcEnabled_23(),
	PhotonPeer_t2940878176::get_offset_of_WarningSize_24(),
	PhotonPeer_t2940878176::get_offset_of_SentCountAllowance_25(),
	PhotonPeer_t2940878176::get_offset_of_TimePingInterval_26(),
	PhotonPeer_t2940878176::get_offset_of_DisconnectTimeout_27(),
	PhotonPeer_t2940878176::get_offset_of_U3CTransportProtocolU3Ek__BackingField_28(),
	PhotonPeer_t2940878176_StaticFields::get_offset_of_OutgoingStreamBufferSize_29(),
	PhotonPeer_t2940878176::get_offset_of_mtu_30(),
	PhotonPeer_t2940878176::get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31(),
	PhotonPeer_t2940878176::get_offset_of_peerBase_32(),
	PhotonPeer_t2940878176::get_offset_of_SendOutgoingLockObject_33(),
	PhotonPeer_t2940878176::get_offset_of_DispatchLockObject_34(),
	PhotonPeer_t2940878176::get_offset_of_EnqueueLock_35(),
	PhotonPeer_t2940878176::get_offset_of_PayloadEncryptionSecret_36(),
	PhotonPeer_t2940878176::get_offset_of_encryptor_37(),
	PhotonPeer_t2940878176::get_offset_of_decryptor_38(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
