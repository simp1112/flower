﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Photon_MonoBehaviour3914164484.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonSystem
struct  PhotonSystem_t2765200621  : public MonoBehaviour_t3914164484
{
public:
	// UnityEngine.UI.Text PhotonSystem::readyText
	Text_t356221433 * ___readyText_3;
	// UnityEngine.GameObject PhotonSystem::goInputModeButton
	GameObject_t1756533147 * ___goInputModeButton_4;
	// UnityEngine.GameObject PhotonSystem::goViewModeButton
	GameObject_t1756533147 * ___goViewModeButton_5;

public:
	inline static int32_t get_offset_of_readyText_3() { return static_cast<int32_t>(offsetof(PhotonSystem_t2765200621, ___readyText_3)); }
	inline Text_t356221433 * get_readyText_3() const { return ___readyText_3; }
	inline Text_t356221433 ** get_address_of_readyText_3() { return &___readyText_3; }
	inline void set_readyText_3(Text_t356221433 * value)
	{
		___readyText_3 = value;
		Il2CppCodeGenWriteBarrier(&___readyText_3, value);
	}

	inline static int32_t get_offset_of_goInputModeButton_4() { return static_cast<int32_t>(offsetof(PhotonSystem_t2765200621, ___goInputModeButton_4)); }
	inline GameObject_t1756533147 * get_goInputModeButton_4() const { return ___goInputModeButton_4; }
	inline GameObject_t1756533147 ** get_address_of_goInputModeButton_4() { return &___goInputModeButton_4; }
	inline void set_goInputModeButton_4(GameObject_t1756533147 * value)
	{
		___goInputModeButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___goInputModeButton_4, value);
	}

	inline static int32_t get_offset_of_goViewModeButton_5() { return static_cast<int32_t>(offsetof(PhotonSystem_t2765200621, ___goViewModeButton_5)); }
	inline GameObject_t1756533147 * get_goViewModeButton_5() const { return ___goViewModeButton_5; }
	inline GameObject_t1756533147 ** get_address_of_goViewModeButton_5() { return &___goViewModeButton_5; }
	inline void set_goViewModeButton_5(GameObject_t1756533147 * value)
	{
		___goViewModeButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___goViewModeButton_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
