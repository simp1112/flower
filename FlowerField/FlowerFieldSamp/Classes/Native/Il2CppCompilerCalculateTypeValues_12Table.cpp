﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_RefreshProperties2240171922.h"
#include "System_System_ComponentModel_RefreshPropertiesAttr2234294918.h"
#include "System_System_ComponentModel_RunWorkerCompletedEve3512304465.h"
#include "System_System_ComponentModel_SByteConverter4003686519.h"
#include "System_System_ComponentModel_SingleConverter3693313828.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"
#include "System_System_ComponentModel_TimeSpanConverter2149358279.h"
#include "System_System_ComponentModel_ToolboxItemAttribute3063187714.h"
#include "System_System_ComponentModel_ToolboxItemFilterAttr3737067842.h"
#include "System_System_ComponentModel_ToolboxItemFilterType1254240086.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "System_System_ComponentModel_TypeConverter_Standard191679357.h"
#include "System_System_ComponentModel_TypeConverter_SimplePr586934322.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3729801536.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691.h"
#include "System_System_ComponentModel_TypeDescriptor_Attrib1066710002.h"
#include "System_System_ComponentModel_TypeDescriptor_Attrib4228428768.h"
#include "System_System_ComponentModel_TypeDescriptor_Wrappe1420150273.h"
#include "System_System_ComponentModel_TypeDescriptor_Defaul2266463351.h"
#include "System_System_ComponentModel_TypeDescriptor_Defaul2227823135.h"
#include "System_System_ComponentModel_Info42715200.h"
#include "System_System_ComponentModel_ComponentInfo2742875487.h"
#include "System_System_ComponentModel_TypeInfo1029530608.h"
#include "System_System_ComponentModel_TypeListConverter2824544940.h"
#include "System_System_ComponentModel_UInt16Converter1747783369.h"
#include "System_System_ComponentModel_UInt32Converter1748821239.h"
#include "System_System_ComponentModel_UInt64Converter977523578.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3891611113.h"
#include "System_System_ComponentModel_Win32Exception1708275760.h"
#include "System_System_Diagnostics_Debug2273457373.h"
#include "System_System_Diagnostics_Stopwatch1380178105.h"
#include "System_System_Net_Security_AuthenticationLevel2424130044.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_LingerOption1165263720.h"
#include "System_System_Net_Sockets_MulticastOption2505469155.h"
#include "System_System_Net_Sockets_ProtocolType2178963134.h"
#include "System_System_Net_Sockets_SelectMode3413969319.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_Sockets_SocketError307542793.h"
#include "System_System_Net_Sockets_SocketException1618573604.h"
#include "System_System_Net_Sockets_SocketFlags2353657790.h"
#include "System_System_Net_Sockets_SocketOptionLevel1505247880.h"
#include "System_System_Net_Sockets_SocketOptionName1089121285.h"
#include "System_System_Net_Sockets_SocketShutdown3247039417.h"
#include "System_System_Net_Sockets_SocketType1143498533.h"
#include "System_System_Net_DefaultCertificatePolicy2545332216.h"
#include "System_System_Net_Dns1335526197.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_FileWebRequest1571840111.h"
#include "System_System_Net_FileWebRequestCreator1109072211.h"
#include "System_System_Net_FtpRequestCreator3711983251.h"
#include "System_System_Net_FtpWebRequest3120721823.h"
#include "System_System_Net_GlobalProxySelection2251180943.h"
#include "System_System_Net_HttpRequestCreator1416559589.h"
#include "System_System_Net_HttpVersion1276659706.h"
#include "System_System_Net_HttpWebRequest1951404513.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPHostEntry994738509.h"
#include "System_System_Net_IPv6Address2596635879.h"
#include "System_System_Net_SecurityProtocolType3099771628.h"
#include "System_System_Net_ServicePoint2765344313.h"
#include "System_System_Net_ServicePointManager745663000.h"
#include "System_System_Net_ServicePointManager_SPKey1552752485.h"
#include "System_System_Net_SocketAddress838303055.h"
#include "System_System_Net_WebHeaderCollection3028142837.h"
#include "System_System_Net_WebProxy1169192840.h"
#include "System_System_Net_Configuration_Dummy2932948314.h"
#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Security_Cryptography_X509Certificat2370524385.h"
#include "System_System_Security_Cryptography_X509Certificates_P870392.h"
#include "System_System_Security_Cryptography_X509Certificat1570828128.h"
#include "System_System_Security_Cryptography_X509Certificat2183514610.h"
#include "System_System_Security_Cryptography_X509Certificate452415348.h"
#include "System_System_Security_Cryptography_X509Certificat2005802885.h"
#include "System_System_Security_Cryptography_X509Certificat1562873317.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "System_System_Security_Cryptography_X509Certificat1108969367.h"
#include "System_System_Security_Cryptography_X509Certificat2356134957.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "System_System_Security_Cryptography_X509Certificat1208230922.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Security_Cryptography_X509Certificate528874471.h"
#include "System_System_Security_Cryptography_X509Certificat2081831987.h"
#include "System_System_Security_Cryptography_X509Certificat3304975821.h"
#include "System_System_Security_Cryptography_X509Certificat3452126517.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "System_System_Security_Cryptography_X509Certificate480677120.h"
#include "System_System_Security_Cryptography_X509Certificat2099881051.h"
#include "System_System_Security_Cryptography_X509Certificat1320896183.h"
#include "System_System_Security_Cryptography_X509Certificate650873211.h"
#include "System_System_Security_Cryptography_X509Certificat3763443773.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (RefreshProperties_t2240171922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1200[4] = 
{
	RefreshProperties_t2240171922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (RefreshPropertiesAttribute_t2234294918), -1, sizeof(RefreshPropertiesAttribute_t2234294918_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1201[4] = 
{
	RefreshPropertiesAttribute_t2234294918::get_offset_of_refresh_0(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_All_1(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_Default_2(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_Repaint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (RunWorkerCompletedEventArgs_t3512304465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1202[1] = 
{
	RunWorkerCompletedEventArgs_t3512304465::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (SByteConverter_t4003686519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (SingleConverter_t3693313828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (StringConverter_t3749524419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (TimeSpanConverter_t2149358279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (ToolboxItemAttribute_t3063187714), -1, sizeof(ToolboxItemAttribute_t3063187714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1207[5] = 
{
	0,
	ToolboxItemAttribute_t3063187714_StaticFields::get_offset_of_Default_1(),
	ToolboxItemAttribute_t3063187714_StaticFields::get_offset_of_None_2(),
	ToolboxItemAttribute_t3063187714::get_offset_of_itemType_3(),
	ToolboxItemAttribute_t3063187714::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (ToolboxItemFilterAttribute_t3737067842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1208[2] = 
{
	ToolboxItemFilterAttribute_t3737067842::get_offset_of_Filter_0(),
	ToolboxItemFilterAttribute_t3737067842::get_offset_of_ItemFilterType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (ToolboxItemFilterType_t1254240086)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1209[5] = 
{
	ToolboxItemFilterType_t1254240086::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (TypeConverter_t745995970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (StandardValuesCollection_t191679357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1211[1] = 
{
	StandardValuesCollection_t191679357::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (SimplePropertyDescriptor_t586934322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1212[2] = 
{
	SimplePropertyDescriptor_t586934322::get_offset_of_componentType_6(),
	SimplePropertyDescriptor_t586934322::get_offset_of_propertyType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (TypeConverterAttribute_t252469870), -1, sizeof(TypeConverterAttribute_t252469870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1213[2] = 
{
	TypeConverterAttribute_t252469870_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t252469870::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (TypeDescriptionProvider_t2438624375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1214[2] = 
{
	TypeDescriptionProvider_t2438624375::get_offset_of__emptyCustomTypeDescriptor_0(),
	TypeDescriptionProvider_t2438624375::get_offset_of__parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (EmptyCustomTypeDescriptor_t3729801536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (TypeDescriptor_t3595688691), -1, sizeof(TypeDescriptor_t3595688691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1216[12] = 
{
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_defaultConverters_1(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_descriptorHandler_2(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentTable_3(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeTable_4(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_editors_5(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProvidersLock_6(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProviders_7(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProvidersLock_8(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProviders_9(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_onDispose_10(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_Refreshed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (AttributeProvider_t1066710002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[1] = 
{
	AttributeProvider_t1066710002::get_offset_of_attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (AttributeTypeDescriptor_t4228428768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1218[1] = 
{
	AttributeTypeDescriptor_t4228428768::get_offset_of_attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (WrappedTypeDescriptionProvider_t1420150273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[1] = 
{
	WrappedTypeDescriptionProvider_t1420150273::get_offset_of_U3CWrappedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (DefaultTypeDescriptor_t2266463351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1220[3] = 
{
	DefaultTypeDescriptor_t2266463351::get_offset_of_owner_1(),
	DefaultTypeDescriptor_t2266463351::get_offset_of_objectType_2(),
	DefaultTypeDescriptor_t2266463351::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (DefaultTypeDescriptionProvider_t2227823135), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (Info_t42715200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1222[6] = 
{
	Info_t42715200::get_offset_of__infoType_0(),
	Info_t42715200::get_offset_of__defaultEvent_1(),
	Info_t42715200::get_offset_of__gotDefaultEvent_2(),
	Info_t42715200::get_offset_of__defaultProperty_3(),
	Info_t42715200::get_offset_of__gotDefaultProperty_4(),
	Info_t42715200::get_offset_of__attributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (ComponentInfo_t2742875487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1223[3] = 
{
	ComponentInfo_t2742875487::get_offset_of__component_6(),
	ComponentInfo_t2742875487::get_offset_of__events_7(),
	ComponentInfo_t2742875487::get_offset_of__properties_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (TypeInfo_t1029530608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1224[2] = 
{
	TypeInfo_t1029530608::get_offset_of__events_6(),
	TypeInfo_t1029530608::get_offset_of__properties_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (TypeListConverter_t2824544940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1225[1] = 
{
	TypeListConverter_t2824544940::get_offset_of_types_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (UInt16Converter_t1747783369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (UInt32Converter_t1748821239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (UInt64Converter_t977523578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (WeakObjectWrapper_t2012978780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1229[2] = 
{
	WeakObjectWrapper_t2012978780::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t2012978780::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (WeakObjectWrapperComparer_t3891611113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (Win32Exception_t1708275760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1231[1] = 
{
	Win32Exception_t1708275760::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (Debug_t2273457373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (Stopwatch_t1380178105), -1, sizeof(Stopwatch_t1380178105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1233[5] = 
{
	Stopwatch_t1380178105_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t1380178105_StaticFields::get_offset_of_IsHighResolution_1(),
	Stopwatch_t1380178105::get_offset_of_elapsed_2(),
	Stopwatch_t1380178105::get_offset_of_started_3(),
	Stopwatch_t1380178105::get_offset_of_is_running_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (AuthenticationLevel_t2424130044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1234[4] = 
{
	AuthenticationLevel_t2424130044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (SslPolicyErrors_t1928581431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1235[5] = 
{
	SslPolicyErrors_t1928581431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (AddressFamily_t303362630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1236[32] = 
{
	AddressFamily_t303362630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (LingerOption_t1165263720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1237[2] = 
{
	LingerOption_t1165263720::get_offset_of_enabled_0(),
	LingerOption_t1165263720::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (MulticastOption_t2505469155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (ProtocolType_t2178963134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1239[26] = 
{
	ProtocolType_t2178963134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (SelectMode_t3413969319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1240[4] = 
{
	SelectMode_t3413969319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (Socket_t3821512045), -1, sizeof(Socket_t3821512045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1241[22] = 
{
	Socket_t3821512045::get_offset_of_readQ_0(),
	Socket_t3821512045::get_offset_of_writeQ_1(),
	Socket_t3821512045::get_offset_of_islistening_2(),
	Socket_t3821512045::get_offset_of_MinListenPort_3(),
	Socket_t3821512045::get_offset_of_MaxListenPort_4(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t3821512045::get_offset_of_linger_timeout_7(),
	Socket_t3821512045::get_offset_of_socket_8(),
	Socket_t3821512045::get_offset_of_address_family_9(),
	Socket_t3821512045::get_offset_of_socket_type_10(),
	Socket_t3821512045::get_offset_of_protocol_type_11(),
	Socket_t3821512045::get_offset_of_blocking_12(),
	Socket_t3821512045::get_offset_of_blocking_thread_13(),
	Socket_t3821512045::get_offset_of_isbound_14(),
	Socket_t3821512045_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t3821512045::get_offset_of_max_bind_count_16(),
	Socket_t3821512045::get_offset_of_connected_17(),
	Socket_t3821512045::get_offset_of_closed_18(),
	Socket_t3821512045::get_offset_of_disposed_19(),
	Socket_t3821512045::get_offset_of_seed_endpoint_20(),
	Socket_t3821512045_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (SocketError_t307542793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1242[48] = 
{
	SocketError_t307542793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (SocketException_t1618573604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (SocketFlags_t2353657790)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1244[11] = 
{
	SocketFlags_t2353657790::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (SocketOptionLevel_t1505247880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1245[6] = 
{
	SocketOptionLevel_t1505247880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (SocketOptionName_t1089121285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1246[44] = 
{
	SocketOptionName_t1089121285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (SocketShutdown_t3247039417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1247[4] = 
{
	SocketShutdown_t3247039417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (SocketType_t1143498533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1248[7] = 
{
	SocketType_t1143498533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (DefaultCertificatePolicy_t2545332216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (Dns_t1335526197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (EndPoint_t4156119363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (FileWebRequest_t1571840111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1252[9] = 
{
	FileWebRequest_t1571840111::get_offset_of_uri_6(),
	FileWebRequest_t1571840111::get_offset_of_webHeaders_7(),
	FileWebRequest_t1571840111::get_offset_of_connectionGroup_8(),
	FileWebRequest_t1571840111::get_offset_of_contentLength_9(),
	FileWebRequest_t1571840111::get_offset_of_fileAccess_10(),
	FileWebRequest_t1571840111::get_offset_of_method_11(),
	FileWebRequest_t1571840111::get_offset_of_proxy_12(),
	FileWebRequest_t1571840111::get_offset_of_preAuthenticate_13(),
	FileWebRequest_t1571840111::get_offset_of_timeout_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (FileWebRequestCreator_t1109072211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (FtpRequestCreator_t3711983251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (FtpWebRequest_t3120721823), -1, sizeof(FtpWebRequest_t3120721823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1255[11] = 
{
	FtpWebRequest_t3120721823::get_offset_of_requestUri_6(),
	FtpWebRequest_t3120721823::get_offset_of_proxy_7(),
	FtpWebRequest_t3120721823::get_offset_of_timeout_8(),
	FtpWebRequest_t3120721823::get_offset_of_rwTimeout_9(),
	FtpWebRequest_t3120721823::get_offset_of_binary_10(),
	FtpWebRequest_t3120721823::get_offset_of_usePassive_11(),
	FtpWebRequest_t3120721823::get_offset_of_method_12(),
	FtpWebRequest_t3120721823::get_offset_of_locker_13(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_supportedCommands_14(),
	FtpWebRequest_t3120721823::get_offset_of_callback_15(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (GlobalProxySelection_t2251180943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (HttpRequestCreator_t1416559589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (HttpVersion_t1276659706), -1, sizeof(HttpVersion_t1276659706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1258[2] = 
{
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (HttpWebRequest_t1951404513), -1, sizeof(HttpWebRequest_t1951404513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1259[24] = 
{
	HttpWebRequest_t1951404513::get_offset_of_requestUri_6(),
	HttpWebRequest_t1951404513::get_offset_of_actualUri_7(),
	HttpWebRequest_t1951404513::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1951404513::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1951404513::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1951404513::get_offset_of_certificates_11(),
	HttpWebRequest_t1951404513::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1951404513::get_offset_of_contentLength_13(),
	HttpWebRequest_t1951404513::get_offset_of_webHeaders_14(),
	HttpWebRequest_t1951404513::get_offset_of_keepAlive_15(),
	HttpWebRequest_t1951404513::get_offset_of_maxAutoRedirect_16(),
	HttpWebRequest_t1951404513::get_offset_of_mediaType_17(),
	HttpWebRequest_t1951404513::get_offset_of_method_18(),
	HttpWebRequest_t1951404513::get_offset_of_initialMethod_19(),
	HttpWebRequest_t1951404513::get_offset_of_pipelined_20(),
	HttpWebRequest_t1951404513::get_offset_of_version_21(),
	HttpWebRequest_t1951404513::get_offset_of_proxy_22(),
	HttpWebRequest_t1951404513::get_offset_of_sendChunked_23(),
	HttpWebRequest_t1951404513::get_offset_of_servicePoint_24(),
	HttpWebRequest_t1951404513::get_offset_of_timeout_25(),
	HttpWebRequest_t1951404513::get_offset_of_redirects_26(),
	HttpWebRequest_t1951404513::get_offset_of_locker_27(),
	HttpWebRequest_t1951404513_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_28(),
	HttpWebRequest_t1951404513::get_offset_of_readWriteTimeout_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1262[11] = 
{
	IPAddress_t1399971723::get_offset_of_m_Address_0(),
	IPAddress_t1399971723::get_offset_of_m_Family_1(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_2(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_3(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_4(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { sizeof (IPEndPoint_t2615413766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1263[2] = 
{
	IPEndPoint_t2615413766::get_offset_of_address_0(),
	IPEndPoint_t2615413766::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1264[3] = 
{
	IPHostEntry_t994738509::get_offset_of_addressList_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (IPv6Address_t2596635879), -1, sizeof(IPv6Address_t2596635879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1265[5] = 
{
	IPv6Address_t2596635879::get_offset_of_address_0(),
	IPv6Address_t2596635879::get_offset_of_prefixLength_1(),
	IPv6Address_t2596635879::get_offset_of_scopeId_2(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (SecurityProtocolType_t3099771628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1268[3] = 
{
	SecurityProtocolType_t3099771628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (ServicePoint_t2765344313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1269[11] = 
{
	ServicePoint_t2765344313::get_offset_of_uri_0(),
	ServicePoint_t2765344313::get_offset_of_connectionLimit_1(),
	ServicePoint_t2765344313::get_offset_of_maxIdleTime_2(),
	ServicePoint_t2765344313::get_offset_of_currentConnections_3(),
	ServicePoint_t2765344313::get_offset_of_idleSince_4(),
	ServicePoint_t2765344313::get_offset_of_usesProxy_5(),
	ServicePoint_t2765344313::get_offset_of_sendContinue_6(),
	ServicePoint_t2765344313::get_offset_of_useConnect_7(),
	ServicePoint_t2765344313::get_offset_of_locker_8(),
	ServicePoint_t2765344313::get_offset_of_hostE_9(),
	ServicePoint_t2765344313::get_offset_of_useNagle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (ServicePointManager_t745663000), -1, sizeof(ServicePointManager_t745663000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1270[10] = 
{
	ServicePointManager_t745663000_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_server_cert_cb_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (SPKey_t1552752485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1271[2] = 
{
	SPKey_t1552752485::get_offset_of_uri_0(),
	SPKey_t1552752485::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (SocketAddress_t838303055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1272[1] = 
{
	SocketAddress_t838303055::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (WebHeaderCollection_t3028142837), -1, sizeof(WebHeaderCollection_t3028142837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1273[5] = 
{
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t3028142837::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (WebProxy_t1169192840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1274[5] = 
{
	WebProxy_t1169192840::get_offset_of_address_0(),
	WebProxy_t1169192840::get_offset_of_bypassOnLocal_1(),
	WebProxy_t1169192840::get_offset_of_bypassList_2(),
	WebProxy_t1169192840::get_offset_of_credentials_3(),
	WebProxy_t1169192840::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (Dummy_t2932948314), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (WebRequest_t1365124353), -1, sizeof(WebRequest_t1365124353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1276[5] = 
{
	WebRequest_t1365124353_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t1365124353_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t1365124353_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t1365124353::get_offset_of_authentication_level_4(),
	WebRequest_t1365124353_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (OpenFlags_t2370524385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1277[6] = 
{
	OpenFlags_t2370524385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (PublicKey_t870392), -1, sizeof(PublicKey_t870392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1278[5] = 
{
	PublicKey_t870392::get_offset_of__key_0(),
	PublicKey_t870392::get_offset_of__keyValue_1(),
	PublicKey_t870392::get_offset_of__params_2(),
	PublicKey_t870392::get_offset_of__oid_3(),
	PublicKey_t870392_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (StoreLocation_t1570828128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1279[3] = 
{
	StoreLocation_t1570828128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (StoreName_t2183514610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1280[9] = 
{
	StoreName_t2183514610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (X500DistinguishedName_t452415348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1281[1] = 
{
	X500DistinguishedName_t452415348::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (X500DistinguishedNameFlags_t2005802885)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1282[11] = 
{
	X500DistinguishedNameFlags_t2005802885::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (X509BasicConstraintsExtension_t1562873317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1283[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t1562873317::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (X509Certificate2_t4056456767), -1, sizeof(X509Certificate2_t4056456767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1284[13] = 
{
	X509Certificate2_t4056456767::get_offset_of__archived_5(),
	X509Certificate2_t4056456767::get_offset_of__extensions_6(),
	X509Certificate2_t4056456767::get_offset_of__name_7(),
	X509Certificate2_t4056456767::get_offset_of__serial_8(),
	X509Certificate2_t4056456767::get_offset_of__publicKey_9(),
	X509Certificate2_t4056456767::get_offset_of_issuer_name_10(),
	X509Certificate2_t4056456767::get_offset_of_subject_name_11(),
	X509Certificate2_t4056456767::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t4056456767::get_offset_of__cert_13(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (X509Certificate2Collection_t1108969367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (X509Certificate2Enumerator_t2356134957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1286[1] = 
{
	X509Certificate2Enumerator_t2356134957::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (X509CertificateCollection_t1197680765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (X509CertificateEnumerator_t1208230922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1288[1] = 
{
	X509CertificateEnumerator_t1208230922::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (X509Chain_t777637347), -1, sizeof(X509Chain_t777637347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1289[15] = 
{
	X509Chain_t777637347::get_offset_of_location_0(),
	X509Chain_t777637347::get_offset_of_elements_1(),
	X509Chain_t777637347::get_offset_of_policy_2(),
	X509Chain_t777637347::get_offset_of_status_3(),
	X509Chain_t777637347_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t777637347::get_offset_of_max_path_length_5(),
	X509Chain_t777637347::get_offset_of_working_issuer_name_6(),
	X509Chain_t777637347::get_offset_of_working_public_key_7(),
	X509Chain_t777637347::get_offset_of_bce_restriction_8(),
	X509Chain_t777637347::get_offset_of_roots_9(),
	X509Chain_t777637347::get_offset_of_cas_10(),
	X509Chain_t777637347::get_offset_of_collection_11(),
	X509Chain_t777637347_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_12(),
	X509Chain_t777637347_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_13(),
	X509Chain_t777637347_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (X509ChainElement_t528874471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1290[4] = 
{
	X509ChainElement_t528874471::get_offset_of_certificate_0(),
	X509ChainElement_t528874471::get_offset_of_status_1(),
	X509ChainElement_t528874471::get_offset_of_info_2(),
	X509ChainElement_t528874471::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (X509ChainElementCollection_t2081831987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1291[1] = 
{
	X509ChainElementCollection_t2081831987::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (X509ChainElementEnumerator_t3304975821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1292[1] = 
{
	X509ChainElementEnumerator_t3304975821::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (X509ChainPolicy_t3452126517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1293[8] = 
{
	X509ChainPolicy_t3452126517::get_offset_of_apps_0(),
	X509ChainPolicy_t3452126517::get_offset_of_cert_1(),
	X509ChainPolicy_t3452126517::get_offset_of_store_2(),
	X509ChainPolicy_t3452126517::get_offset_of_rflag_3(),
	X509ChainPolicy_t3452126517::get_offset_of_mode_4(),
	X509ChainPolicy_t3452126517::get_offset_of_timeout_5(),
	X509ChainPolicy_t3452126517::get_offset_of_vflags_6(),
	X509ChainPolicy_t3452126517::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (X509ChainStatus_t4278378721)+ sizeof (Il2CppObject), sizeof(X509ChainStatus_t4278378721_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1294[2] = 
{
	X509ChainStatus_t4278378721::get_offset_of_status_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509ChainStatus_t4278378721::get_offset_of_info_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (X509ChainStatusFlags_t480677120)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1295[24] = 
{
	X509ChainStatusFlags_t480677120::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (X509EnhancedKeyUsageExtension_t2099881051), -1, sizeof(X509EnhancedKeyUsageExtension_t2099881051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1296[3] = 
{
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t2099881051_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (X509Extension_t1320896183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1297[1] = 
{
	X509Extension_t1320896183::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (X509ExtensionCollection_t650873211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1298[1] = 
{
	X509ExtensionCollection_t650873211::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (X509ExtensionEnumerator_t3763443773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1299[1] = 
{
	X509ExtensionEnumerator_t3763443773::get_offset_of_enumerator_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
