﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Serializati3585985662.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel980888449.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionP3211808698.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1763623363;
// System.Type
struct Type_t;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2218572769;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t250795966;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t2470135126;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t822653733;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t1736907404;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t173433802;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t2940878176  : public Il2CppObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::<SerializationProtocolType>k__BackingField
	int32_t ___U3CSerializationProtocolTypeU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1763623363 * ___SocketImplementationConfig_7;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_8;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_9;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	Il2CppObject * ___U3CListenerU3Ek__BackingField_10;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t250795966 * ___U3CTrafficStatsIncomingU3Ek__BackingField_11;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t250795966 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_12;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t2470135126 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_13;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t1380178105 * ___trafficStatsStopwatch_14;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_15;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::commandLogSize
	int32_t ___commandLogSize_16;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_17;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_20;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_22;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_23;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_24;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_25;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_26;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_27;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_28;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_30;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_31;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t822653733 * ___peerBase_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	Il2CppObject * ___SendOutgoingLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	Il2CppObject * ___DispatchLockObject_34;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	Il2CppObject * ___EnqueueLock_35;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t3397334013* ___PayloadEncryptionSecret_36;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::encryptor
	Encryptor_t1736907404 * ___encryptor_37;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::decryptor
	Decryptor_t173433802 * ___decryptor_38;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier(&___clientVersion_5, value);
	}

	inline static int32_t get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CSerializationProtocolTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CSerializationProtocolTypeU3Ek__BackingField_6() const { return ___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return &___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline void set_U3CSerializationProtocolTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CSerializationProtocolTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___SocketImplementationConfig_7)); }
	inline Dictionary_2_t1763623363 * get_SocketImplementationConfig_7() const { return ___SocketImplementationConfig_7; }
	inline Dictionary_2_t1763623363 ** get_address_of_SocketImplementationConfig_7() { return &___SocketImplementationConfig_7; }
	inline void set_SocketImplementationConfig_7(Dictionary_2_t1763623363 * value)
	{
		___SocketImplementationConfig_7 = value;
		Il2CppCodeGenWriteBarrier(&___SocketImplementationConfig_7, value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CSocketImplementationU3Ek__BackingField_8)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_8() const { return ___U3CSocketImplementationU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_8() { return &___U3CSocketImplementationU3Ek__BackingField_8; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_8(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSocketImplementationU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_DebugOut_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___DebugOut_9)); }
	inline uint8_t get_DebugOut_9() const { return ___DebugOut_9; }
	inline uint8_t* get_address_of_DebugOut_9() { return &___DebugOut_9; }
	inline void set_DebugOut_9(uint8_t value)
	{
		___DebugOut_9 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CListenerU3Ek__BackingField_10)); }
	inline Il2CppObject * get_U3CListenerU3Ek__BackingField_10() const { return ___U3CListenerU3Ek__BackingField_10; }
	inline Il2CppObject ** get_address_of_U3CListenerU3Ek__BackingField_10() { return &___U3CListenerU3Ek__BackingField_10; }
	inline void set_U3CListenerU3Ek__BackingField_10(Il2CppObject * value)
	{
		___U3CListenerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CListenerU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CTrafficStatsIncomingU3Ek__BackingField_11)); }
	inline TrafficStats_t250795966 * get_U3CTrafficStatsIncomingU3Ek__BackingField_11() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_11; }
	inline TrafficStats_t250795966 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_11() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_11; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_11(TrafficStats_t250795966 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTrafficStatsIncomingU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CTrafficStatsOutgoingU3Ek__BackingField_12)); }
	inline TrafficStats_t250795966 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_12() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_12; }
	inline TrafficStats_t250795966 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_12() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_12; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_12(TrafficStats_t250795966 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTrafficStatsOutgoingU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CTrafficStatsGameLevelU3Ek__BackingField_13)); }
	inline TrafficStatsGameLevel_t2470135126 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_13() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_13; }
	inline TrafficStatsGameLevel_t2470135126 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_13() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_13; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_13(TrafficStatsGameLevel_t2470135126 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTrafficStatsGameLevelU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___trafficStatsStopwatch_14)); }
	inline Stopwatch_t1380178105 * get_trafficStatsStopwatch_14() const { return ___trafficStatsStopwatch_14; }
	inline Stopwatch_t1380178105 ** get_address_of_trafficStatsStopwatch_14() { return &___trafficStatsStopwatch_14; }
	inline void set_trafficStatsStopwatch_14(Stopwatch_t1380178105 * value)
	{
		___trafficStatsStopwatch_14 = value;
		Il2CppCodeGenWriteBarrier(&___trafficStatsStopwatch_14, value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___trafficStatsEnabled_15)); }
	inline bool get_trafficStatsEnabled_15() const { return ___trafficStatsEnabled_15; }
	inline bool* get_address_of_trafficStatsEnabled_15() { return &___trafficStatsEnabled_15; }
	inline void set_trafficStatsEnabled_15(bool value)
	{
		___trafficStatsEnabled_15 = value;
	}

	inline static int32_t get_offset_of_commandLogSize_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___commandLogSize_16)); }
	inline int32_t get_commandLogSize_16() const { return ___commandLogSize_16; }
	inline int32_t* get_address_of_commandLogSize_16() { return &___commandLogSize_16; }
	inline void set_commandLogSize_16(int32_t value)
	{
		___commandLogSize_16 = value;
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CEnableServerTracingU3Ek__BackingField_17)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_17() const { return ___U3CEnableServerTracingU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_17() { return &___U3CEnableServerTracingU3Ek__BackingField_17; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_17(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___quickResendAttempts_18)); }
	inline uint8_t get_quickResendAttempts_18() const { return ___quickResendAttempts_18; }
	inline uint8_t* get_address_of_quickResendAttempts_18() { return &___quickResendAttempts_18; }
	inline void set_quickResendAttempts_18(uint8_t value)
	{
		___quickResendAttempts_18 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___RhttpMinConnections_19)); }
	inline int32_t get_RhttpMinConnections_19() const { return ___RhttpMinConnections_19; }
	inline int32_t* get_address_of_RhttpMinConnections_19() { return &___RhttpMinConnections_19; }
	inline void set_RhttpMinConnections_19(int32_t value)
	{
		___RhttpMinConnections_19 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___RhttpMaxConnections_20)); }
	inline int32_t get_RhttpMaxConnections_20() const { return ___RhttpMaxConnections_20; }
	inline int32_t* get_address_of_RhttpMaxConnections_20() { return &___RhttpMaxConnections_20; }
	inline void set_RhttpMaxConnections_20(int32_t value)
	{
		___RhttpMaxConnections_20 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_21(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___ChannelCount_22)); }
	inline uint8_t get_ChannelCount_22() const { return ___ChannelCount_22; }
	inline uint8_t* get_address_of_ChannelCount_22() { return &___ChannelCount_22; }
	inline void set_ChannelCount_22(uint8_t value)
	{
		___ChannelCount_22 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___crcEnabled_23)); }
	inline bool get_crcEnabled_23() const { return ___crcEnabled_23; }
	inline bool* get_address_of_crcEnabled_23() { return &___crcEnabled_23; }
	inline void set_crcEnabled_23(bool value)
	{
		___crcEnabled_23 = value;
	}

	inline static int32_t get_offset_of_WarningSize_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___WarningSize_24)); }
	inline int32_t get_WarningSize_24() const { return ___WarningSize_24; }
	inline int32_t* get_address_of_WarningSize_24() { return &___WarningSize_24; }
	inline void set_WarningSize_24(int32_t value)
	{
		___WarningSize_24 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___SentCountAllowance_25)); }
	inline int32_t get_SentCountAllowance_25() const { return ___SentCountAllowance_25; }
	inline int32_t* get_address_of_SentCountAllowance_25() { return &___SentCountAllowance_25; }
	inline void set_SentCountAllowance_25(int32_t value)
	{
		___SentCountAllowance_25 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___TimePingInterval_26)); }
	inline int32_t get_TimePingInterval_26() const { return ___TimePingInterval_26; }
	inline int32_t* get_address_of_TimePingInterval_26() { return &___TimePingInterval_26; }
	inline void set_TimePingInterval_26(int32_t value)
	{
		___TimePingInterval_26 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___DisconnectTimeout_27)); }
	inline int32_t get_DisconnectTimeout_27() const { return ___DisconnectTimeout_27; }
	inline int32_t* get_address_of_DisconnectTimeout_27() { return &___DisconnectTimeout_27; }
	inline void set_DisconnectTimeout_27(int32_t value)
	{
		___DisconnectTimeout_27 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CTransportProtocolU3Ek__BackingField_28)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_28() const { return ___U3CTransportProtocolU3Ek__BackingField_28; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_28() { return &___U3CTransportProtocolU3Ek__BackingField_28; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_28(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_mtu_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___mtu_30)); }
	inline int32_t get_mtu_30() const { return ___mtu_30; }
	inline int32_t* get_address_of_mtu_30() { return &___mtu_30; }
	inline void set_mtu_30(int32_t value)
	{
		___mtu_30 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___U3CIsSendingOnlyAcksU3Ek__BackingField_31)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_31() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_31() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_31; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_31(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_peerBase_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___peerBase_32)); }
	inline PeerBase_t822653733 * get_peerBase_32() const { return ___peerBase_32; }
	inline PeerBase_t822653733 ** get_address_of_peerBase_32() { return &___peerBase_32; }
	inline void set_peerBase_32(PeerBase_t822653733 * value)
	{
		___peerBase_32 = value;
		Il2CppCodeGenWriteBarrier(&___peerBase_32, value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___SendOutgoingLockObject_33)); }
	inline Il2CppObject * get_SendOutgoingLockObject_33() const { return ___SendOutgoingLockObject_33; }
	inline Il2CppObject ** get_address_of_SendOutgoingLockObject_33() { return &___SendOutgoingLockObject_33; }
	inline void set_SendOutgoingLockObject_33(Il2CppObject * value)
	{
		___SendOutgoingLockObject_33 = value;
		Il2CppCodeGenWriteBarrier(&___SendOutgoingLockObject_33, value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___DispatchLockObject_34)); }
	inline Il2CppObject * get_DispatchLockObject_34() const { return ___DispatchLockObject_34; }
	inline Il2CppObject ** get_address_of_DispatchLockObject_34() { return &___DispatchLockObject_34; }
	inline void set_DispatchLockObject_34(Il2CppObject * value)
	{
		___DispatchLockObject_34 = value;
		Il2CppCodeGenWriteBarrier(&___DispatchLockObject_34, value);
	}

	inline static int32_t get_offset_of_EnqueueLock_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___EnqueueLock_35)); }
	inline Il2CppObject * get_EnqueueLock_35() const { return ___EnqueueLock_35; }
	inline Il2CppObject ** get_address_of_EnqueueLock_35() { return &___EnqueueLock_35; }
	inline void set_EnqueueLock_35(Il2CppObject * value)
	{
		___EnqueueLock_35 = value;
		Il2CppCodeGenWriteBarrier(&___EnqueueLock_35, value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___PayloadEncryptionSecret_36)); }
	inline ByteU5BU5D_t3397334013* get_PayloadEncryptionSecret_36() const { return ___PayloadEncryptionSecret_36; }
	inline ByteU5BU5D_t3397334013** get_address_of_PayloadEncryptionSecret_36() { return &___PayloadEncryptionSecret_36; }
	inline void set_PayloadEncryptionSecret_36(ByteU5BU5D_t3397334013* value)
	{
		___PayloadEncryptionSecret_36 = value;
		Il2CppCodeGenWriteBarrier(&___PayloadEncryptionSecret_36, value);
	}

	inline static int32_t get_offset_of_encryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___encryptor_37)); }
	inline Encryptor_t1736907404 * get_encryptor_37() const { return ___encryptor_37; }
	inline Encryptor_t1736907404 ** get_address_of_encryptor_37() { return &___encryptor_37; }
	inline void set_encryptor_37(Encryptor_t1736907404 * value)
	{
		___encryptor_37 = value;
		Il2CppCodeGenWriteBarrier(&___encryptor_37, value);
	}

	inline static int32_t get_offset_of_decryptor_38() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176, ___decryptor_38)); }
	inline Decryptor_t173433802 * get_decryptor_38() const { return ___decryptor_38; }
	inline Decryptor_t173433802 ** get_address_of_decryptor_38() { return &___decryptor_38; }
	inline void set_decryptor_38(Decryptor_t173433802 * value)
	{
		___decryptor_38 = value;
		Il2CppCodeGenWriteBarrier(&___decryptor_38, value);
	}
};

struct PhotonPeer_t2940878176_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_29;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t2940878176_StaticFields, ___OutgoingStreamBufferSize_29)); }
	inline int32_t get_OutgoingStreamBufferSize_29() const { return ___OutgoingStreamBufferSize_29; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_29() { return &___OutgoingStreamBufferSize_29; }
	inline void set_OutgoingStreamBufferSize_29(int32_t value)
	{
		___OutgoingStreamBufferSize_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
