﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionP3211808698.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_Con442361298.h"

// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t2940878176;
// ExitGames.Client.Photon.IProtocol
struct IProtocol_t4285598013;
// ExitGames.Client.Photon.IPhotonSocket
struct IPhotonSocket_t429031990;
// System.String
struct String_t;
// ExitGames.Client.Photon.NCommand
struct NCommand_t2133614299;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>
struct Queue_1_t1477165535;
// Photon.SocketServer.Security.ICryptoProvider
struct ICryptoProvider_t3263388049;
// System.Random
struct Random_t1044426839;
// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem>
struct LinkedList_1_t3721527771;
// ExitGames.Client.Photon.NetworkSimulationSet
struct NetworkSimulationSet_t2323123337;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem>
struct Queue_1_t2194021988;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// ExitGames.Client.Photon.StreamBuffer
struct StreamBuffer_t3747118964;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PeerBase
struct  PeerBase_t822653733  : public Il2CppObject
{
public:
	// ExitGames.Client.Photon.PhotonPeer ExitGames.Client.Photon.PeerBase::ppeer
	PhotonPeer_t2940878176 * ___ppeer_0;
	// ExitGames.Client.Photon.IProtocol ExitGames.Client.Photon.PeerBase::protocol
	IProtocol_t4285598013 * ___protocol_1;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PeerBase::usedProtocol
	uint8_t ___usedProtocol_2;
	// ExitGames.Client.Photon.IPhotonSocket ExitGames.Client.Photon.PeerBase::rt
	IPhotonSocket_t429031990 * ___rt_3;
	// System.String ExitGames.Client.Photon.PeerBase::<ServerAddress>k__BackingField
	String_t* ___U3CServerAddressU3Ek__BackingField_4;
	// System.String ExitGames.Client.Photon.PeerBase::<HttpUrlParameters>k__BackingField
	String_t* ___U3CHttpUrlParametersU3Ek__BackingField_5;
	// System.Int32 ExitGames.Client.Photon.PeerBase::ByteCountLastOperation
	int32_t ___ByteCountLastOperation_6;
	// System.Int32 ExitGames.Client.Photon.PeerBase::ByteCountCurrentDispatch
	int32_t ___ByteCountCurrentDispatch_7;
	// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.PeerBase::CommandInCurrentDispatch
	NCommand_t2133614299 * ___CommandInCurrentDispatch_8;
	// System.Int32 ExitGames.Client.Photon.PeerBase::TrafficPackageHeaderSize
	int32_t ___TrafficPackageHeaderSize_9;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetLossByCrc
	int32_t ___packetLossByCrc_10;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetLossByChallenge
	int32_t ___packetLossByChallenge_11;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction> ExitGames.Client.Photon.PeerBase::ActionQueue
	Queue_1_t1477165535 * ___ActionQueue_12;
	// System.Int16 ExitGames.Client.Photon.PeerBase::peerID
	int16_t ___peerID_13;
	// ExitGames.Client.Photon.PeerBase/ConnectionStateValue ExitGames.Client.Photon.PeerBase::peerConnectionState
	uint8_t ___peerConnectionState_14;
	// System.Int32 ExitGames.Client.Photon.PeerBase::serverTimeOffset
	int32_t ___serverTimeOffset_15;
	// System.Boolean ExitGames.Client.Photon.PeerBase::serverTimeOffsetIsAvailable
	bool ___serverTimeOffsetIsAvailable_16;
	// System.Int32 ExitGames.Client.Photon.PeerBase::roundTripTime
	int32_t ___roundTripTime_17;
	// System.Int32 ExitGames.Client.Photon.PeerBase::roundTripTimeVariance
	int32_t ___roundTripTimeVariance_18;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lastRoundTripTime
	int32_t ___lastRoundTripTime_19;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lowestRoundTripTime
	int32_t ___lowestRoundTripTime_20;
	// System.Int32 ExitGames.Client.Photon.PeerBase::lastRoundTripTimeVariance
	int32_t ___lastRoundTripTimeVariance_21;
	// System.Int32 ExitGames.Client.Photon.PeerBase::highestRoundTripTimeVariance
	int32_t ___highestRoundTripTimeVariance_22;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timestampOfLastReceive
	int32_t ___timestampOfLastReceive_23;
	// System.Int32 ExitGames.Client.Photon.PeerBase::packetThrottleInterval
	int32_t ___packetThrottleInterval_24;
	// System.Int64 ExitGames.Client.Photon.PeerBase::bytesOut
	int64_t ___bytesOut_26;
	// System.Int64 ExitGames.Client.Photon.PeerBase::bytesIn
	int64_t ___bytesIn_27;
	// System.Int32 ExitGames.Client.Photon.PeerBase::commandBufferSize
	int32_t ___commandBufferSize_28;
	// Photon.SocketServer.Security.ICryptoProvider ExitGames.Client.Photon.PeerBase::CryptoProvider
	Il2CppObject * ___CryptoProvider_29;
	// System.Random ExitGames.Client.Photon.PeerBase::lagRandomizer
	Random_t1044426839 * ___lagRandomizer_30;
	// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem> ExitGames.Client.Photon.PeerBase::NetSimListOutgoing
	LinkedList_1_t3721527771 * ___NetSimListOutgoing_31;
	// System.Collections.Generic.LinkedList`1<ExitGames.Client.Photon.SimulationItem> ExitGames.Client.Photon.PeerBase::NetSimListIncoming
	LinkedList_1_t3721527771 * ___NetSimListIncoming_32;
	// ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PeerBase::networkSimulationSettings
	NetworkSimulationSet_t2323123337 * ___networkSimulationSettings_33;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem> ExitGames.Client.Photon.PeerBase::CommandLog
	Queue_1_t2194021988 * ___CommandLog_34;
	// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem> ExitGames.Client.Photon.PeerBase::InReliableLog
	Queue_1_t2194021988 * ___InReliableLog_35;
	// System.Object ExitGames.Client.Photon.PeerBase::CustomInitData
	Il2CppObject * ___CustomInitData_36;
	// System.String ExitGames.Client.Photon.PeerBase::AppId
	String_t* ___AppId_37;
	// System.Byte[] ExitGames.Client.Photon.PeerBase::<TcpConnectionPrefix>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CTcpConnectionPrefixU3Ek__BackingField_38;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeBase
	int32_t ___timeBase_39;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeInt
	int32_t ___timeInt_40;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeoutInt
	int32_t ___timeoutInt_41;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastAckReceive
	int32_t ___timeLastAckReceive_42;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastSendAck
	int32_t ___timeLastSendAck_43;
	// System.Int32 ExitGames.Client.Photon.PeerBase::timeLastSendOutgoing
	int32_t ___timeLastSendOutgoing_44;
	// System.Boolean ExitGames.Client.Photon.PeerBase::ApplicationIsInitialized
	bool ___ApplicationIsInitialized_48;
	// System.Boolean ExitGames.Client.Photon.PeerBase::isEncryptionAvailable
	bool ___isEncryptionAvailable_49;
	// System.Int32 ExitGames.Client.Photon.PeerBase::outgoingCommandsInStream
	int32_t ___outgoingCommandsInStream_50;
	// ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::SerializeMemStream
	StreamBuffer_t3747118964 * ___SerializeMemStream_51;

public:
	inline static int32_t get_offset_of_ppeer_0() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___ppeer_0)); }
	inline PhotonPeer_t2940878176 * get_ppeer_0() const { return ___ppeer_0; }
	inline PhotonPeer_t2940878176 ** get_address_of_ppeer_0() { return &___ppeer_0; }
	inline void set_ppeer_0(PhotonPeer_t2940878176 * value)
	{
		___ppeer_0 = value;
		Il2CppCodeGenWriteBarrier(&___ppeer_0, value);
	}

	inline static int32_t get_offset_of_protocol_1() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___protocol_1)); }
	inline IProtocol_t4285598013 * get_protocol_1() const { return ___protocol_1; }
	inline IProtocol_t4285598013 ** get_address_of_protocol_1() { return &___protocol_1; }
	inline void set_protocol_1(IProtocol_t4285598013 * value)
	{
		___protocol_1 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_1, value);
	}

	inline static int32_t get_offset_of_usedProtocol_2() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___usedProtocol_2)); }
	inline uint8_t get_usedProtocol_2() const { return ___usedProtocol_2; }
	inline uint8_t* get_address_of_usedProtocol_2() { return &___usedProtocol_2; }
	inline void set_usedProtocol_2(uint8_t value)
	{
		___usedProtocol_2 = value;
	}

	inline static int32_t get_offset_of_rt_3() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___rt_3)); }
	inline IPhotonSocket_t429031990 * get_rt_3() const { return ___rt_3; }
	inline IPhotonSocket_t429031990 ** get_address_of_rt_3() { return &___rt_3; }
	inline void set_rt_3(IPhotonSocket_t429031990 * value)
	{
		___rt_3 = value;
		Il2CppCodeGenWriteBarrier(&___rt_3, value);
	}

	inline static int32_t get_offset_of_U3CServerAddressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___U3CServerAddressU3Ek__BackingField_4)); }
	inline String_t* get_U3CServerAddressU3Ek__BackingField_4() const { return ___U3CServerAddressU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServerAddressU3Ek__BackingField_4() { return &___U3CServerAddressU3Ek__BackingField_4; }
	inline void set_U3CServerAddressU3Ek__BackingField_4(String_t* value)
	{
		___U3CServerAddressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServerAddressU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___U3CHttpUrlParametersU3Ek__BackingField_5)); }
	inline String_t* get_U3CHttpUrlParametersU3Ek__BackingField_5() const { return ___U3CHttpUrlParametersU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CHttpUrlParametersU3Ek__BackingField_5() { return &___U3CHttpUrlParametersU3Ek__BackingField_5; }
	inline void set_U3CHttpUrlParametersU3Ek__BackingField_5(String_t* value)
	{
		___U3CHttpUrlParametersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpUrlParametersU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_ByteCountLastOperation_6() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___ByteCountLastOperation_6)); }
	inline int32_t get_ByteCountLastOperation_6() const { return ___ByteCountLastOperation_6; }
	inline int32_t* get_address_of_ByteCountLastOperation_6() { return &___ByteCountLastOperation_6; }
	inline void set_ByteCountLastOperation_6(int32_t value)
	{
		___ByteCountLastOperation_6 = value;
	}

	inline static int32_t get_offset_of_ByteCountCurrentDispatch_7() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___ByteCountCurrentDispatch_7)); }
	inline int32_t get_ByteCountCurrentDispatch_7() const { return ___ByteCountCurrentDispatch_7; }
	inline int32_t* get_address_of_ByteCountCurrentDispatch_7() { return &___ByteCountCurrentDispatch_7; }
	inline void set_ByteCountCurrentDispatch_7(int32_t value)
	{
		___ByteCountCurrentDispatch_7 = value;
	}

	inline static int32_t get_offset_of_CommandInCurrentDispatch_8() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___CommandInCurrentDispatch_8)); }
	inline NCommand_t2133614299 * get_CommandInCurrentDispatch_8() const { return ___CommandInCurrentDispatch_8; }
	inline NCommand_t2133614299 ** get_address_of_CommandInCurrentDispatch_8() { return &___CommandInCurrentDispatch_8; }
	inline void set_CommandInCurrentDispatch_8(NCommand_t2133614299 * value)
	{
		___CommandInCurrentDispatch_8 = value;
		Il2CppCodeGenWriteBarrier(&___CommandInCurrentDispatch_8, value);
	}

	inline static int32_t get_offset_of_TrafficPackageHeaderSize_9() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___TrafficPackageHeaderSize_9)); }
	inline int32_t get_TrafficPackageHeaderSize_9() const { return ___TrafficPackageHeaderSize_9; }
	inline int32_t* get_address_of_TrafficPackageHeaderSize_9() { return &___TrafficPackageHeaderSize_9; }
	inline void set_TrafficPackageHeaderSize_9(int32_t value)
	{
		___TrafficPackageHeaderSize_9 = value;
	}

	inline static int32_t get_offset_of_packetLossByCrc_10() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___packetLossByCrc_10)); }
	inline int32_t get_packetLossByCrc_10() const { return ___packetLossByCrc_10; }
	inline int32_t* get_address_of_packetLossByCrc_10() { return &___packetLossByCrc_10; }
	inline void set_packetLossByCrc_10(int32_t value)
	{
		___packetLossByCrc_10 = value;
	}

	inline static int32_t get_offset_of_packetLossByChallenge_11() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___packetLossByChallenge_11)); }
	inline int32_t get_packetLossByChallenge_11() const { return ___packetLossByChallenge_11; }
	inline int32_t* get_address_of_packetLossByChallenge_11() { return &___packetLossByChallenge_11; }
	inline void set_packetLossByChallenge_11(int32_t value)
	{
		___packetLossByChallenge_11 = value;
	}

	inline static int32_t get_offset_of_ActionQueue_12() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___ActionQueue_12)); }
	inline Queue_1_t1477165535 * get_ActionQueue_12() const { return ___ActionQueue_12; }
	inline Queue_1_t1477165535 ** get_address_of_ActionQueue_12() { return &___ActionQueue_12; }
	inline void set_ActionQueue_12(Queue_1_t1477165535 * value)
	{
		___ActionQueue_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionQueue_12, value);
	}

	inline static int32_t get_offset_of_peerID_13() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___peerID_13)); }
	inline int16_t get_peerID_13() const { return ___peerID_13; }
	inline int16_t* get_address_of_peerID_13() { return &___peerID_13; }
	inline void set_peerID_13(int16_t value)
	{
		___peerID_13 = value;
	}

	inline static int32_t get_offset_of_peerConnectionState_14() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___peerConnectionState_14)); }
	inline uint8_t get_peerConnectionState_14() const { return ___peerConnectionState_14; }
	inline uint8_t* get_address_of_peerConnectionState_14() { return &___peerConnectionState_14; }
	inline void set_peerConnectionState_14(uint8_t value)
	{
		___peerConnectionState_14 = value;
	}

	inline static int32_t get_offset_of_serverTimeOffset_15() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___serverTimeOffset_15)); }
	inline int32_t get_serverTimeOffset_15() const { return ___serverTimeOffset_15; }
	inline int32_t* get_address_of_serverTimeOffset_15() { return &___serverTimeOffset_15; }
	inline void set_serverTimeOffset_15(int32_t value)
	{
		___serverTimeOffset_15 = value;
	}

	inline static int32_t get_offset_of_serverTimeOffsetIsAvailable_16() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___serverTimeOffsetIsAvailable_16)); }
	inline bool get_serverTimeOffsetIsAvailable_16() const { return ___serverTimeOffsetIsAvailable_16; }
	inline bool* get_address_of_serverTimeOffsetIsAvailable_16() { return &___serverTimeOffsetIsAvailable_16; }
	inline void set_serverTimeOffsetIsAvailable_16(bool value)
	{
		___serverTimeOffsetIsAvailable_16 = value;
	}

	inline static int32_t get_offset_of_roundTripTime_17() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___roundTripTime_17)); }
	inline int32_t get_roundTripTime_17() const { return ___roundTripTime_17; }
	inline int32_t* get_address_of_roundTripTime_17() { return &___roundTripTime_17; }
	inline void set_roundTripTime_17(int32_t value)
	{
		___roundTripTime_17 = value;
	}

	inline static int32_t get_offset_of_roundTripTimeVariance_18() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___roundTripTimeVariance_18)); }
	inline int32_t get_roundTripTimeVariance_18() const { return ___roundTripTimeVariance_18; }
	inline int32_t* get_address_of_roundTripTimeVariance_18() { return &___roundTripTimeVariance_18; }
	inline void set_roundTripTimeVariance_18(int32_t value)
	{
		___roundTripTimeVariance_18 = value;
	}

	inline static int32_t get_offset_of_lastRoundTripTime_19() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___lastRoundTripTime_19)); }
	inline int32_t get_lastRoundTripTime_19() const { return ___lastRoundTripTime_19; }
	inline int32_t* get_address_of_lastRoundTripTime_19() { return &___lastRoundTripTime_19; }
	inline void set_lastRoundTripTime_19(int32_t value)
	{
		___lastRoundTripTime_19 = value;
	}

	inline static int32_t get_offset_of_lowestRoundTripTime_20() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___lowestRoundTripTime_20)); }
	inline int32_t get_lowestRoundTripTime_20() const { return ___lowestRoundTripTime_20; }
	inline int32_t* get_address_of_lowestRoundTripTime_20() { return &___lowestRoundTripTime_20; }
	inline void set_lowestRoundTripTime_20(int32_t value)
	{
		___lowestRoundTripTime_20 = value;
	}

	inline static int32_t get_offset_of_lastRoundTripTimeVariance_21() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___lastRoundTripTimeVariance_21)); }
	inline int32_t get_lastRoundTripTimeVariance_21() const { return ___lastRoundTripTimeVariance_21; }
	inline int32_t* get_address_of_lastRoundTripTimeVariance_21() { return &___lastRoundTripTimeVariance_21; }
	inline void set_lastRoundTripTimeVariance_21(int32_t value)
	{
		___lastRoundTripTimeVariance_21 = value;
	}

	inline static int32_t get_offset_of_highestRoundTripTimeVariance_22() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___highestRoundTripTimeVariance_22)); }
	inline int32_t get_highestRoundTripTimeVariance_22() const { return ___highestRoundTripTimeVariance_22; }
	inline int32_t* get_address_of_highestRoundTripTimeVariance_22() { return &___highestRoundTripTimeVariance_22; }
	inline void set_highestRoundTripTimeVariance_22(int32_t value)
	{
		___highestRoundTripTimeVariance_22 = value;
	}

	inline static int32_t get_offset_of_timestampOfLastReceive_23() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timestampOfLastReceive_23)); }
	inline int32_t get_timestampOfLastReceive_23() const { return ___timestampOfLastReceive_23; }
	inline int32_t* get_address_of_timestampOfLastReceive_23() { return &___timestampOfLastReceive_23; }
	inline void set_timestampOfLastReceive_23(int32_t value)
	{
		___timestampOfLastReceive_23 = value;
	}

	inline static int32_t get_offset_of_packetThrottleInterval_24() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___packetThrottleInterval_24)); }
	inline int32_t get_packetThrottleInterval_24() const { return ___packetThrottleInterval_24; }
	inline int32_t* get_address_of_packetThrottleInterval_24() { return &___packetThrottleInterval_24; }
	inline void set_packetThrottleInterval_24(int32_t value)
	{
		___packetThrottleInterval_24 = value;
	}

	inline static int32_t get_offset_of_bytesOut_26() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___bytesOut_26)); }
	inline int64_t get_bytesOut_26() const { return ___bytesOut_26; }
	inline int64_t* get_address_of_bytesOut_26() { return &___bytesOut_26; }
	inline void set_bytesOut_26(int64_t value)
	{
		___bytesOut_26 = value;
	}

	inline static int32_t get_offset_of_bytesIn_27() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___bytesIn_27)); }
	inline int64_t get_bytesIn_27() const { return ___bytesIn_27; }
	inline int64_t* get_address_of_bytesIn_27() { return &___bytesIn_27; }
	inline void set_bytesIn_27(int64_t value)
	{
		___bytesIn_27 = value;
	}

	inline static int32_t get_offset_of_commandBufferSize_28() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___commandBufferSize_28)); }
	inline int32_t get_commandBufferSize_28() const { return ___commandBufferSize_28; }
	inline int32_t* get_address_of_commandBufferSize_28() { return &___commandBufferSize_28; }
	inline void set_commandBufferSize_28(int32_t value)
	{
		___commandBufferSize_28 = value;
	}

	inline static int32_t get_offset_of_CryptoProvider_29() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___CryptoProvider_29)); }
	inline Il2CppObject * get_CryptoProvider_29() const { return ___CryptoProvider_29; }
	inline Il2CppObject ** get_address_of_CryptoProvider_29() { return &___CryptoProvider_29; }
	inline void set_CryptoProvider_29(Il2CppObject * value)
	{
		___CryptoProvider_29 = value;
		Il2CppCodeGenWriteBarrier(&___CryptoProvider_29, value);
	}

	inline static int32_t get_offset_of_lagRandomizer_30() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___lagRandomizer_30)); }
	inline Random_t1044426839 * get_lagRandomizer_30() const { return ___lagRandomizer_30; }
	inline Random_t1044426839 ** get_address_of_lagRandomizer_30() { return &___lagRandomizer_30; }
	inline void set_lagRandomizer_30(Random_t1044426839 * value)
	{
		___lagRandomizer_30 = value;
		Il2CppCodeGenWriteBarrier(&___lagRandomizer_30, value);
	}

	inline static int32_t get_offset_of_NetSimListOutgoing_31() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___NetSimListOutgoing_31)); }
	inline LinkedList_1_t3721527771 * get_NetSimListOutgoing_31() const { return ___NetSimListOutgoing_31; }
	inline LinkedList_1_t3721527771 ** get_address_of_NetSimListOutgoing_31() { return &___NetSimListOutgoing_31; }
	inline void set_NetSimListOutgoing_31(LinkedList_1_t3721527771 * value)
	{
		___NetSimListOutgoing_31 = value;
		Il2CppCodeGenWriteBarrier(&___NetSimListOutgoing_31, value);
	}

	inline static int32_t get_offset_of_NetSimListIncoming_32() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___NetSimListIncoming_32)); }
	inline LinkedList_1_t3721527771 * get_NetSimListIncoming_32() const { return ___NetSimListIncoming_32; }
	inline LinkedList_1_t3721527771 ** get_address_of_NetSimListIncoming_32() { return &___NetSimListIncoming_32; }
	inline void set_NetSimListIncoming_32(LinkedList_1_t3721527771 * value)
	{
		___NetSimListIncoming_32 = value;
		Il2CppCodeGenWriteBarrier(&___NetSimListIncoming_32, value);
	}

	inline static int32_t get_offset_of_networkSimulationSettings_33() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___networkSimulationSettings_33)); }
	inline NetworkSimulationSet_t2323123337 * get_networkSimulationSettings_33() const { return ___networkSimulationSettings_33; }
	inline NetworkSimulationSet_t2323123337 ** get_address_of_networkSimulationSettings_33() { return &___networkSimulationSettings_33; }
	inline void set_networkSimulationSettings_33(NetworkSimulationSet_t2323123337 * value)
	{
		___networkSimulationSettings_33 = value;
		Il2CppCodeGenWriteBarrier(&___networkSimulationSettings_33, value);
	}

	inline static int32_t get_offset_of_CommandLog_34() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___CommandLog_34)); }
	inline Queue_1_t2194021988 * get_CommandLog_34() const { return ___CommandLog_34; }
	inline Queue_1_t2194021988 ** get_address_of_CommandLog_34() { return &___CommandLog_34; }
	inline void set_CommandLog_34(Queue_1_t2194021988 * value)
	{
		___CommandLog_34 = value;
		Il2CppCodeGenWriteBarrier(&___CommandLog_34, value);
	}

	inline static int32_t get_offset_of_InReliableLog_35() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___InReliableLog_35)); }
	inline Queue_1_t2194021988 * get_InReliableLog_35() const { return ___InReliableLog_35; }
	inline Queue_1_t2194021988 ** get_address_of_InReliableLog_35() { return &___InReliableLog_35; }
	inline void set_InReliableLog_35(Queue_1_t2194021988 * value)
	{
		___InReliableLog_35 = value;
		Il2CppCodeGenWriteBarrier(&___InReliableLog_35, value);
	}

	inline static int32_t get_offset_of_CustomInitData_36() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___CustomInitData_36)); }
	inline Il2CppObject * get_CustomInitData_36() const { return ___CustomInitData_36; }
	inline Il2CppObject ** get_address_of_CustomInitData_36() { return &___CustomInitData_36; }
	inline void set_CustomInitData_36(Il2CppObject * value)
	{
		___CustomInitData_36 = value;
		Il2CppCodeGenWriteBarrier(&___CustomInitData_36, value);
	}

	inline static int32_t get_offset_of_AppId_37() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___AppId_37)); }
	inline String_t* get_AppId_37() const { return ___AppId_37; }
	inline String_t** get_address_of_AppId_37() { return &___AppId_37; }
	inline void set_AppId_37(String_t* value)
	{
		___AppId_37 = value;
		Il2CppCodeGenWriteBarrier(&___AppId_37, value);
	}

	inline static int32_t get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___U3CTcpConnectionPrefixU3Ek__BackingField_38)); }
	inline ByteU5BU5D_t3397334013* get_U3CTcpConnectionPrefixU3Ek__BackingField_38() const { return ___U3CTcpConnectionPrefixU3Ek__BackingField_38; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CTcpConnectionPrefixU3Ek__BackingField_38() { return &___U3CTcpConnectionPrefixU3Ek__BackingField_38; }
	inline void set_U3CTcpConnectionPrefixU3Ek__BackingField_38(ByteU5BU5D_t3397334013* value)
	{
		___U3CTcpConnectionPrefixU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTcpConnectionPrefixU3Ek__BackingField_38, value);
	}

	inline static int32_t get_offset_of_timeBase_39() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timeBase_39)); }
	inline int32_t get_timeBase_39() const { return ___timeBase_39; }
	inline int32_t* get_address_of_timeBase_39() { return &___timeBase_39; }
	inline void set_timeBase_39(int32_t value)
	{
		___timeBase_39 = value;
	}

	inline static int32_t get_offset_of_timeInt_40() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timeInt_40)); }
	inline int32_t get_timeInt_40() const { return ___timeInt_40; }
	inline int32_t* get_address_of_timeInt_40() { return &___timeInt_40; }
	inline void set_timeInt_40(int32_t value)
	{
		___timeInt_40 = value;
	}

	inline static int32_t get_offset_of_timeoutInt_41() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timeoutInt_41)); }
	inline int32_t get_timeoutInt_41() const { return ___timeoutInt_41; }
	inline int32_t* get_address_of_timeoutInt_41() { return &___timeoutInt_41; }
	inline void set_timeoutInt_41(int32_t value)
	{
		___timeoutInt_41 = value;
	}

	inline static int32_t get_offset_of_timeLastAckReceive_42() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timeLastAckReceive_42)); }
	inline int32_t get_timeLastAckReceive_42() const { return ___timeLastAckReceive_42; }
	inline int32_t* get_address_of_timeLastAckReceive_42() { return &___timeLastAckReceive_42; }
	inline void set_timeLastAckReceive_42(int32_t value)
	{
		___timeLastAckReceive_42 = value;
	}

	inline static int32_t get_offset_of_timeLastSendAck_43() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timeLastSendAck_43)); }
	inline int32_t get_timeLastSendAck_43() const { return ___timeLastSendAck_43; }
	inline int32_t* get_address_of_timeLastSendAck_43() { return &___timeLastSendAck_43; }
	inline void set_timeLastSendAck_43(int32_t value)
	{
		___timeLastSendAck_43 = value;
	}

	inline static int32_t get_offset_of_timeLastSendOutgoing_44() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___timeLastSendOutgoing_44)); }
	inline int32_t get_timeLastSendOutgoing_44() const { return ___timeLastSendOutgoing_44; }
	inline int32_t* get_address_of_timeLastSendOutgoing_44() { return &___timeLastSendOutgoing_44; }
	inline void set_timeLastSendOutgoing_44(int32_t value)
	{
		___timeLastSendOutgoing_44 = value;
	}

	inline static int32_t get_offset_of_ApplicationIsInitialized_48() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___ApplicationIsInitialized_48)); }
	inline bool get_ApplicationIsInitialized_48() const { return ___ApplicationIsInitialized_48; }
	inline bool* get_address_of_ApplicationIsInitialized_48() { return &___ApplicationIsInitialized_48; }
	inline void set_ApplicationIsInitialized_48(bool value)
	{
		___ApplicationIsInitialized_48 = value;
	}

	inline static int32_t get_offset_of_isEncryptionAvailable_49() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___isEncryptionAvailable_49)); }
	inline bool get_isEncryptionAvailable_49() const { return ___isEncryptionAvailable_49; }
	inline bool* get_address_of_isEncryptionAvailable_49() { return &___isEncryptionAvailable_49; }
	inline void set_isEncryptionAvailable_49(bool value)
	{
		___isEncryptionAvailable_49 = value;
	}

	inline static int32_t get_offset_of_outgoingCommandsInStream_50() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___outgoingCommandsInStream_50)); }
	inline int32_t get_outgoingCommandsInStream_50() const { return ___outgoingCommandsInStream_50; }
	inline int32_t* get_address_of_outgoingCommandsInStream_50() { return &___outgoingCommandsInStream_50; }
	inline void set_outgoingCommandsInStream_50(int32_t value)
	{
		___outgoingCommandsInStream_50 = value;
	}

	inline static int32_t get_offset_of_SerializeMemStream_51() { return static_cast<int32_t>(offsetof(PeerBase_t822653733, ___SerializeMemStream_51)); }
	inline StreamBuffer_t3747118964 * get_SerializeMemStream_51() const { return ___SerializeMemStream_51; }
	inline StreamBuffer_t3747118964 ** get_address_of_SerializeMemStream_51() { return &___SerializeMemStream_51; }
	inline void set_SerializeMemStream_51(StreamBuffer_t3747118964 * value)
	{
		___SerializeMemStream_51 = value;
		Il2CppCodeGenWriteBarrier(&___SerializeMemStream_51, value);
	}
};

struct PeerBase_t822653733_StaticFields
{
public:
	// System.Int16 ExitGames.Client.Photon.PeerBase::peerCount
	int16_t ___peerCount_25;

public:
	inline static int32_t get_offset_of_peerCount_25() { return static_cast<int32_t>(offsetof(PeerBase_t822653733_StaticFields, ___peerCount_25)); }
	inline int16_t get_peerCount_25() const { return ___peerCount_25; }
	inline int16_t* get_address_of_peerCount_25() { return &___peerCount_25; }
	inline void set_peerCount_25(int16_t value)
	{
		___peerCount_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
