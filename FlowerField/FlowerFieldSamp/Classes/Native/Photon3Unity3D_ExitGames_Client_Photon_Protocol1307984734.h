﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.Type,ExitGames.Client.Photon.CustomType>
struct Dictionary_2_t3407023366;
// System.Collections.Generic.Dictionary`2<System.Byte,ExitGames.Client.Photon.CustomType>
struct Dictionary_2_t844425476;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Protocol
struct  Protocol_t1307984734  : public Il2CppObject
{
public:

public:
};

struct Protocol_t1307984734_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,ExitGames.Client.Photon.CustomType> ExitGames.Client.Photon.Protocol::TypeDict
	Dictionary_2_t3407023366 * ___TypeDict_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,ExitGames.Client.Photon.CustomType> ExitGames.Client.Photon.Protocol::CodeDict
	Dictionary_2_t844425476 * ___CodeDict_1;
	// System.Single[] ExitGames.Client.Photon.Protocol::memFloatBlock
	SingleU5BU5D_t577127397* ___memFloatBlock_2;
	// System.Byte[] ExitGames.Client.Photon.Protocol::memDeserialize
	ByteU5BU5D_t3397334013* ___memDeserialize_3;

public:
	inline static int32_t get_offset_of_TypeDict_0() { return static_cast<int32_t>(offsetof(Protocol_t1307984734_StaticFields, ___TypeDict_0)); }
	inline Dictionary_2_t3407023366 * get_TypeDict_0() const { return ___TypeDict_0; }
	inline Dictionary_2_t3407023366 ** get_address_of_TypeDict_0() { return &___TypeDict_0; }
	inline void set_TypeDict_0(Dictionary_2_t3407023366 * value)
	{
		___TypeDict_0 = value;
		Il2CppCodeGenWriteBarrier(&___TypeDict_0, value);
	}

	inline static int32_t get_offset_of_CodeDict_1() { return static_cast<int32_t>(offsetof(Protocol_t1307984734_StaticFields, ___CodeDict_1)); }
	inline Dictionary_2_t844425476 * get_CodeDict_1() const { return ___CodeDict_1; }
	inline Dictionary_2_t844425476 ** get_address_of_CodeDict_1() { return &___CodeDict_1; }
	inline void set_CodeDict_1(Dictionary_2_t844425476 * value)
	{
		___CodeDict_1 = value;
		Il2CppCodeGenWriteBarrier(&___CodeDict_1, value);
	}

	inline static int32_t get_offset_of_memFloatBlock_2() { return static_cast<int32_t>(offsetof(Protocol_t1307984734_StaticFields, ___memFloatBlock_2)); }
	inline SingleU5BU5D_t577127397* get_memFloatBlock_2() const { return ___memFloatBlock_2; }
	inline SingleU5BU5D_t577127397** get_address_of_memFloatBlock_2() { return &___memFloatBlock_2; }
	inline void set_memFloatBlock_2(SingleU5BU5D_t577127397* value)
	{
		___memFloatBlock_2 = value;
		Il2CppCodeGenWriteBarrier(&___memFloatBlock_2, value);
	}

	inline static int32_t get_offset_of_memDeserialize_3() { return static_cast<int32_t>(offsetof(Protocol_t1307984734_StaticFields, ___memDeserialize_3)); }
	inline ByteU5BU5D_t3397334013* get_memDeserialize_3() const { return ___memDeserialize_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_memDeserialize_3() { return &___memDeserialize_3; }
	inline void set_memDeserialize_3(ByteU5BU5D_t3397334013* value)
	{
		___memDeserialize_3 = value;
		Il2CppCodeGenWriteBarrier(&___memDeserialize_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
