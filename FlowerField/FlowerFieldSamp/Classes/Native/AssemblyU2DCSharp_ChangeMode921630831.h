﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeMode
struct  ChangeMode_t921630831  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ChangeMode::Camera
	GameObject_t1756533147 * ___Camera_2;
	// UnityEngine.GameObject ChangeMode::inputModeCanvas
	GameObject_t1756533147 * ___inputModeCanvas_3;
	// UnityEngine.GameObject ChangeMode::viewModeCanvas
	GameObject_t1756533147 * ___viewModeCanvas_4;
	// System.Single ChangeMode::lookingInputMode
	float ___lookingInputMode_5;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(ChangeMode_t921630831, ___Camera_2)); }
	inline GameObject_t1756533147 * get_Camera_2() const { return ___Camera_2; }
	inline GameObject_t1756533147 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(GameObject_t1756533147 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier(&___Camera_2, value);
	}

	inline static int32_t get_offset_of_inputModeCanvas_3() { return static_cast<int32_t>(offsetof(ChangeMode_t921630831, ___inputModeCanvas_3)); }
	inline GameObject_t1756533147 * get_inputModeCanvas_3() const { return ___inputModeCanvas_3; }
	inline GameObject_t1756533147 ** get_address_of_inputModeCanvas_3() { return &___inputModeCanvas_3; }
	inline void set_inputModeCanvas_3(GameObject_t1756533147 * value)
	{
		___inputModeCanvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___inputModeCanvas_3, value);
	}

	inline static int32_t get_offset_of_viewModeCanvas_4() { return static_cast<int32_t>(offsetof(ChangeMode_t921630831, ___viewModeCanvas_4)); }
	inline GameObject_t1756533147 * get_viewModeCanvas_4() const { return ___viewModeCanvas_4; }
	inline GameObject_t1756533147 ** get_address_of_viewModeCanvas_4() { return &___viewModeCanvas_4; }
	inline void set_viewModeCanvas_4(GameObject_t1756533147 * value)
	{
		___viewModeCanvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___viewModeCanvas_4, value);
	}

	inline static int32_t get_offset_of_lookingInputMode_5() { return static_cast<int32_t>(offsetof(ChangeMode_t921630831, ___lookingInputMode_5)); }
	inline float get_lookingInputMode_5() const { return ___lookingInputMode_5; }
	inline float* get_address_of_lookingInputMode_5() { return &___lookingInputMode_5; }
	inline void set_lookingInputMode_5(float value)
	{
		___lookingInputMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
