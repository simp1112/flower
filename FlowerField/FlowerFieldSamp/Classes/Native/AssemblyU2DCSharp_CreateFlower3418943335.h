﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Photon_MonoBehaviour3914164484.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateFlower
struct  CreateFlower_t3418943335  : public MonoBehaviour_t3914164484
{
public:
	// System.Single CreateFlower::CreateCount
	float ___CreateCount_3;
	// System.Single CreateFlower::FlowerPosX
	float ___FlowerPosX_4;
	// UnityEngine.GameObject CreateFlower::CreateButton
	GameObject_t1756533147 * ___CreateButton_5;

public:
	inline static int32_t get_offset_of_CreateCount_3() { return static_cast<int32_t>(offsetof(CreateFlower_t3418943335, ___CreateCount_3)); }
	inline float get_CreateCount_3() const { return ___CreateCount_3; }
	inline float* get_address_of_CreateCount_3() { return &___CreateCount_3; }
	inline void set_CreateCount_3(float value)
	{
		___CreateCount_3 = value;
	}

	inline static int32_t get_offset_of_FlowerPosX_4() { return static_cast<int32_t>(offsetof(CreateFlower_t3418943335, ___FlowerPosX_4)); }
	inline float get_FlowerPosX_4() const { return ___FlowerPosX_4; }
	inline float* get_address_of_FlowerPosX_4() { return &___FlowerPosX_4; }
	inline void set_FlowerPosX_4(float value)
	{
		___FlowerPosX_4 = value;
	}

	inline static int32_t get_offset_of_CreateButton_5() { return static_cast<int32_t>(offsetof(CreateFlower_t3418943335, ___CreateButton_5)); }
	inline GameObject_t1756533147 * get_CreateButton_5() const { return ___CreateButton_5; }
	inline GameObject_t1756533147 ** get_address_of_CreateButton_5() { return &___CreateButton_5; }
	inline void set_CreateButton_5(GameObject_t1756533147 * value)
	{
		___CreateButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___CreateButton_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
