﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// goInputMode
struct  goInputMode_t2968674023  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject goInputMode::inputImageCanvas
	GameObject_t1756533147 * ___inputImageCanvas_2;
	// UnityEngine.GameObject goInputMode::collectModeCanvas
	GameObject_t1756533147 * ___collectModeCanvas_3;
	// UnityEngine.GameObject goInputMode::viewModeCanvas
	GameObject_t1756533147 * ___viewModeCanvas_4;

public:
	inline static int32_t get_offset_of_inputImageCanvas_2() { return static_cast<int32_t>(offsetof(goInputMode_t2968674023, ___inputImageCanvas_2)); }
	inline GameObject_t1756533147 * get_inputImageCanvas_2() const { return ___inputImageCanvas_2; }
	inline GameObject_t1756533147 ** get_address_of_inputImageCanvas_2() { return &___inputImageCanvas_2; }
	inline void set_inputImageCanvas_2(GameObject_t1756533147 * value)
	{
		___inputImageCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___inputImageCanvas_2, value);
	}

	inline static int32_t get_offset_of_collectModeCanvas_3() { return static_cast<int32_t>(offsetof(goInputMode_t2968674023, ___collectModeCanvas_3)); }
	inline GameObject_t1756533147 * get_collectModeCanvas_3() const { return ___collectModeCanvas_3; }
	inline GameObject_t1756533147 ** get_address_of_collectModeCanvas_3() { return &___collectModeCanvas_3; }
	inline void set_collectModeCanvas_3(GameObject_t1756533147 * value)
	{
		___collectModeCanvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectModeCanvas_3, value);
	}

	inline static int32_t get_offset_of_viewModeCanvas_4() { return static_cast<int32_t>(offsetof(goInputMode_t2968674023, ___viewModeCanvas_4)); }
	inline GameObject_t1756533147 * get_viewModeCanvas_4() const { return ___viewModeCanvas_4; }
	inline GameObject_t1756533147 ** get_address_of_viewModeCanvas_4() { return &___viewModeCanvas_4; }
	inline void set_viewModeCanvas_4(GameObject_t1756533147 * value)
	{
		___viewModeCanvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___viewModeCanvas_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
