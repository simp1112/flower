﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_Demo2DJumpAndRun3867174783.h"
#include "AssemblyU2DCSharp_JumpAndRunMovement1247290497.h"
#include "AssemblyU2DCSharp_ColorPerPlayer3468414963.h"
#include "AssemblyU2DCSharp_ColorPerPlayerApply1602831959.h"
#include "AssemblyU2DCSharp_DemoBoxesGui1886963857.h"
#include "AssemblyU2DCSharp_DemoBoxesGui_U3CSwapTipU3Ec__Ite1221830033.h"
#include "AssemblyU2DCSharp_OnAwakePhysicsSettings211072656.h"
#include "AssemblyU2DCSharp_OnClickFlashRpc4256173558.h"
#include "AssemblyU2DCSharp_OnClickFlashRpc_U3CFlashU3Ec__It3298883714.h"
#include "AssemblyU2DCSharp_OnDoubleclickDestroy1672824578.h"
#include "AssemblyU2DCSharp_ClickAndDrag3685540827.h"
#include "AssemblyU2DCSharp_DemoOwnershipGui3074530025.h"
#include "AssemblyU2DCSharp_HighlightOwnedGameObj87578320.h"
#include "AssemblyU2DCSharp_InstantiateCube1938453577.h"
#include "AssemblyU2DCSharp_MaterialPerOwner1592108355.h"
#include "AssemblyU2DCSharp_OnClickDisableObj3388396834.h"
#include "AssemblyU2DCSharp_OnClickRequestOwnership527990915.h"
#include "AssemblyU2DCSharp_OnClickRightDestroy2493671783.h"
#include "AssemblyU2DCSharp_ShowInfoOfPlayer4185712249.h"
#include "AssemblyU2DCSharp_ChannelSelector4169853654.h"
#include "AssemblyU2DCSharp_ChatAppIdCheckerUI2770724865.h"
#include "AssemblyU2DCSharp_ChatGui1028549327.h"
#include "AssemblyU2DCSharp_FriendItem4118685085.h"
#include "AssemblyU2DCSharp_IgnoreUiRaycastWhenInactive3799487598.h"
#include "AssemblyU2DCSharp_NamePickGui2446596115.h"
#include "AssemblyU2DCSharp_GUICustomAuth406084710.h"
#include "AssemblyU2DCSharp_GUICustomAuth_GuiState201418673.h"
#include "AssemblyU2DCSharp_GUIFriendFinding1375409624.h"
#include "AssemblyU2DCSharp_GUIFriendsInRoom2264033788.h"
#include "AssemblyU2DCSharp_OnClickCallMethod4200492266.h"
#include "AssemblyU2DCSharp_HubGui2401392858.h"
#include "AssemblyU2DCSharp_HubGui_DemoBtn4078322204.h"
#include "AssemblyU2DCSharp_MoveCam451587648.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoHubManager3056509599.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoHubManager_D3185271366.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_ToDemoHubButton2519611493.h"
#include "AssemblyU2DCSharp_ToHubButton1019813820.h"
#include "AssemblyU2DCSharp_DemoMecanimGUI1698114554.h"
#include "AssemblyU2DCSharp_MessageOverlay4042034875.h"
#include "AssemblyU2DCSharp_OnCollideSwitchTeam3159386164.h"
#include "AssemblyU2DCSharp_OnPickedUpScript2405509937.h"
#include "AssemblyU2DCSharp_PickupCamera291866563.h"
#include "AssemblyU2DCSharp_PickupCharacterState2669369948.h"
#include "AssemblyU2DCSharp_PickupController3031549840.h"
#include "AssemblyU2DCSharp_PickupDemoGui3184520662.h"
#include "AssemblyU2DCSharp_PickupTriggerForward3629707929.h"
#include "AssemblyU2DCSharp_DemoRPGMovement2452842063.h"
#include "AssemblyU2DCSharp_RPGCamera369834252.h"
#include "AssemblyU2DCSharp_RPGMovement2231436662.h"
#include "AssemblyU2DCSharp_RpsCore474941484.h"
#include "AssemblyU2DCSharp_RpsCore_Hand3764893688.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2000[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2002[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2006[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2008[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2009[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2022[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2028[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2033[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2045[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (Demo2DJumpAndRun_t3867174783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (JumpAndRunMovement_t1247290497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[6] = 
{
	JumpAndRunMovement_t1247290497::get_offset_of_Speed_2(),
	JumpAndRunMovement_t1247290497::get_offset_of_JumpForce_3(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_Animator_4(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_Body_5(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_PhotonView_6(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_IsGrounded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (ColorPerPlayer_t3468414963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[8] = 
{
	ColorPerPlayer_t3468414963::get_offset_of_Colors_3(),
	0,
	ColorPerPlayer_t3468414963::get_offset_of_ShowColorLabel_5(),
	ColorPerPlayer_t3468414963::get_offset_of_ColorLabelArea_6(),
	ColorPerPlayer_t3468414963::get_offset_of_img_7(),
	ColorPerPlayer_t3468414963::get_offset_of_MyColor_8(),
	ColorPerPlayer_t3468414963::get_offset_of_U3CColorPickedU3Ek__BackingField_9(),
	ColorPerPlayer_t3468414963::get_offset_of_isInitialized_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ColorPerPlayerApply_t1602831959), -1, sizeof(ColorPerPlayerApply_t1602831959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2052[3] = 
{
	ColorPerPlayerApply_t1602831959_StaticFields::get_offset_of_colorPickerCache_3(),
	ColorPerPlayerApply_t1602831959::get_offset_of_rendererComponent_4(),
	ColorPerPlayerApply_t1602831959::get_offset_of_isInitialized_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (DemoBoxesGui_t1886963857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[7] = 
{
	DemoBoxesGui_t1886963857::get_offset_of_HideUI_2(),
	DemoBoxesGui_t1886963857::get_offset_of_GuiTextForTips_3(),
	DemoBoxesGui_t1886963857::get_offset_of_tipsIndex_4(),
	DemoBoxesGui_t1886963857::get_offset_of_tips_5(),
	0,
	DemoBoxesGui_t1886963857::get_offset_of_timeSinceLastTip_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (U3CSwapTipU3Ec__Iterator0_t1221830033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[5] = 
{
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U3CalphaU3E__0_0(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24this_1(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24current_2(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24disposing_3(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (OnAwakePhysicsSettings_t211072656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (OnClickFlashRpc_t4256173558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[3] = 
{
	OnClickFlashRpc_t4256173558::get_offset_of_originalMaterial_3(),
	OnClickFlashRpc_t4256173558::get_offset_of_originalColor_4(),
	OnClickFlashRpc_t4256173558::get_offset_of_isFlashing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (U3CFlashU3Ec__Iterator0_t3298883714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[6] = 
{
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U3CfU3E__1_0(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U3ClerpedU3E__2_1(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24this_2(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24current_3(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24disposing_4(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (OnDoubleclickDestroy_t1672824578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[2] = 
{
	OnDoubleclickDestroy_t1672824578::get_offset_of_timeOfLastClick_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ClickAndDrag_t3685540827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[3] = 
{
	ClickAndDrag_t3685540827::get_offset_of_camOnPress_3(),
	ClickAndDrag_t3685540827::get_offset_of_following_4(),
	ClickAndDrag_t3685540827::get_offset_of_factor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (DemoOwnershipGui_t3074530025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[2] = 
{
	DemoOwnershipGui_t3074530025::get_offset_of_Skin_2(),
	DemoOwnershipGui_t3074530025::get_offset_of_TransferOwnershipOnRequest_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (HighlightOwnedGameObj_t87578320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[3] = 
{
	HighlightOwnedGameObj_t87578320::get_offset_of_PointerPrefab_3(),
	HighlightOwnedGameObj_t87578320::get_offset_of_Offset_4(),
	HighlightOwnedGameObj_t87578320::get_offset_of_markerTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (InstantiateCube_t1938453577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[3] = 
{
	InstantiateCube_t1938453577::get_offset_of_Prefab_2(),
	InstantiateCube_t1938453577::get_offset_of_InstantiateType_3(),
	InstantiateCube_t1938453577::get_offset_of_showGui_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (MaterialPerOwner_t1592108355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[2] = 
{
	MaterialPerOwner_t1592108355::get_offset_of_assignedColorForUserId_3(),
	MaterialPerOwner_t1592108355::get_offset_of_m_Renderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (OnClickDisableObj_t3388396834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (OnClickRequestOwnership_t527990915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (OnClickRightDestroy_t2493671783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (ShowInfoOfPlayer_t4185712249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[5] = 
{
	ShowInfoOfPlayer_t4185712249::get_offset_of_textGo_3(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_tm_4(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_CharacterSize_5(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_font_6(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_DisableOnOwnObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (ChannelSelector_t4169853654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[1] = 
{
	ChannelSelector_t4169853654::get_offset_of_Channel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (ChatAppIdCheckerUI_t2770724865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[1] = 
{
	ChatAppIdCheckerUI_t2770724865::get_offset_of_Description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (ChatGui_t1028549327), -1, sizeof(ChatGui_t1028549327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2070[23] = 
{
	ChatGui_t1028549327::get_offset_of_ChannelsToJoinOnConnect_2(),
	ChatGui_t1028549327::get_offset_of_FriendsList_3(),
	ChatGui_t1028549327::get_offset_of_HistoryLengthToFetch_4(),
	ChatGui_t1028549327::get_offset_of_U3CUserNameU3Ek__BackingField_5(),
	ChatGui_t1028549327::get_offset_of_selectedChannelName_6(),
	ChatGui_t1028549327::get_offset_of_chatClient_7(),
	ChatGui_t1028549327::get_offset_of_missingAppIdErrorPanel_8(),
	ChatGui_t1028549327::get_offset_of_ConnectingLabel_9(),
	ChatGui_t1028549327::get_offset_of_ChatPanel_10(),
	ChatGui_t1028549327::get_offset_of_UserIdFormPanel_11(),
	ChatGui_t1028549327::get_offset_of_InputFieldChat_12(),
	ChatGui_t1028549327::get_offset_of_CurrentChannelText_13(),
	ChatGui_t1028549327::get_offset_of_ChannelToggleToInstantiate_14(),
	ChatGui_t1028549327::get_offset_of_FriendListUiItemtoInstantiate_15(),
	ChatGui_t1028549327::get_offset_of_channelToggles_16(),
	ChatGui_t1028549327::get_offset_of_friendListItemLUT_17(),
	ChatGui_t1028549327::get_offset_of_ShowState_18(),
	ChatGui_t1028549327::get_offset_of_Title_19(),
	ChatGui_t1028549327::get_offset_of_StateText_20(),
	ChatGui_t1028549327::get_offset_of_UserIdText_21(),
	ChatGui_t1028549327_StaticFields::get_offset_of_HelpText_22(),
	ChatGui_t1028549327::get_offset_of_TestLength_23(),
	ChatGui_t1028549327::get_offset_of_testBytes_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (FriendItem_t4118685085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[3] = 
{
	FriendItem_t4118685085::get_offset_of_NameLabel_2(),
	FriendItem_t4118685085::get_offset_of_StatusLabel_3(),
	FriendItem_t4118685085::get_offset_of_Health_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (IgnoreUiRaycastWhenInactive_t3799487598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (NamePickGui_t2446596115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[3] = 
{
	0,
	NamePickGui_t2446596115::get_offset_of_chatNewComponent_3(),
	NamePickGui_t2446596115::get_offset_of_idInput_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (GUICustomAuth_t406084710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[6] = 
{
	GUICustomAuth_t406084710::get_offset_of_GuiRect_2(),
	GUICustomAuth_t406084710::get_offset_of_authName_3(),
	GUICustomAuth_t406084710::get_offset_of_authToken_4(),
	GUICustomAuth_t406084710::get_offset_of_authDebugMessage_5(),
	GUICustomAuth_t406084710::get_offset_of_guiState_6(),
	GUICustomAuth_t406084710::get_offset_of_RootOf3dButtons_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (GuiState_t201418673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2075[5] = 
{
	GuiState_t201418673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (GUIFriendFinding_t1375409624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[3] = 
{
	GUIFriendFinding_t1375409624::get_offset_of_friendListOfSomeCommunity_2(),
	GUIFriendFinding_t1375409624::get_offset_of_GuiRect_3(),
	GUIFriendFinding_t1375409624::get_offset_of_ExpectedUsers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (GUIFriendsInRoom_t2264033788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[1] = 
{
	GUIFriendsInRoom_t2264033788::get_offset_of_GuiRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (OnClickCallMethod_t4200492266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[2] = 
{
	OnClickCallMethod_t4200492266::get_offset_of_TargetGameObject_3(),
	OnClickCallMethod_t4200492266::get_offset_of_TargetMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (HubGui_t2401392858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[6] = 
{
	HubGui_t2401392858::get_offset_of_Skin_2(),
	HubGui_t2401392858::get_offset_of_scrollPos_3(),
	HubGui_t2401392858::get_offset_of_demoDescription_4(),
	HubGui_t2401392858::get_offset_of_demoBtn_5(),
	HubGui_t2401392858::get_offset_of_webLink_6(),
	HubGui_t2401392858::get_offset_of_m_Headline_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (DemoBtn_t4078322204)+ sizeof (Il2CppObject), sizeof(DemoBtn_t4078322204_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[2] = 
{
	DemoBtn_t4078322204::get_offset_of_Text_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoBtn_t4078322204::get_offset_of_Link_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (MoveCam_t451587648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[4] = 
{
	MoveCam_t451587648::get_offset_of_originalPos_2(),
	MoveCam_t451587648::get_offset_of_randomPos_3(),
	MoveCam_t451587648::get_offset_of_camTransform_4(),
	MoveCam_t451587648::get_offset_of_lookAt_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (DemoHubManager_t3056509599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[9] = 
{
	DemoHubManager_t3056509599::get_offset_of_TitleText_2(),
	DemoHubManager_t3056509599::get_offset_of_DescriptionText_3(),
	DemoHubManager_t3056509599::get_offset_of_OpenSceneButton_4(),
	DemoHubManager_t3056509599::get_offset_of_OpenTutorialLinkButton_5(),
	DemoHubManager_t3056509599::get_offset_of_OpenDocLinkButton_6(),
	DemoHubManager_t3056509599::get_offset_of_MainDemoWebLink_7(),
	DemoHubManager_t3056509599::get_offset_of__data_8(),
	DemoHubManager_t3056509599::get_offset_of_currentSelection_9(),
	DemoHubManager_t3056509599::get_offset_of_BugFixbounds_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (DemoData_t3185271366)+ sizeof (Il2CppObject), sizeof(DemoData_t3185271366_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[5] = 
{
	DemoData_t3185271366::get_offset_of_Title_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_Description_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_Scene_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_TutorialLink_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_DocLink_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (ToDemoHubButton_t2519611493), -1, sizeof(ToDemoHubButton_t2519611493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2084[2] = 
{
	ToDemoHubButton_t2519611493_StaticFields::get_offset_of_instance_2(),
	ToDemoHubButton_t2519611493::get_offset_of__canvasGroup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (ToHubButton_t1019813820), -1, sizeof(ToHubButton_t1019813820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2085[3] = 
{
	ToHubButton_t1019813820::get_offset_of_ButtonTexture_2(),
	ToHubButton_t1019813820::get_offset_of_ButtonRect_3(),
	ToHubButton_t1019813820_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (DemoMecanimGUI_t1698114554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[6] = 
{
	DemoMecanimGUI_t1698114554::get_offset_of_Skin_3(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_AnimatorView_4(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_RemoteAnimator_5(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_SlideIn_6(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_FoundPlayerSlideIn_7(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_IsOpen_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (MessageOverlay_t4042034875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[1] = 
{
	MessageOverlay_t4042034875::get_offset_of_Objects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (OnCollideSwitchTeam_t3159386164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[1] = 
{
	OnCollideSwitchTeam_t3159386164::get_offset_of_TeamToSwitchTo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (OnPickedUpScript_t2405509937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (PickupCamera_t291866563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[19] = 
{
	PickupCamera_t291866563::get_offset_of_cameraTransform_3(),
	PickupCamera_t291866563::get_offset_of__target_4(),
	PickupCamera_t291866563::get_offset_of_distance_5(),
	PickupCamera_t291866563::get_offset_of_height_6(),
	PickupCamera_t291866563::get_offset_of_angularSmoothLag_7(),
	PickupCamera_t291866563::get_offset_of_angularMaxSpeed_8(),
	PickupCamera_t291866563::get_offset_of_heightSmoothLag_9(),
	PickupCamera_t291866563::get_offset_of_snapSmoothLag_10(),
	PickupCamera_t291866563::get_offset_of_snapMaxSpeed_11(),
	PickupCamera_t291866563::get_offset_of_clampHeadPositionScreenSpace_12(),
	PickupCamera_t291866563::get_offset_of_lockCameraTimeout_13(),
	PickupCamera_t291866563::get_offset_of_headOffset_14(),
	PickupCamera_t291866563::get_offset_of_centerOffset_15(),
	PickupCamera_t291866563::get_offset_of_heightVelocity_16(),
	PickupCamera_t291866563::get_offset_of_angleVelocity_17(),
	PickupCamera_t291866563::get_offset_of_snap_18(),
	PickupCamera_t291866563::get_offset_of_controller_19(),
	PickupCamera_t291866563::get_offset_of_targetHeight_20(),
	PickupCamera_t291866563::get_offset_of_m_CameraTransformCamera_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (PickupCharacterState_t2669369948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2091[6] = 
{
	PickupCharacterState_t2669369948::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (PickupController_t3031549840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[45] = 
{
	PickupController_t3031549840::get_offset_of_idleAnimation_2(),
	PickupController_t3031549840::get_offset_of_walkAnimation_3(),
	PickupController_t3031549840::get_offset_of_runAnimation_4(),
	PickupController_t3031549840::get_offset_of_jumpPoseAnimation_5(),
	PickupController_t3031549840::get_offset_of_walkMaxAnimationSpeed_6(),
	PickupController_t3031549840::get_offset_of_trotMaxAnimationSpeed_7(),
	PickupController_t3031549840::get_offset_of_runMaxAnimationSpeed_8(),
	PickupController_t3031549840::get_offset_of_jumpAnimationSpeed_9(),
	PickupController_t3031549840::get_offset_of_landAnimationSpeed_10(),
	PickupController_t3031549840::get_offset_of__animation_11(),
	PickupController_t3031549840::get_offset_of__characterState_12(),
	PickupController_t3031549840::get_offset_of_walkSpeed_13(),
	PickupController_t3031549840::get_offset_of_trotSpeed_14(),
	PickupController_t3031549840::get_offset_of_runSpeed_15(),
	PickupController_t3031549840::get_offset_of_inAirControlAcceleration_16(),
	PickupController_t3031549840::get_offset_of_jumpHeight_17(),
	PickupController_t3031549840::get_offset_of_gravity_18(),
	PickupController_t3031549840::get_offset_of_speedSmoothing_19(),
	PickupController_t3031549840::get_offset_of_rotateSpeed_20(),
	PickupController_t3031549840::get_offset_of_trotAfterSeconds_21(),
	PickupController_t3031549840::get_offset_of_canJump_22(),
	PickupController_t3031549840::get_offset_of_jumpRepeatTime_23(),
	PickupController_t3031549840::get_offset_of_jumpTimeout_24(),
	PickupController_t3031549840::get_offset_of_groundedTimeout_25(),
	PickupController_t3031549840::get_offset_of_lockCameraTimer_26(),
	PickupController_t3031549840::get_offset_of_moveDirection_27(),
	PickupController_t3031549840::get_offset_of_verticalSpeed_28(),
	PickupController_t3031549840::get_offset_of_moveSpeed_29(),
	PickupController_t3031549840::get_offset_of_collisionFlags_30(),
	PickupController_t3031549840::get_offset_of_jumping_31(),
	PickupController_t3031549840::get_offset_of_jumpingReachedApex_32(),
	PickupController_t3031549840::get_offset_of_movingBack_33(),
	PickupController_t3031549840::get_offset_of_isMoving_34(),
	PickupController_t3031549840::get_offset_of_walkTimeStart_35(),
	PickupController_t3031549840::get_offset_of_lastJumpButtonTime_36(),
	PickupController_t3031549840::get_offset_of_lastJumpTime_37(),
	PickupController_t3031549840::get_offset_of_inAirVelocity_38(),
	PickupController_t3031549840::get_offset_of_lastGroundedTime_39(),
	PickupController_t3031549840::get_offset_of_velocity_40(),
	PickupController_t3031549840::get_offset_of_lastPos_41(),
	PickupController_t3031549840::get_offset_of_remotePosition_42(),
	PickupController_t3031549840::get_offset_of_isControllable_43(),
	PickupController_t3031549840::get_offset_of_DoRotate_44(),
	PickupController_t3031549840::get_offset_of_RemoteSmoothing_45(),
	PickupController_t3031549840::get_offset_of_AssignAsTagObject_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (PickupDemoGui_t3184520662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[4] = 
{
	PickupDemoGui_t3184520662::get_offset_of_ShowScores_2(),
	PickupDemoGui_t3184520662::get_offset_of_ShowDropButton_3(),
	PickupDemoGui_t3184520662::get_offset_of_ShowTeams_4(),
	PickupDemoGui_t3184520662::get_offset_of_DropOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (PickupTriggerForward_t3629707929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (DemoRPGMovement_t2452842063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[1] = 
{
	DemoRPGMovement_t2452842063::get_offset_of_Camera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (RPGCamera_t369834252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[9] = 
{
	RPGCamera_t369834252::get_offset_of_Target_2(),
	RPGCamera_t369834252::get_offset_of_MaximumDistance_3(),
	RPGCamera_t369834252::get_offset_of_MinimumDistance_4(),
	RPGCamera_t369834252::get_offset_of_ScrollModifier_5(),
	RPGCamera_t369834252::get_offset_of_TurnModifier_6(),
	RPGCamera_t369834252::get_offset_of_m_CameraTransform_7(),
	RPGCamera_t369834252::get_offset_of_m_LookAtPoint_8(),
	RPGCamera_t369834252::get_offset_of_m_LocalForwardVector_9(),
	RPGCamera_t369834252::get_offset_of_m_Distance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (RPGMovement_t2231436662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[12] = 
{
	RPGMovement_t2231436662::get_offset_of_ForwardSpeed_2(),
	RPGMovement_t2231436662::get_offset_of_BackwardSpeed_3(),
	RPGMovement_t2231436662::get_offset_of_StrafeSpeed_4(),
	RPGMovement_t2231436662::get_offset_of_RotateSpeed_5(),
	RPGMovement_t2231436662::get_offset_of_m_CharacterController_6(),
	RPGMovement_t2231436662::get_offset_of_m_LastPosition_7(),
	RPGMovement_t2231436662::get_offset_of_m_Animator_8(),
	RPGMovement_t2231436662::get_offset_of_m_PhotonView_9(),
	RPGMovement_t2231436662::get_offset_of_m_TransformView_10(),
	RPGMovement_t2231436662::get_offset_of_m_AnimatorSpeed_11(),
	RPGMovement_t2231436662::get_offset_of_m_CurrentMovement_12(),
	RPGMovement_t2231436662::get_offset_of_m_CurrentTurnSpeed_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (RpsCore_t474941484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[24] = 
{
	RpsCore_t474941484::get_offset_of_ConnectUiView_3(),
	RpsCore_t474941484::get_offset_of_GameUiView_4(),
	RpsCore_t474941484::get_offset_of_ButtonCanvasGroup_5(),
	RpsCore_t474941484::get_offset_of_TimerFillImage_6(),
	RpsCore_t474941484::get_offset_of_TurnText_7(),
	RpsCore_t474941484::get_offset_of_TimeText_8(),
	RpsCore_t474941484::get_offset_of_RemotePlayerText_9(),
	RpsCore_t474941484::get_offset_of_LocalPlayerText_10(),
	RpsCore_t474941484::get_offset_of_WinOrLossImage_11(),
	RpsCore_t474941484::get_offset_of_localSelectionImage_12(),
	RpsCore_t474941484::get_offset_of_localSelection_13(),
	RpsCore_t474941484::get_offset_of_remoteSelectionImage_14(),
	RpsCore_t474941484::get_offset_of_remoteSelection_15(),
	RpsCore_t474941484::get_offset_of_SelectedRock_16(),
	RpsCore_t474941484::get_offset_of_SelectedPaper_17(),
	RpsCore_t474941484::get_offset_of_SelectedScissors_18(),
	RpsCore_t474941484::get_offset_of_SpriteWin_19(),
	RpsCore_t474941484::get_offset_of_SpriteLose_20(),
	RpsCore_t474941484::get_offset_of_SpriteDraw_21(),
	RpsCore_t474941484::get_offset_of_DisconnectedPanel_22(),
	RpsCore_t474941484::get_offset_of_result_23(),
	RpsCore_t474941484::get_offset_of_turnManager_24(),
	RpsCore_t474941484::get_offset_of_randomHand_25(),
	RpsCore_t474941484::get_offset_of_IsShowingResults_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Hand_t3764893688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[5] = 
{
	Hand_t3764893688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
