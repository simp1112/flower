﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonCodes743513534.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase822653733.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_My1657508700.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_Con442361298.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_Eg3215798507.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_U32364778250.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_U32364778087.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogItem2374365153.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogRecei2470685347.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogReceive64766788.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogSentRe516191028.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer2873400856.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer_U3C876985080.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer_U32443069021.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer_U31441634991.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetChannel943840543.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_NCommand2133614299.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TPeer393296942.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TPeer_U3CU32091585694.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Serializati3585985662.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Serializatio399833188.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IProtocol4285598013.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_OperationRe3015313336.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_OperationRe3648537128.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EventData126381822.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SerializeMet368871939.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SerializeSt1159648263.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DeserializeM109063152.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DeserializeS212185564.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CustomType1469665469.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Protocol1307984734.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Protocol162332747579.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Protocol16_4193807259.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPing3196351980.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingMono3450024213.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingNativeS4076003259.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingNativeD2401029042.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocke2406574302.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocke3428191965.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IPhotonSocke429031990.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IPhotonSock1369263269.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IPhotonSock1369263270.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SocketUdp2014015958.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SocketTcp447931986.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SimulationI3416819542.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_NetworkSimu2323123337.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStat2470135126.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStats250795966.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EncryptorMana99719152.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EncryptorMa1736907404.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EncryptorMan173433802.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E__931465826.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_1381760539.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_1737990896.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_1825222500.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_2631791559.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E__748431829.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput621514313.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (PhotonCodes_t743513534), -1, sizeof(PhotonCodes_t743513534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	PhotonCodes_t743513534_StaticFields::get_offset_of_ClientKey_0(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_ModeKey_1(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_ServerKey_2(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_InitEncryption_3(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_Ping_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (PeerBase_t822653733), -1, sizeof(PeerBase_t822653733_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[52] = 
{
	PeerBase_t822653733::get_offset_of_ppeer_0(),
	PeerBase_t822653733::get_offset_of_protocol_1(),
	PeerBase_t822653733::get_offset_of_usedProtocol_2(),
	PeerBase_t822653733::get_offset_of_rt_3(),
	PeerBase_t822653733::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	PeerBase_t822653733::get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5(),
	PeerBase_t822653733::get_offset_of_ByteCountLastOperation_6(),
	PeerBase_t822653733::get_offset_of_ByteCountCurrentDispatch_7(),
	PeerBase_t822653733::get_offset_of_CommandInCurrentDispatch_8(),
	PeerBase_t822653733::get_offset_of_TrafficPackageHeaderSize_9(),
	PeerBase_t822653733::get_offset_of_packetLossByCrc_10(),
	PeerBase_t822653733::get_offset_of_packetLossByChallenge_11(),
	PeerBase_t822653733::get_offset_of_ActionQueue_12(),
	PeerBase_t822653733::get_offset_of_peerID_13(),
	PeerBase_t822653733::get_offset_of_peerConnectionState_14(),
	PeerBase_t822653733::get_offset_of_serverTimeOffset_15(),
	PeerBase_t822653733::get_offset_of_serverTimeOffsetIsAvailable_16(),
	PeerBase_t822653733::get_offset_of_roundTripTime_17(),
	PeerBase_t822653733::get_offset_of_roundTripTimeVariance_18(),
	PeerBase_t822653733::get_offset_of_lastRoundTripTime_19(),
	PeerBase_t822653733::get_offset_of_lowestRoundTripTime_20(),
	PeerBase_t822653733::get_offset_of_lastRoundTripTimeVariance_21(),
	PeerBase_t822653733::get_offset_of_highestRoundTripTimeVariance_22(),
	PeerBase_t822653733::get_offset_of_timestampOfLastReceive_23(),
	PeerBase_t822653733::get_offset_of_packetThrottleInterval_24(),
	PeerBase_t822653733_StaticFields::get_offset_of_peerCount_25(),
	PeerBase_t822653733::get_offset_of_bytesOut_26(),
	PeerBase_t822653733::get_offset_of_bytesIn_27(),
	PeerBase_t822653733::get_offset_of_commandBufferSize_28(),
	PeerBase_t822653733::get_offset_of_CryptoProvider_29(),
	PeerBase_t822653733::get_offset_of_lagRandomizer_30(),
	PeerBase_t822653733::get_offset_of_NetSimListOutgoing_31(),
	PeerBase_t822653733::get_offset_of_NetSimListIncoming_32(),
	PeerBase_t822653733::get_offset_of_networkSimulationSettings_33(),
	PeerBase_t822653733::get_offset_of_CommandLog_34(),
	PeerBase_t822653733::get_offset_of_InReliableLog_35(),
	PeerBase_t822653733::get_offset_of_CustomInitData_36(),
	PeerBase_t822653733::get_offset_of_AppId_37(),
	PeerBase_t822653733::get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_38(),
	PeerBase_t822653733::get_offset_of_timeBase_39(),
	PeerBase_t822653733::get_offset_of_timeInt_40(),
	PeerBase_t822653733::get_offset_of_timeoutInt_41(),
	PeerBase_t822653733::get_offset_of_timeLastAckReceive_42(),
	PeerBase_t822653733::get_offset_of_timeLastSendAck_43(),
	PeerBase_t822653733::get_offset_of_timeLastSendOutgoing_44(),
	0,
	0,
	0,
	PeerBase_t822653733::get_offset_of_ApplicationIsInitialized_48(),
	PeerBase_t822653733::get_offset_of_isEncryptionAvailable_49(),
	PeerBase_t822653733::get_offset_of_outgoingCommandsInStream_50(),
	PeerBase_t822653733::get_offset_of_SerializeMemStream_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (MyAction_t1657508700), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (ConnectionStateValue_t442361298)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[7] = 
{
	ConnectionStateValue_t442361298::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (EgMessageType_t3215798507)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[10] = 
{
	EgMessageType_t3215798507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (U3CU3Ec__DisplayClass145_0_t2364778250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[3] = 
{
	U3CU3Ec__DisplayClass145_0_t2364778250::get_offset_of_level_0(),
	U3CU3Ec__DisplayClass145_0_t2364778250::get_offset_of_debugReturn_1(),
	U3CU3Ec__DisplayClass145_0_t2364778250::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (U3CU3Ec__DisplayClass146_0_t2364778087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[2] = 
{
	U3CU3Ec__DisplayClass146_0_t2364778087::get_offset_of_statusValue_0(),
	U3CU3Ec__DisplayClass146_0_t2364778087::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (CmdLogItem_t2374365153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[5] = 
{
	CmdLogItem_t2374365153::get_offset_of_TimeInt_0(),
	CmdLogItem_t2374365153::get_offset_of_Channel_1(),
	CmdLogItem_t2374365153::get_offset_of_SequenceNumber_2(),
	CmdLogItem_t2374365153::get_offset_of_Rtt_3(),
	CmdLogItem_t2374365153::get_offset_of_Variance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (CmdLogReceivedReliable_t2470685347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[2] = 
{
	CmdLogReceivedReliable_t2470685347::get_offset_of_TimeSinceLastSend_5(),
	CmdLogReceivedReliable_t2470685347::get_offset_of_TimeSinceLastSendAck_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (CmdLogReceivedAck_t64766788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[1] = 
{
	CmdLogReceivedAck_t64766788::get_offset_of_ReceivedSentTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (CmdLogSentReliable_t516191028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	CmdLogSentReliable_t516191028::get_offset_of_Resend_5(),
	CmdLogSentReliable_t516191028::get_offset_of_RoundtripTimeout_6(),
	CmdLogSentReliable_t516191028::get_offset_of_Timeout_7(),
	CmdLogSentReliable_t516191028::get_offset_of_TriggeredTimeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (EnetPeer_t2873400856), -1, sizeof(EnetPeer_t2873400856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[21] = 
{
	EnetPeer_t2873400856_StaticFields::get_offset_of_HMAC_SIZE_52(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_BLOCK_SIZE_53(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_IV_SIZE_54(),
	EnetPeer_t2873400856::get_offset_of_sentReliableCommands_55(),
	EnetPeer_t2873400856::get_offset_of_outgoingAcknowledgementsPool_56(),
	EnetPeer_t2873400856::get_offset_of_windowSize_57(),
	EnetPeer_t2873400856::get_offset_of_udpCommandCount_58(),
	EnetPeer_t2873400856::get_offset_of_udpBuffer_59(),
	EnetPeer_t2873400856::get_offset_of_udpBufferIndex_60(),
	EnetPeer_t2873400856::get_offset_of_udpBufferLength_61(),
	EnetPeer_t2873400856::get_offset_of_bufferForEncryption_62(),
	EnetPeer_t2873400856::get_offset_of_challenge_63(),
	EnetPeer_t2873400856::get_offset_of_reliableCommandsRepeated_64(),
	EnetPeer_t2873400856::get_offset_of_reliableCommandsSent_65(),
	EnetPeer_t2873400856::get_offset_of_serverSentTime_66(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_udpHeader0xF3_67(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_messageHeader_68(),
	EnetPeer_t2873400856::get_offset_of_datagramEncryptedConnection_69(),
	EnetPeer_t2873400856::get_offset_of_channelArray_70(),
	EnetPeer_t2873400856::get_offset_of_commandsToRemove_71(),
	EnetPeer_t2873400856::get_offset_of_commandsToResend_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U3CU3Ec__DisplayClass56_0_t876985080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[2] = 
{
	U3CU3Ec__DisplayClass56_0_t876985080::get_offset_of_dataCopy_0(),
	U3CU3Ec__DisplayClass56_0_t876985080::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (U3CU3Ec__DisplayClass56_1_t2443069021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	U3CU3Ec__DisplayClass56_1_t2443069021::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass56_1_t2443069021::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (U3CU3Ec__DisplayClass62_0_t1441634991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[2] = 
{
	U3CU3Ec__DisplayClass62_0_t1441634991::get_offset_of_readCommand_0(),
	U3CU3Ec__DisplayClass62_0_t1441634991::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (EnetChannel_t943840543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[9] = 
{
	EnetChannel_t943840543::get_offset_of_ChannelNumber_0(),
	EnetChannel_t943840543::get_offset_of_incomingReliableCommandsList_1(),
	EnetChannel_t943840543::get_offset_of_incomingUnreliableCommandsList_2(),
	EnetChannel_t943840543::get_offset_of_outgoingReliableCommandsList_3(),
	EnetChannel_t943840543::get_offset_of_outgoingUnreliableCommandsList_4(),
	EnetChannel_t943840543::get_offset_of_incomingReliableSequenceNumber_5(),
	EnetChannel_t943840543::get_offset_of_incomingUnreliableSequenceNumber_6(),
	EnetChannel_t943840543::get_offset_of_outgoingReliableSequenceNumber_7(),
	EnetChannel_t943840543::get_offset_of_outgoingUnreliableSequenceNumber_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (NCommand_t2133614299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[49] = 
{
	NCommand_t2133614299::get_offset_of_commandFlags_0(),
	0,
	0,
	0,
	0,
	0,
	NCommand_t2133614299::get_offset_of_commandType_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t2133614299::get_offset_of_commandChannelID_17(),
	NCommand_t2133614299::get_offset_of_reliableSequenceNumber_18(),
	NCommand_t2133614299::get_offset_of_unreliableSequenceNumber_19(),
	NCommand_t2133614299::get_offset_of_unsequencedGroupNumber_20(),
	NCommand_t2133614299::get_offset_of_reservedByte_21(),
	NCommand_t2133614299::get_offset_of_startSequenceNumber_22(),
	NCommand_t2133614299::get_offset_of_fragmentCount_23(),
	NCommand_t2133614299::get_offset_of_fragmentNumber_24(),
	NCommand_t2133614299::get_offset_of_totalLength_25(),
	NCommand_t2133614299::get_offset_of_fragmentOffset_26(),
	NCommand_t2133614299::get_offset_of_fragmentsRemaining_27(),
	NCommand_t2133614299::get_offset_of_commandSentTime_28(),
	NCommand_t2133614299::get_offset_of_commandSentCount_29(),
	NCommand_t2133614299::get_offset_of_roundTripTimeout_30(),
	NCommand_t2133614299::get_offset_of_timeoutTime_31(),
	NCommand_t2133614299::get_offset_of_ackReceivedReliableSequenceNumber_32(),
	NCommand_t2133614299::get_offset_of_ackReceivedSentTime_33(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t2133614299::get_offset_of_Size_45(),
	NCommand_t2133614299::get_offset_of_commandHeader_46(),
	NCommand_t2133614299::get_offset_of_SizeOfHeader_47(),
	NCommand_t2133614299::get_offset_of_Payload_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (TPeer_t393296942), -1, sizeof(TPeer_t393296942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1817[8] = 
{
	TPeer_t393296942::get_offset_of_incomingList_52(),
	TPeer_t393296942::get_offset_of_outgoingStream_53(),
	TPeer_t393296942::get_offset_of_lastPingResult_54(),
	TPeer_t393296942::get_offset_of_pingRequest_55(),
	TPeer_t393296942_StaticFields::get_offset_of_tcpFramedMessageHead_56(),
	TPeer_t393296942_StaticFields::get_offset_of_tcpMsgHead_57(),
	TPeer_t393296942::get_offset_of_messageHeader_58(),
	TPeer_t393296942::get_offset_of_DoFraming_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (U3CU3Ec__DisplayClass30_0_t2091585694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[2] = 
{
	U3CU3Ec__DisplayClass30_0_t2091585694::get_offset_of_data_0(),
	U3CU3Ec__DisplayClass30_0_t2091585694::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (SerializationProtocol_t3585985662)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[2] = 
{
	SerializationProtocol_t3585985662::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (SerializationProtocolFactory_t399833188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (IProtocol_t4285598013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (OperationRequest_t3015313336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[2] = 
{
	OperationRequest_t3015313336::get_offset_of_OperationCode_0(),
	OperationRequest_t3015313336::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (OperationResponse_t3648537128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	OperationResponse_t3648537128::get_offset_of_OperationCode_0(),
	OperationResponse_t3648537128::get_offset_of_ReturnCode_1(),
	OperationResponse_t3648537128::get_offset_of_DebugMessage_2(),
	OperationResponse_t3648537128::get_offset_of_Parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (EventData_t126381822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[2] = 
{
	EventData_t126381822::get_offset_of_Code_0(),
	EventData_t126381822::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (SerializeMethod_t368871939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (SerializeStreamMethod_t1159648263), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (DeserializeMethod_t109063152), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (DeserializeStreamMethod_t212185564), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (CustomType_t1469665469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[6] = 
{
	CustomType_t1469665469::get_offset_of_Code_0(),
	CustomType_t1469665469::get_offset_of_Type_1(),
	CustomType_t1469665469::get_offset_of_SerializeFunction_2(),
	CustomType_t1469665469::get_offset_of_DeserializeFunction_3(),
	CustomType_t1469665469::get_offset_of_SerializeStreamFunction_4(),
	CustomType_t1469665469::get_offset_of_DeserializeStreamFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (Protocol_t1307984734), -1, sizeof(Protocol_t1307984734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1830[4] = 
{
	Protocol_t1307984734_StaticFields::get_offset_of_TypeDict_0(),
	Protocol_t1307984734_StaticFields::get_offset_of_CodeDict_1(),
	Protocol_t1307984734_StaticFields::get_offset_of_memFloatBlock_2(),
	Protocol_t1307984734_StaticFields::get_offset_of_memDeserialize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (Protocol16_t2332747579), -1, sizeof(Protocol16_t2332747579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[13] = 
{
	Protocol16_t2332747579::get_offset_of_versionBytes_0(),
	Protocol16_t2332747579::get_offset_of_memShort_1(),
	Protocol16_t2332747579::get_offset_of_memLongBlock_2(),
	Protocol16_t2332747579::get_offset_of_memLongBlockBytes_3(),
	Protocol16_t2332747579_StaticFields::get_offset_of_memFloatBlock_4(),
	Protocol16_t2332747579_StaticFields::get_offset_of_memFloatBlockBytes_5(),
	Protocol16_t2332747579::get_offset_of_memDoubleBlock_6(),
	Protocol16_t2332747579::get_offset_of_memDoubleBlockBytes_7(),
	Protocol16_t2332747579::get_offset_of_memInteger_8(),
	Protocol16_t2332747579::get_offset_of_memLong_9(),
	Protocol16_t2332747579::get_offset_of_memFloat_10(),
	Protocol16_t2332747579::get_offset_of_memDouble_11(),
	Protocol16_t2332747579::get_offset_of_memString_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (GpType_t4193807259)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1832[22] = 
{
	GpType_t4193807259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (PhotonPing_t3196351980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[6] = 
{
	PhotonPing_t3196351980::get_offset_of_DebugString_0(),
	PhotonPing_t3196351980::get_offset_of_Successful_1(),
	PhotonPing_t3196351980::get_offset_of_GotResult_2(),
	PhotonPing_t3196351980::get_offset_of_PingLength_3(),
	PhotonPing_t3196351980::get_offset_of_PingBytes_4(),
	PhotonPing_t3196351980::get_offset_of_PingId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (PingMono_t3450024213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[1] = 
{
	PingMono_t3450024213::get_offset_of_sock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (PingNativeStatic_t4076003259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (PingNativeDynamic_t2401029042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (PhotonSocketState_t2406574302)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1837[5] = 
{
	PhotonSocketState_t2406574302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (PhotonSocketError_t3428191965)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1838[5] = 
{
	PhotonSocketError_t3428191965::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (IPhotonSocket_t429031990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[9] = 
{
	IPhotonSocket_t429031990::get_offset_of_peerBase_0(),
	IPhotonSocket_t429031990::get_offset_of_Protocol_1(),
	IPhotonSocket_t429031990::get_offset_of_PollReceive_2(),
	IPhotonSocket_t429031990::get_offset_of_U3CStateU3Ek__BackingField_3(),
	IPhotonSocket_t429031990::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	IPhotonSocket_t429031990::get_offset_of_U3CServerPortU3Ek__BackingField_5(),
	IPhotonSocket_t429031990::get_offset_of_U3CAddressResolvedAsIpv6U3Ek__BackingField_6(),
	IPhotonSocket_t429031990::get_offset_of_U3CUrlProtocolU3Ek__BackingField_7(),
	IPhotonSocket_t429031990::get_offset_of_U3CUrlPathU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (U3CU3Ec__DisplayClass40_0_t1369263269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[2] = 
{
	U3CU3Ec__DisplayClass40_0_t1369263269::get_offset_of_inBufferCopy_0(),
	U3CU3Ec__DisplayClass40_0_t1369263269::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U3CU3Ec__DisplayClass40_1_t1369263270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[3] = 
{
	U3CU3Ec__DisplayClass40_1_t1369263270::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass40_1_t1369263270::get_offset_of_inBuffer_1(),
	U3CU3Ec__DisplayClass40_1_t1369263270::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (SocketUdp_t2014015958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[2] = 
{
	SocketUdp_t2014015958::get_offset_of_sock_9(),
	SocketUdp_t2014015958::get_offset_of_syncer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (SocketTcp_t447931986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[2] = 
{
	SocketTcp_t447931986::get_offset_of_sock_9(),
	SocketTcp_t447931986::get_offset_of_syncer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (SimulationItem_t3416819542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[4] = 
{
	SimulationItem_t3416819542::get_offset_of_stopw_0(),
	SimulationItem_t3416819542::get_offset_of_TimeToExecute_1(),
	SimulationItem_t3416819542::get_offset_of_ActionToExecute_2(),
	SimulationItem_t3416819542::get_offset_of_U3CDelayU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (NetworkSimulationSet_t2323123337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[12] = 
{
	NetworkSimulationSet_t2323123337::get_offset_of_isSimulationEnabled_0(),
	NetworkSimulationSet_t2323123337::get_offset_of_outgoingLag_1(),
	NetworkSimulationSet_t2323123337::get_offset_of_outgoingJitter_2(),
	NetworkSimulationSet_t2323123337::get_offset_of_outgoingLossPercentage_3(),
	NetworkSimulationSet_t2323123337::get_offset_of_incomingLag_4(),
	NetworkSimulationSet_t2323123337::get_offset_of_incomingJitter_5(),
	NetworkSimulationSet_t2323123337::get_offset_of_incomingLossPercentage_6(),
	NetworkSimulationSet_t2323123337::get_offset_of_peerBase_7(),
	NetworkSimulationSet_t2323123337::get_offset_of_netSimThread_8(),
	NetworkSimulationSet_t2323123337::get_offset_of_NetSimManualResetEvent_9(),
	NetworkSimulationSet_t2323123337::get_offset_of_U3CLostPackagesOutU3Ek__BackingField_10(),
	NetworkSimulationSet_t2323123337::get_offset_of_U3CLostPackagesInU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (TrafficStatsGameLevel_t2470135126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[16] = 
{
	TrafficStatsGameLevel_t2470135126::get_offset_of_timeOfLastDispatchCall_0(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_timeOfLastSendCall_1(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3COperationByteCountU3Ek__BackingField_2(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3COperationCountU3Ek__BackingField_3(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CResultByteCountU3Ek__BackingField_4(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CResultCountU3Ek__BackingField_5(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CEventByteCountU3Ek__BackingField_6(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CEventCountU3Ek__BackingField_7(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestOpResponseCallbackU3Ek__BackingField_8(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestEventCallbackU3Ek__BackingField_10(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestEventCallbackCodeU3Ek__BackingField_11(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (TrafficStats_t250795966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[13] = 
{
	TrafficStats_t250795966::get_offset_of_U3CPackageHeaderSizeU3Ek__BackingField_0(),
	TrafficStats_t250795966::get_offset_of_U3CReliableCommandCountU3Ek__BackingField_1(),
	TrafficStats_t250795966::get_offset_of_U3CUnreliableCommandCountU3Ek__BackingField_2(),
	TrafficStats_t250795966::get_offset_of_U3CFragmentCommandCountU3Ek__BackingField_3(),
	TrafficStats_t250795966::get_offset_of_U3CControlCommandCountU3Ek__BackingField_4(),
	TrafficStats_t250795966::get_offset_of_U3CTotalPacketCountU3Ek__BackingField_5(),
	TrafficStats_t250795966::get_offset_of_U3CTotalCommandsInPacketsU3Ek__BackingField_6(),
	TrafficStats_t250795966::get_offset_of_U3CReliableCommandBytesU3Ek__BackingField_7(),
	TrafficStats_t250795966::get_offset_of_U3CUnreliableCommandBytesU3Ek__BackingField_8(),
	TrafficStats_t250795966::get_offset_of_U3CFragmentCommandBytesU3Ek__BackingField_9(),
	TrafficStats_t250795966::get_offset_of_U3CControlCommandBytesU3Ek__BackingField_10(),
	TrafficStats_t250795966::get_offset_of_U3CTimestampOfLastAckU3Ek__BackingField_11(),
	TrafficStats_t250795966::get_offset_of_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (CryptoBase_t99719152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[5] = 
{
	0,
	0,
	0,
	CryptoBase_t99719152::get_offset_of_encryptor_3(),
	CryptoBase_t99719152::get_offset_of_hmacsha256_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (Encryptor_t1736907404), -1, sizeof(Encryptor_t1736907404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[1] = 
{
	Encryptor_t1736907404_StaticFields::get_offset_of_zeroBytes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Decryptor_t173433802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	Decryptor_t173433802::get_offset_of_IV_5(),
	Decryptor_t173433802::get_offset_of_readBuffer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U34989E5469B40416DC5AFB739C747E32B40CC5C77_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U349ECABA9727A1AF0636082C467485A1A9A04B669_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U36668D4903321030E42A6CE59AB96ADD9D0214FAC_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U367AFD2CE00BA1923E3B8BCACC08CA0B0B21C09F6_3(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U370AE3F6F18539B6C47CFF9F0D9672AEEBDBCDB4C_4(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_AEAF34DCCF141E917F02F7768DAEA80AA2B13B95_5(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_C033BD4351FBA3732545EA2E016D52B0FC3E69EC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (__StaticArrayInitTypeSizeU3D9_t931465826)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D9_t931465826 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (__StaticArrayInitTypeSizeU3D13_t1381760539)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D13_t1381760539 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (__StaticArrayInitTypeSizeU3D96_t1737990896)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D96_t1737990896 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (__StaticArrayInitTypeSizeU3D128_t1825222500)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D128_t1825222500 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (__StaticArrayInitTypeSizeU3D192_t2631791559)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D192_t2631791559 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (__StaticArrayInitTypeSizeU3D1212_t748431829)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D1212_t748431829 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1878[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1883[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1886[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[21] = 
{
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_3(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_hovered_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1893[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1894[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (BaseInput_t621514313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[6] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t1295781545::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t1295781545::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_12(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
