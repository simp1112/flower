﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_CloudRegionCode989201940.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Region
struct  Region_t1621871372  : public Il2CppObject
{
public:
	// CloudRegionCode Region::Code
	int32_t ___Code_0;
	// System.String Region::Cluster
	String_t* ___Cluster_1;
	// System.String Region::HostAndPort
	String_t* ___HostAndPort_2;
	// System.Int32 Region::Ping
	int32_t ___Ping_3;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(Region_t1621871372, ___Code_0)); }
	inline int32_t get_Code_0() const { return ___Code_0; }
	inline int32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(int32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Cluster_1() { return static_cast<int32_t>(offsetof(Region_t1621871372, ___Cluster_1)); }
	inline String_t* get_Cluster_1() const { return ___Cluster_1; }
	inline String_t** get_address_of_Cluster_1() { return &___Cluster_1; }
	inline void set_Cluster_1(String_t* value)
	{
		___Cluster_1 = value;
		Il2CppCodeGenWriteBarrier(&___Cluster_1, value);
	}

	inline static int32_t get_offset_of_HostAndPort_2() { return static_cast<int32_t>(offsetof(Region_t1621871372, ___HostAndPort_2)); }
	inline String_t* get_HostAndPort_2() const { return ___HostAndPort_2; }
	inline String_t** get_address_of_HostAndPort_2() { return &___HostAndPort_2; }
	inline void set_HostAndPort_2(String_t* value)
	{
		___HostAndPort_2 = value;
		Il2CppCodeGenWriteBarrier(&___HostAndPort_2, value);
	}

	inline static int32_t get_offset_of_Ping_3() { return static_cast<int32_t>(offsetof(Region_t1621871372, ___Ping_3)); }
	inline int32_t get_Ping_3() const { return ___Ping_3; }
	inline int32_t* get_address_of_Ping_3() { return &___Ping_3; }
	inline void set_Ping_3(int32_t value)
	{
		___Ping_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
