﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RpsCore_ResultType695753136.h"
#include "AssemblyU2DCSharp_RpsCore_U3CShowResultsBeginNextT1284374256.h"
#include "AssemblyU2DCSharp_RpsCore_U3CCycleRemoteHandCorout3693359940.h"
#include "AssemblyU2DCSharp_RpsDebug918692002.h"
#include "AssemblyU2DCSharp_RpsDemoConnect3312340854.h"
#include "AssemblyU2DCSharp_CubeExtra2071917625.h"
#include "AssemblyU2DCSharp_CubeInter627064331.h"
#include "AssemblyU2DCSharp_CubeInter_State3649530779.h"
#include "AssemblyU2DCSharp_CubeLerp4109879556.h"
#include "AssemblyU2DCSharp_DragToMove3509972354.h"
#include "AssemblyU2DCSharp_DragToMove_U3CRecordMouseU3Ec__I1192151892.h"
#include "AssemblyU2DCSharp_IELdemo2078752065.h"
#include "AssemblyU2DCSharp_ThirdPersonCamera2751132817.h"
#include "AssemblyU2DCSharp_CharacterState1314841520.h"
#include "AssemblyU2DCSharp_ThirdPersonController1841729452.h"
#include "AssemblyU2DCSharp_ThirdPersonNetwork41212330.h"
#include "AssemblyU2DCSharp_WorkerInGame3235698221.h"
#include "AssemblyU2DCSharp_WorkerMenu3574853087.h"
#include "AssemblyU2DCSharp_AudioRpc64563249.h"
#include "AssemblyU2DCSharp_ClickDetector591071706.h"
#include "AssemblyU2DCSharp_GameLogic2306947540.h"
#include "AssemblyU2DCSharp_myThirdPersonController936732382.h"
#include "AssemblyU2DCSharp_NetworkCharacter3306655953.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething2729402039.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething_ResourceTyp4210697809.h"
#include "AssemblyU2DCSharp_RandomMatchmaker1757474292.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Cam1885250270.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Gam2764997197.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Lau3975258832.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Loa4252622961.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Pla1544181735.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Play514296090.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Pla1608799526.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Play308274001.h"
#include "AssemblyU2DCSharp_IdleRunJump1415086481.h"
#include "AssemblyU2DCSharp_PlayerDiamond3535846257.h"
#include "AssemblyU2DCSharp_CustomTypes3265409152.h"
#include "AssemblyU2DCSharp_PhotonNetworkingMessage846703697.h"
#include "AssemblyU2DCSharp_PhotonLogLevel1525257006.h"
#include "AssemblyU2DCSharp_PhotonTargets112131816.h"
#include "AssemblyU2DCSharp_CloudRegionCode989201940.h"
#include "AssemblyU2DCSharp_CloudRegionFlag3778436051.h"
#include "AssemblyU2DCSharp_ConnectionState4147259307.h"
#include "AssemblyU2DCSharp_EncryptionMode2259972134.h"
#include "AssemblyU2DCSharp_EncryptionDataParameters3227721395.h"
#include "AssemblyU2DCSharp_Extensions612262650.h"
#include "AssemblyU2DCSharp_GameObjectExtensions1406008347.h"
#include "AssemblyU2DCSharp_FriendInfo3883672584.h"
#include "AssemblyU2DCSharp_ExitGames_Client_GUI_GizmoType3206103292.h"
#include "AssemblyU2DCSharp_ExitGames_Client_GUI_GizmoTypeDra271436531.h"
#include "AssemblyU2DCSharp_LoadBalancingPeer3951471977.h"
#include "AssemblyU2DCSharp_LoadBalancingPeer_RoomOptionBit2208905521.h"
#include "AssemblyU2DCSharp_OpJoinRandomRoomParams449785843.h"
#include "AssemblyU2DCSharp_EnterRoomParams2222988781.h"
#include "AssemblyU2DCSharp_ErrorCode4164829903.h"
#include "AssemblyU2DCSharp_ActorProperties3996677212.h"
#include "AssemblyU2DCSharp_GamePropertyKey574996222.h"
#include "AssemblyU2DCSharp_EventCode2490424903.h"
#include "AssemblyU2DCSharp_ParameterCode39909338.h"
#include "AssemblyU2DCSharp_OperationCode3935042484.h"
#include "AssemblyU2DCSharp_JoinMode2789942079.h"
#include "AssemblyU2DCSharp_MatchmakingMode160361017.h"
#include "AssemblyU2DCSharp_ReceiverGroup262744424.h"
#include "AssemblyU2DCSharp_EventCaching2031337087.h"
#include "AssemblyU2DCSharp_PropertyTypeFlag3878913881.h"
#include "AssemblyU2DCSharp_RoomOptions590971513.h"
#include "AssemblyU2DCSharp_RaiseEventOptions565739090.h"
#include "AssemblyU2DCSharp_LobbyType3334525524.h"
#include "AssemblyU2DCSharp_TypedLobby3014596312.h"
#include "AssemblyU2DCSharp_TypedLobbyInfo4102455292.h"
#include "AssemblyU2DCSharp_AuthModeOption918236656.h"
#include "AssemblyU2DCSharp_CustomAuthenticationType1962747993.h"
#include "AssemblyU2DCSharp_AuthenticationValues1430961670.h"
#include "AssemblyU2DCSharp_ClientState3876498872.h"
#include "AssemblyU2DCSharp_JoinType4282379012.h"
#include "AssemblyU2DCSharp_DisconnectCause1534558295.h"
#include "AssemblyU2DCSharp_ServerConnection1155372161.h"
#include "AssemblyU2DCSharp_NetworkingPeer3034011222.h"
#include "AssemblyU2DCSharp_Photon_MonoBehaviour3914164484.h"
#include "AssemblyU2DCSharp_Photon_PunBehaviour692890556.h"
#include "AssemblyU2DCSharp_PhotonMessageInfo13590565.h"
#include "AssemblyU2DCSharp_PunEvent2040493857.h"
#include "AssemblyU2DCSharp_PhotonStream2436786422.h"
#include "AssemblyU2DCSharp_SceneManagerHelper3890528889.h"
#include "AssemblyU2DCSharp_WebRpcResponse2303421426.h"
#include "AssemblyU2DCSharp_PhotonHandler3957736394.h"
#include "AssemblyU2DCSharp_PhotonHandler_U3CPingAvailableRe1897949406.h"
#include "AssemblyU2DCSharp_PhotonLagSimulationGui2028718096.h"
#include "AssemblyU2DCSharp_PhotonNetwork1563132424.h"
#include "AssemblyU2DCSharp_PhotonNetwork_EventCallback2860523912.h"
#include "AssemblyU2DCSharp_PhotonPlayer4120608827.h"
#include "AssemblyU2DCSharp_PhotonStatsGui1789448968.h"
#include "AssemblyU2DCSharp_PhotonStreamQueue4116903749.h"
#include "AssemblyU2DCSharp_ViewSynchronization3814158713.h"
#include "AssemblyU2DCSharp_OnSerializeTransform4060828641.h"
#include "AssemblyU2DCSharp_OnSerializeRigidBody320933176.h"
#include "AssemblyU2DCSharp_OwnershipOption2481125440.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (ResultType_t695753136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[5] = 
{
	ResultType_t695753136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[4] = 
{
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24this_0(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24current_1(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24disposing_2(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[4] = 
{
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24this_0(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24current_1(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24disposing_2(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (RpsDebug_t918692002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[2] = 
{
	RpsDebug_t918692002::get_offset_of_ConnectionDebugButton_2(),
	RpsDebug_t918692002::get_offset_of_ShowConnectionDebug_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (RpsDemoConnect_t3312340854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[6] = 
{
	RpsDemoConnect_t3312340854::get_offset_of_InputField_3(),
	RpsDemoConnect_t3312340854::get_offset_of_UserId_4(),
	RpsDemoConnect_t3312340854::get_offset_of_previousRoomPlayerPrefKey_5(),
	RpsDemoConnect_t3312340854::get_offset_of_previousRoom_6(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (CubeExtra_t2071917625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[5] = 
{
	CubeExtra_t2071917625::get_offset_of_Factor_3(),
	CubeExtra_t2071917625::get_offset_of_latestCorrectPos_4(),
	CubeExtra_t2071917625::get_offset_of_movementVector_5(),
	CubeExtra_t2071917625::get_offset_of_errorVector_6(),
	CubeExtra_t2071917625::get_offset_of_lastTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (CubeInter_t627064331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[3] = 
{
	CubeInter_t627064331::get_offset_of_m_BufferedState_3(),
	CubeInter_t627064331::get_offset_of_m_TimestampCount_4(),
	CubeInter_t627064331::get_offset_of_InterpolationDelay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (State_t3649530779)+ sizeof (Il2CppObject), sizeof(State_t3649530779 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2107[3] = 
{
	State_t3649530779::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	State_t3649530779::get_offset_of_pos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	State_t3649530779::get_offset_of_rot_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (CubeLerp_t4109879556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[3] = 
{
	CubeLerp_t4109879556::get_offset_of_latestCorrectPos_3(),
	CubeLerp_t4109879556::get_offset_of_onUpdatePos_4(),
	CubeLerp_t4109879556::get_offset_of_fraction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (DragToMove_t3509972354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[7] = 
{
	DragToMove_t3509972354::get_offset_of_speed_2(),
	DragToMove_t3509972354::get_offset_of_cubes_3(),
	DragToMove_t3509972354::get_offset_of_PositionsQueue_4(),
	DragToMove_t3509972354::get_offset_of_cubeStartPositions_5(),
	DragToMove_t3509972354::get_offset_of_nextPosIndex_6(),
	DragToMove_t3509972354::get_offset_of_lerpTime_7(),
	DragToMove_t3509972354::get_offset_of_recording_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (U3CRecordMouseU3Ec__Iterator0_t1192151892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[7] = 
{
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U3Cv3U3E__1_0(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U3CinputRayU3E__1_1(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U3ChitU3E__1_2(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24this_3(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24current_4(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24disposing_5(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (IELdemo_t2078752065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[1] = 
{
	IELdemo_t2078752065::get_offset_of_Skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (ThirdPersonCamera_t2751132817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[19] = 
{
	ThirdPersonCamera_t2751132817::get_offset_of_cameraTransform_2(),
	ThirdPersonCamera_t2751132817::get_offset_of__target_3(),
	ThirdPersonCamera_t2751132817::get_offset_of_distance_4(),
	ThirdPersonCamera_t2751132817::get_offset_of_height_5(),
	ThirdPersonCamera_t2751132817::get_offset_of_angularSmoothLag_6(),
	ThirdPersonCamera_t2751132817::get_offset_of_angularMaxSpeed_7(),
	ThirdPersonCamera_t2751132817::get_offset_of_heightSmoothLag_8(),
	ThirdPersonCamera_t2751132817::get_offset_of_snapSmoothLag_9(),
	ThirdPersonCamera_t2751132817::get_offset_of_snapMaxSpeed_10(),
	ThirdPersonCamera_t2751132817::get_offset_of_clampHeadPositionScreenSpace_11(),
	ThirdPersonCamera_t2751132817::get_offset_of_lockCameraTimeout_12(),
	ThirdPersonCamera_t2751132817::get_offset_of_headOffset_13(),
	ThirdPersonCamera_t2751132817::get_offset_of_centerOffset_14(),
	ThirdPersonCamera_t2751132817::get_offset_of_heightVelocity_15(),
	ThirdPersonCamera_t2751132817::get_offset_of_angleVelocity_16(),
	ThirdPersonCamera_t2751132817::get_offset_of_snap_17(),
	ThirdPersonCamera_t2751132817::get_offset_of_controller_18(),
	ThirdPersonCamera_t2751132817::get_offset_of_targetHeight_19(),
	ThirdPersonCamera_t2751132817::get_offset_of_m_CameraTransformCamera_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (CharacterState_t1314841520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[6] = 
{
	CharacterState_t1314841520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (ThirdPersonController_t1841729452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[41] = 
{
	ThirdPersonController_t1841729452::get_offset_of_idleAnimation_2(),
	ThirdPersonController_t1841729452::get_offset_of_walkAnimation_3(),
	ThirdPersonController_t1841729452::get_offset_of_runAnimation_4(),
	ThirdPersonController_t1841729452::get_offset_of_jumpPoseAnimation_5(),
	ThirdPersonController_t1841729452::get_offset_of_walkMaxAnimationSpeed_6(),
	ThirdPersonController_t1841729452::get_offset_of_trotMaxAnimationSpeed_7(),
	ThirdPersonController_t1841729452::get_offset_of_runMaxAnimationSpeed_8(),
	ThirdPersonController_t1841729452::get_offset_of_jumpAnimationSpeed_9(),
	ThirdPersonController_t1841729452::get_offset_of_landAnimationSpeed_10(),
	ThirdPersonController_t1841729452::get_offset_of__animation_11(),
	ThirdPersonController_t1841729452::get_offset_of__characterState_12(),
	ThirdPersonController_t1841729452::get_offset_of_walkSpeed_13(),
	ThirdPersonController_t1841729452::get_offset_of_trotSpeed_14(),
	ThirdPersonController_t1841729452::get_offset_of_runSpeed_15(),
	ThirdPersonController_t1841729452::get_offset_of_inAirControlAcceleration_16(),
	ThirdPersonController_t1841729452::get_offset_of_jumpHeight_17(),
	ThirdPersonController_t1841729452::get_offset_of_gravity_18(),
	ThirdPersonController_t1841729452::get_offset_of_speedSmoothing_19(),
	ThirdPersonController_t1841729452::get_offset_of_rotateSpeed_20(),
	ThirdPersonController_t1841729452::get_offset_of_trotAfterSeconds_21(),
	ThirdPersonController_t1841729452::get_offset_of_canJump_22(),
	ThirdPersonController_t1841729452::get_offset_of_jumpRepeatTime_23(),
	ThirdPersonController_t1841729452::get_offset_of_jumpTimeout_24(),
	ThirdPersonController_t1841729452::get_offset_of_groundedTimeout_25(),
	ThirdPersonController_t1841729452::get_offset_of_lockCameraTimer_26(),
	ThirdPersonController_t1841729452::get_offset_of_moveDirection_27(),
	ThirdPersonController_t1841729452::get_offset_of_verticalSpeed_28(),
	ThirdPersonController_t1841729452::get_offset_of_moveSpeed_29(),
	ThirdPersonController_t1841729452::get_offset_of_collisionFlags_30(),
	ThirdPersonController_t1841729452::get_offset_of_jumping_31(),
	ThirdPersonController_t1841729452::get_offset_of_jumpingReachedApex_32(),
	ThirdPersonController_t1841729452::get_offset_of_movingBack_33(),
	ThirdPersonController_t1841729452::get_offset_of_isMoving_34(),
	ThirdPersonController_t1841729452::get_offset_of_walkTimeStart_35(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpButtonTime_36(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpTime_37(),
	ThirdPersonController_t1841729452::get_offset_of_inAirVelocity_38(),
	ThirdPersonController_t1841729452::get_offset_of_lastGroundedTime_39(),
	ThirdPersonController_t1841729452::get_offset_of_isControllable_40(),
	ThirdPersonController_t1841729452::get_offset_of_lastPos_41(),
	ThirdPersonController_t1841729452::get_offset_of_velocity_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (ThirdPersonNetwork_t41212330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[5] = 
{
	ThirdPersonNetwork_t41212330::get_offset_of_cameraScript_3(),
	ThirdPersonNetwork_t41212330::get_offset_of_controllerScript_4(),
	ThirdPersonNetwork_t41212330::get_offset_of_firstTake_5(),
	ThirdPersonNetwork_t41212330::get_offset_of_correctPlayerPos_6(),
	ThirdPersonNetwork_t41212330::get_offset_of_correctPlayerRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (WorkerInGame_t3235698221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[1] = 
{
	WorkerInGame_t3235698221::get_offset_of_playerPrefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (WorkerMenu_t3574853087), -1, sizeof(WorkerMenu_t3574853087_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2117[9] = 
{
	WorkerMenu_t3574853087::get_offset_of_Skin_2(),
	WorkerMenu_t3574853087::get_offset_of_WidthAndHeight_3(),
	WorkerMenu_t3574853087::get_offset_of_roomName_4(),
	WorkerMenu_t3574853087::get_offset_of_scrollPos_5(),
	WorkerMenu_t3574853087::get_offset_of_connectFailed_6(),
	WorkerMenu_t3574853087_StaticFields::get_offset_of_SceneNameMenu_7(),
	WorkerMenu_t3574853087_StaticFields::get_offset_of_SceneNameGame_8(),
	WorkerMenu_t3574853087::get_offset_of_errorDialog_9(),
	WorkerMenu_t3574853087::get_offset_of_timeToClearDialog_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (AudioRpc_t64563249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[3] = 
{
	AudioRpc_t64563249::get_offset_of_marco_3(),
	AudioRpc_t64563249::get_offset_of_polo_4(),
	AudioRpc_t64563249::get_offset_of_m_Source_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (ClickDetector_t591071706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (GameLogic_t2306947540), -1, sizeof(GameLogic_t2306947540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[2] = 
{
	GameLogic_t2306947540_StaticFields::get_offset_of_playerWhoIsIt_2(),
	GameLogic_t2306947540_StaticFields::get_offset_of_ScenePhotonView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (myThirdPersonController_t936732382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (NetworkCharacter_t3306655953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[2] = 
{
	NetworkCharacter_t3306655953::get_offset_of_correctPlayerPos_3(),
	NetworkCharacter_t3306655953::get_offset_of_correctPlayerRot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (OnClickLoadSomething_t2729402039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[2] = 
{
	OnClickLoadSomething_t2729402039::get_offset_of_ResourceTypeToLoad_2(),
	OnClickLoadSomething_t2729402039::get_offset_of_ResourceToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (ResourceTypeOption_t4210697809)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2124[3] = 
{
	ResourceTypeOption_t4210697809::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (RandomMatchmaker_t1757474292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[1] = 
{
	RandomMatchmaker_t1757474292::get_offset_of_myPhotonView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (CameraWork_t1885250270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[9] = 
{
	CameraWork_t1885250270::get_offset_of_distance_2(),
	CameraWork_t1885250270::get_offset_of_height_3(),
	CameraWork_t1885250270::get_offset_of_heightSmoothLag_4(),
	CameraWork_t1885250270::get_offset_of_centerOffset_5(),
	CameraWork_t1885250270::get_offset_of_followOnStart_6(),
	CameraWork_t1885250270::get_offset_of_cameraTransform_7(),
	CameraWork_t1885250270::get_offset_of_isFollowing_8(),
	CameraWork_t1885250270::get_offset_of_heightVelocity_9(),
	CameraWork_t1885250270::get_offset_of_targetHeight_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (GameManager_t2764997197), -1, sizeof(GameManager_t2764997197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2127[3] = 
{
	GameManager_t2764997197_StaticFields::get_offset_of_Instance_3(),
	GameManager_t2764997197::get_offset_of_playerPrefab_4(),
	GameManager_t2764997197::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (Launcher_t3975258832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[6] = 
{
	Launcher_t3975258832::get_offset_of_controlPanel_3(),
	Launcher_t3975258832::get_offset_of_feedbackText_4(),
	Launcher_t3975258832::get_offset_of_maxPlayersPerRoom_5(),
	Launcher_t3975258832::get_offset_of_loaderAnime_6(),
	Launcher_t3975258832::get_offset_of_isConnecting_7(),
	Launcher_t3975258832::get_offset_of__gameVersion_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (LoaderAnime_t4252622961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[7] = 
{
	LoaderAnime_t4252622961::get_offset_of_speed_2(),
	LoaderAnime_t4252622961::get_offset_of_radius_3(),
	LoaderAnime_t4252622961::get_offset_of_particles_4(),
	LoaderAnime_t4252622961::get_offset_of__offset_5(),
	LoaderAnime_t4252622961::get_offset_of__transform_6(),
	LoaderAnime_t4252622961::get_offset_of__particleTransform_7(),
	LoaderAnime_t4252622961::get_offset_of__isAnimating_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (PlayerAnimatorManager_t1544181735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[2] = 
{
	PlayerAnimatorManager_t1544181735::get_offset_of_DirectionDampTime_3(),
	PlayerAnimatorManager_t1544181735::get_offset_of_animator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (PlayerManager_t514296090), -1, sizeof(PlayerManager_t514296090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[5] = 
{
	PlayerManager_t514296090::get_offset_of_PlayerUiPrefab_3(),
	PlayerManager_t514296090::get_offset_of_Beams_4(),
	PlayerManager_t514296090::get_offset_of_Health_5(),
	PlayerManager_t514296090_StaticFields::get_offset_of_LocalPlayerInstance_6(),
	PlayerManager_t514296090::get_offset_of_IsFiring_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (PlayerNameInputField_t1608799526), -1, sizeof(PlayerNameInputField_t1608799526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[1] = 
{
	PlayerNameInputField_t1608799526_StaticFields::get_offset_of_playerNamePrefKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (PlayerUI_t308274001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[8] = 
{
	PlayerUI_t308274001::get_offset_of_ScreenOffset_2(),
	PlayerUI_t308274001::get_offset_of_PlayerNameText_3(),
	PlayerUI_t308274001::get_offset_of_PlayerHealthSlider_4(),
	PlayerUI_t308274001::get_offset_of__target_5(),
	PlayerUI_t308274001::get_offset_of__characterControllerHeight_6(),
	PlayerUI_t308274001::get_offset_of__targetTransform_7(),
	PlayerUI_t308274001::get_offset_of__targetRenderer_8(),
	PlayerUI_t308274001::get_offset_of__targetPosition_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (IdleRunJump_t1415086481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[10] = 
{
	IdleRunJump_t1415086481::get_offset_of_animator_2(),
	IdleRunJump_t1415086481::get_offset_of_DirectionDampTime_3(),
	IdleRunJump_t1415086481::get_offset_of_ApplyGravity_4(),
	IdleRunJump_t1415086481::get_offset_of_SynchronizedMaxSpeed_5(),
	IdleRunJump_t1415086481::get_offset_of_TurnSpeedModifier_6(),
	IdleRunJump_t1415086481::get_offset_of_SynchronizedTurnSpeed_7(),
	IdleRunJump_t1415086481::get_offset_of_SynchronizedSpeedAcceleration_8(),
	IdleRunJump_t1415086481::get_offset_of_m_PhotonView_9(),
	IdleRunJump_t1415086481::get_offset_of_m_TransformView_10(),
	IdleRunJump_t1415086481::get_offset_of_m_SpeedModifier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (PlayerDiamond_t3535846257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[6] = 
{
	PlayerDiamond_t3535846257::get_offset_of_HeadTransform_2(),
	PlayerDiamond_t3535846257::get_offset_of_HeightOffset_3(),
	PlayerDiamond_t3535846257::get_offset_of_m_PhotonView_4(),
	PlayerDiamond_t3535846257::get_offset_of_m_DiamondRenderer_5(),
	PlayerDiamond_t3535846257::get_offset_of_m_Rotation_6(),
	PlayerDiamond_t3535846257::get_offset_of_m_Height_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (CustomTypes_t3265409152), -1, sizeof(CustomTypes_t3265409152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2136[12] = 
{
	CustomTypes_t3265409152_StaticFields::get_offset_of_memVector3_0(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_memVector2_1(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_memQuarternion_2(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_memPlayer_3(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (PhotonNetworkingMessage_t846703697)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[31] = 
{
	PhotonNetworkingMessage_t846703697::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (PhotonLogLevel_t1525257006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2138[4] = 
{
	PhotonLogLevel_t1525257006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (PhotonTargets_t112131816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2139[8] = 
{
	PhotonTargets_t112131816::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (CloudRegionCode_t989201940)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2140[13] = 
{
	CloudRegionCode_t989201940::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (CloudRegionFlag_t3778436051)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[12] = 
{
	CloudRegionFlag_t3778436051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (ConnectionState_t4147259307)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2142[6] = 
{
	ConnectionState_t4147259307::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (EncryptionMode_t2259972134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2143[3] = 
{
	EncryptionMode_t2259972134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (EncryptionDataParameters_t3227721395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (Extensions_t612262650), -1, sizeof(Extensions_t612262650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[1] = 
{
	Extensions_t612262650_StaticFields::get_offset_of_ParametersOfMethods_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (GameObjectExtensions_t1406008347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (FriendInfo_t3883672584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[3] = 
{
	FriendInfo_t3883672584::get_offset_of_U3CNameU3Ek__BackingField_0(),
	FriendInfo_t3883672584::get_offset_of_U3CIsOnlineU3Ek__BackingField_1(),
	FriendInfo_t3883672584::get_offset_of_U3CRoomU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (GizmoType_t3206103292)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2148[5] = 
{
	GizmoType_t3206103292::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (GizmoTypeDrawer_t271436531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (LoadBalancingPeer_t3951471977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[1] = 
{
	LoadBalancingPeer_t3951471977::get_offset_of_opParameters_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (RoomOptionBit_t2208905521)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2151[7] = 
{
	RoomOptionBit_t2208905521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (OpJoinRandomRoomParams_t449785843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[6] = 
{
	OpJoinRandomRoomParams_t449785843::get_offset_of_ExpectedCustomRoomProperties_0(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_ExpectedMaxPlayers_1(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_MatchingType_2(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_TypedLobby_3(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_SqlLobbyFilter_4(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_ExpectedUsers_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (EnterRoomParams_t2222988781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[8] = 
{
	EnterRoomParams_t2222988781::get_offset_of_RoomName_0(),
	EnterRoomParams_t2222988781::get_offset_of_RoomOptions_1(),
	EnterRoomParams_t2222988781::get_offset_of_Lobby_2(),
	EnterRoomParams_t2222988781::get_offset_of_PlayerProperties_3(),
	EnterRoomParams_t2222988781::get_offset_of_OnGameServer_4(),
	EnterRoomParams_t2222988781::get_offset_of_CreateIfNotExists_5(),
	EnterRoomParams_t2222988781::get_offset_of_RejoinOnly_6(),
	EnterRoomParams_t2222988781::get_offset_of_ExpectedUsers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (ErrorCode_t4164829903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (ActorProperties_t3996677212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (GamePropertyKey_t574996222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (EventCode_t2490424903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (ParameterCode_t39909338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[67] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (OperationCode_t3935042484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (JoinMode_t2789942079)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[5] = 
{
	JoinMode_t2789942079::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (MatchmakingMode_t160361017)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2161[4] = 
{
	MatchmakingMode_t160361017::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (ReceiverGroup_t262744424)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[4] = 
{
	ReceiverGroup_t262744424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (EventCaching_t2031337087)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2163[13] = 
{
	EventCaching_t2031337087::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (PropertyTypeFlag_t3878913881)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2164[5] = 
{
	PropertyTypeFlag_t3878913881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (RoomOptions_t590971513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[12] = 
{
	RoomOptions_t590971513::get_offset_of_isVisibleField_0(),
	RoomOptions_t590971513::get_offset_of_isOpenField_1(),
	RoomOptions_t590971513::get_offset_of_MaxPlayers_2(),
	RoomOptions_t590971513::get_offset_of_PlayerTtl_3(),
	RoomOptions_t590971513::get_offset_of_EmptyRoomTtl_4(),
	RoomOptions_t590971513::get_offset_of_cleanupCacheOnLeaveField_5(),
	RoomOptions_t590971513::get_offset_of_CustomRoomProperties_6(),
	RoomOptions_t590971513::get_offset_of_CustomRoomPropertiesForLobby_7(),
	RoomOptions_t590971513::get_offset_of_Plugins_8(),
	RoomOptions_t590971513::get_offset_of_suppressRoomEventsField_9(),
	RoomOptions_t590971513::get_offset_of_publishUserIdField_10(),
	RoomOptions_t590971513::get_offset_of_deleteNullPropertiesField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (RaiseEventOptions_t565739090), -1, sizeof(RaiseEventOptions_t565739090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2166[8] = 
{
	RaiseEventOptions_t565739090_StaticFields::get_offset_of_Default_0(),
	RaiseEventOptions_t565739090::get_offset_of_CachingOption_1(),
	RaiseEventOptions_t565739090::get_offset_of_InterestGroup_2(),
	RaiseEventOptions_t565739090::get_offset_of_TargetActors_3(),
	RaiseEventOptions_t565739090::get_offset_of_Receivers_4(),
	RaiseEventOptions_t565739090::get_offset_of_SequenceChannel_5(),
	RaiseEventOptions_t565739090::get_offset_of_ForwardToWebhook_6(),
	RaiseEventOptions_t565739090::get_offset_of_Encrypt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (LobbyType_t3334525524)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2167[4] = 
{
	LobbyType_t3334525524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (TypedLobby_t3014596312), -1, sizeof(TypedLobby_t3014596312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2168[3] = 
{
	TypedLobby_t3014596312::get_offset_of_Name_0(),
	TypedLobby_t3014596312::get_offset_of_Type_1(),
	TypedLobby_t3014596312_StaticFields::get_offset_of_Default_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (TypedLobbyInfo_t4102455292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[2] = 
{
	TypedLobbyInfo_t4102455292::get_offset_of_PlayerCount_3(),
	TypedLobbyInfo_t4102455292::get_offset_of_RoomCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (AuthModeOption_t918236656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2170[4] = 
{
	AuthModeOption_t918236656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (CustomAuthenticationType_t1962747993)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2171[8] = 
{
	CustomAuthenticationType_t1962747993::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (AuthenticationValues_t1430961670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[5] = 
{
	AuthenticationValues_t1430961670::get_offset_of_authType_0(),
	AuthenticationValues_t1430961670::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t1430961670::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t1430961670::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t1430961670::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ClientState_t3876498872)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[22] = 
{
	ClientState_t3876498872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (JoinType_t4282379012)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[5] = 
{
	JoinType_t4282379012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (DisconnectCause_t1534558295)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2175[13] = 
{
	DisconnectCause_t1534558295::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (ServerConnection_t1155372161)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	ServerConnection_t1155372161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (NetworkingPeer_t3034011222), -1, sizeof(NetworkingPeer_t3034011222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2177[65] = 
{
	NetworkingPeer_t3034011222::get_offset_of_AppId_40(),
	NetworkingPeer_t3034011222::get_offset_of_U3CAuthValuesU3Ek__BackingField_41(),
	NetworkingPeer_t3034011222::get_offset_of_tokenCache_42(),
	NetworkingPeer_t3034011222::get_offset_of_AuthMode_43(),
	NetworkingPeer_t3034011222::get_offset_of_EncryptionMode_44(),
	NetworkingPeer_t3034011222::get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_45(),
	0,
	0,
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_ProtocolToNameServerPort_48(),
	NetworkingPeer_t3034011222::get_offset_of_U3CMasterServerAddressU3Ek__BackingField_49(),
	NetworkingPeer_t3034011222::get_offset_of_U3CGameServerAddressU3Ek__BackingField_50(),
	NetworkingPeer_t3034011222::get_offset_of_U3CServerU3Ek__BackingField_51(),
	NetworkingPeer_t3034011222::get_offset_of_U3CStateU3Ek__BackingField_52(),
	NetworkingPeer_t3034011222::get_offset_of_IsInitialConnect_53(),
	NetworkingPeer_t3034011222::get_offset_of_insideLobby_54(),
	NetworkingPeer_t3034011222::get_offset_of_U3ClobbyU3Ek__BackingField_55(),
	NetworkingPeer_t3034011222::get_offset_of_LobbyStatistics_56(),
	NetworkingPeer_t3034011222::get_offset_of_mGameList_57(),
	NetworkingPeer_t3034011222::get_offset_of_mGameListCopy_58(),
	NetworkingPeer_t3034011222::get_offset_of_playername_59(),
	NetworkingPeer_t3034011222::get_offset_of_mPlayernameHasToBeUpdated_60(),
	NetworkingPeer_t3034011222::get_offset_of_currentRoom_61(),
	NetworkingPeer_t3034011222::get_offset_of_U3CLocalPlayerU3Ek__BackingField_62(),
	NetworkingPeer_t3034011222::get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_63(),
	NetworkingPeer_t3034011222::get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_64(),
	NetworkingPeer_t3034011222::get_offset_of_U3CRoomsCountU3Ek__BackingField_65(),
	NetworkingPeer_t3034011222::get_offset_of_lastJoinType_66(),
	NetworkingPeer_t3034011222::get_offset_of_enterRoomParamsCache_67(),
	NetworkingPeer_t3034011222::get_offset_of_didAuthenticate_68(),
	NetworkingPeer_t3034011222::get_offset_of_friendListRequested_69(),
	NetworkingPeer_t3034011222::get_offset_of_friendListTimestamp_70(),
	NetworkingPeer_t3034011222::get_offset_of_isFetchingFriendList_71(),
	NetworkingPeer_t3034011222::get_offset_of_U3CAvailableRegionsU3Ek__BackingField_72(),
	NetworkingPeer_t3034011222::get_offset_of_U3CCloudRegionU3Ek__BackingField_73(),
	NetworkingPeer_t3034011222::get_offset_of_mActors_74(),
	NetworkingPeer_t3034011222::get_offset_of_mOtherPlayerListCopy_75(),
	NetworkingPeer_t3034011222::get_offset_of_mPlayerListCopy_76(),
	NetworkingPeer_t3034011222::get_offset_of_hasSwitchedMC_77(),
	NetworkingPeer_t3034011222::get_offset_of_allowedReceivingGroups_78(),
	NetworkingPeer_t3034011222::get_offset_of_blockSendingGroups_79(),
	NetworkingPeer_t3034011222::get_offset_of_photonViewList_80(),
	NetworkingPeer_t3034011222::get_offset_of_readStream_81(),
	NetworkingPeer_t3034011222::get_offset_of_pStream_82(),
	NetworkingPeer_t3034011222::get_offset_of_dataPerGroupReliable_83(),
	NetworkingPeer_t3034011222::get_offset_of_dataPerGroupUnreliable_84(),
	NetworkingPeer_t3034011222::get_offset_of_currentLevelPrefix_85(),
	NetworkingPeer_t3034011222::get_offset_of_loadingLevelAndPausedNetwork_86(),
	0,
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_UsePrefabCache_88(),
	NetworkingPeer_t3034011222::get_offset_of_ObjectPool_89(),
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_PrefabCache_90(),
	NetworkingPeer_t3034011222::get_offset_of_monoRPCMethodsCache_91(),
	NetworkingPeer_t3034011222::get_offset_of_rpcShortcuts_92(),
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_OnPhotonInstantiateString_93(),
	NetworkingPeer_t3034011222::get_offset_of_cachedServerAddress_94(),
	NetworkingPeer_t3034011222::get_offset_of_cachedApplicationName_95(),
	NetworkingPeer_t3034011222::get_offset_of_cachedProtocolType_96(),
	NetworkingPeer_t3034011222::get_offset_of__isReconnecting_97(),
	NetworkingPeer_t3034011222::get_offset_of_tempInstantiationData_98(),
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_ObjectsInOneUpdate_99(),
	NetworkingPeer_t3034011222::get_offset_of_options_100(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (MonoBehaviour_t3914164484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[1] = 
{
	MonoBehaviour_t3914164484::get_offset_of_pvCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (PunBehaviour_t692890556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (PhotonMessageInfo_t13590565)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[3] = 
{
	PhotonMessageInfo_t13590565::get_offset_of_timeInt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PhotonMessageInfo_t13590565::get_offset_of_sender_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PhotonMessageInfo_t13590565::get_offset_of_photonView_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (PunEvent_t2040493857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (PhotonStream_t2436786422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[4] = 
{
	PhotonStream_t2436786422::get_offset_of_write_0(),
	PhotonStream_t2436786422::get_offset_of_writeData_1(),
	PhotonStream_t2436786422::get_offset_of_readData_2(),
	PhotonStream_t2436786422::get_offset_of_currentItem_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (SceneManagerHelper_t3890528889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (WebRpcResponse_t2303421426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[4] = 
{
	WebRpcResponse_t2303421426::get_offset_of_U3CNameU3Ek__BackingField_0(),
	WebRpcResponse_t2303421426::get_offset_of_U3CReturnCodeU3Ek__BackingField_1(),
	WebRpcResponse_t2303421426::get_offset_of_U3CDebugMessageU3Ek__BackingField_2(),
	WebRpcResponse_t2303421426::get_offset_of_U3CParametersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (PhotonHandler_t3957736394), -1, sizeof(PhotonHandler_t3957736394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[12] = 
{
	PhotonHandler_t3957736394_StaticFields::get_offset_of_SP_2(),
	PhotonHandler_t3957736394::get_offset_of_updateInterval_3(),
	PhotonHandler_t3957736394::get_offset_of_updateIntervalOnSerialize_4(),
	PhotonHandler_t3957736394::get_offset_of_nextSendTickCount_5(),
	PhotonHandler_t3957736394::get_offset_of_nextSendTickCountOnSerialize_6(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_sendThreadShouldRun_7(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_timerToStopConnectionInBackground_8(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_AppQuits_9(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_PingImplementation_10(),
	0,
	PhotonHandler_t3957736394_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[7] = 
{
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U3CpingManagerU3E__0_0(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24locvar0_1(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U3CbestU3E__0_2(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_connectToBest_3(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24current_4(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24disposing_5(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (PhotonLagSimulationGui_t2028718096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[4] = 
{
	PhotonLagSimulationGui_t2028718096::get_offset_of_WindowRect_2(),
	PhotonLagSimulationGui_t2028718096::get_offset_of_WindowId_3(),
	PhotonLagSimulationGui_t2028718096::get_offset_of_Visible_4(),
	PhotonLagSimulationGui_t2028718096::get_offset_of_U3CPeerU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (PhotonNetwork_t1563132424), -1, sizeof(PhotonNetwork_t1563132424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2191[34] = 
{
	0,
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_U3CgameVersionU3Ek__BackingField_1(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_photonMono_2(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_networkingPeer_3(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_MAX_VIEW_IDS_4(),
	0,
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_PhotonServerSettings_6(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_InstantiateInRoomOnly_7(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_logLevel_8(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_U3CFriendsU3Ek__BackingField_9(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_precisionForVectorSynchronization_10(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_precisionForQuaternionSynchronization_11(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_precisionForFloatSynchronization_12(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_UseRpcMonoBehaviourCache_13(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_UsePrefabCache_14(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_PrefabCache_15(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_SendMonoMessageTargets_16(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_SendMonoMessageTargetType_17(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_StartRpcsAsCoroutine_18(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_isOfflineMode_19(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_offlineModeRoom_20(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_maxConnections_21(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of__mAutomaticallySyncScene_22(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_m_autoCleanUpPlayerObjects_23(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_sendInterval_24(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_sendIntervalOnSerialize_25(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_m_isMessageQueueRunning_26(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_UsePreciseTimer_27(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_startupStopwatch_28(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_BackgroundTimeout_29(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_OnEventCall_30(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_lastUsedViewSubId_31(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_lastUsedViewSubIdStatic_32(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_manuallyAllocatedViewIds_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (EventCallback_t2860523912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (PhotonPlayer_t4120608827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[7] = 
{
	PhotonPlayer_t4120608827::get_offset_of_actorID_0(),
	PhotonPlayer_t4120608827::get_offset_of_nameField_1(),
	PhotonPlayer_t4120608827::get_offset_of_U3CUserIdU3Ek__BackingField_2(),
	PhotonPlayer_t4120608827::get_offset_of_IsLocal_3(),
	PhotonPlayer_t4120608827::get_offset_of_U3CIsInactiveU3Ek__BackingField_4(),
	PhotonPlayer_t4120608827::get_offset_of_U3CCustomPropertiesU3Ek__BackingField_5(),
	PhotonPlayer_t4120608827::get_offset_of_TagObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (PhotonStatsGui_t1789448968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[7] = 
{
	PhotonStatsGui_t1789448968::get_offset_of_statsWindowOn_2(),
	PhotonStatsGui_t1789448968::get_offset_of_statsOn_3(),
	PhotonStatsGui_t1789448968::get_offset_of_healthStatsVisible_4(),
	PhotonStatsGui_t1789448968::get_offset_of_trafficStatsOn_5(),
	PhotonStatsGui_t1789448968::get_offset_of_buttonsOn_6(),
	PhotonStatsGui_t1789448968::get_offset_of_statsRect_7(),
	PhotonStatsGui_t1789448968::get_offset_of_WindowId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (PhotonStreamQueue_t4116903749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[8] = 
{
	PhotonStreamQueue_t4116903749::get_offset_of_m_SampleRate_0(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_SampleCount_1(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_ObjectsPerSample_2(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_LastSampleTime_3(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_LastFrameCount_4(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_NextObjectIndex_5(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_Objects_6(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_IsWriting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ViewSynchronization_t3814158713)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2196[5] = 
{
	ViewSynchronization_t3814158713::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (OnSerializeTransform_t4060828641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	OnSerializeTransform_t4060828641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (OnSerializeRigidBody_t320933176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2198[4] = 
{
	OnSerializeRigidBody_t320933176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (OwnershipOption_t2481125440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[4] = 
{
	OwnershipOption_t2481125440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
