﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovie2: MonoBehaviour {

	public int Width = 320;//1920or320
	public int Height = 240;//1080or240
	public int FPS = 6;//30or6

	public WebCamTexture webcamTexture;
	public Color32[] color32;

	public GameObject Input;
	public GameObject Output;
	public GameObject After;


	void Start () 
	{
		Input.SetActive (true);//カメラ映像用のプレーンのみ表示
		After.SetActive (false);

		WebCamDevice[] devices = WebCamTexture.devices;

		webcamTexture = new WebCamTexture(devices[0].name, Width, Height, FPS);
		GetComponent<Renderer> ().material.mainTexture = webcamTexture;
		webcamTexture.Play();
	}
	public void restart()
	{
		Input.SetActive (true);//カメラ映像用のプレーンのみ表示
		After.SetActive (false);

		WebCamDevice[] devices = WebCamTexture.devices;

		webcamTexture = new WebCamTexture(devices[0].name, Width, Height, FPS);
		GetComponent<Renderer> ().material.mainTexture = webcamTexture;
		webcamTexture.Play();
	}

	void Update () 
	{
		

	}

	public void flamecut()
	{
		color32 = webcamTexture.GetPixels32 ();

		Texture2D texture = new Texture2D (webcamTexture.width, webcamTexture.height);
		Output.GetComponent<Renderer> ().material.mainTexture = texture;

		texture.SetPixels32 (color32);
		texture.Apply ();

		Input.SetActive (false);//カメラ映像を消して白を消す用のプレーン表示
		After.SetActive (true);

		TrimFlower2 TF = Output.GetComponent<TrimFlower2>();
		TF.clear();


	}


		
}
