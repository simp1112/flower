﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goInputMode : MonoBehaviour {

	public GameObject inputImageCanvas;
	public GameObject collectModeCanvas;
	public GameObject viewModeCanvas;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void inputClick(){
		collectModeCanvas.SetActive (false);
		viewModeCanvas.SetActive (false);
		inputImageCanvas.SetActive (true);
	}
}
