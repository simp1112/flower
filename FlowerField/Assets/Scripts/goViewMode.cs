﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goViewMode : MonoBehaviour {

	public GameObject collectModeCanvas;
	public GameObject viewModeCanvas;
	public GameObject MainCamera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void collectClick(){
		collectModeCanvas.SetActive (false);
		viewModeCanvas.SetActive (true);
		MainCamera.transform.rotation = Quaternion.Euler (0, 180, 0);
	}
}
