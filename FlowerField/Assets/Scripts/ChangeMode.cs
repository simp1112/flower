﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMode : MonoBehaviour {

	public GameObject Camera;
	public GameObject inputModeCanvas;
	public GameObject viewModeCanvas;
	private float lookingInputMode = 0f; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Click(){
		if (Camera.transform.rotation.y == lookingInputMode){
			Camera.transform.rotation = Quaternion.Euler(0,180,0);
			inputModeCanvas.SetActive (false);
			viewModeCanvas.SetActive (true);
		} else {
			Camera.transform.rotation = Quaternion.Euler (0, 0, 0);
			inputModeCanvas.SetActive (true);
			viewModeCanvas.SetActive (false);
		}

	}
}
