﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrimFlower2 : Photon.MonoBehaviour {

	public GameObject After;
	public Material m;
	public float red = 0.7f;
	public float blue = 0.7f;
	public float green = 0.7f;
	public Slider sldR,sldB,sldG;
	public Text RT, BT, GT;

	public PhotonView InputView;	//追加

	void Start () 
	{
		
	}

	void Update()
	{
		sldR.GetComponent<Slider>(); //スライダーの値の取得
		sldB.GetComponent<Slider>();
		sldG.GetComponent<Slider>();

		RT.text=sldR.value.ToString();
		BT.text=sldB.value.ToString();
		GT.text=sldG.value.ToString();
	}

	public void clear()	//ボタンクリック時
	{
		Texture2D mainTexture = (Texture2D) GetComponent<Renderer> ().material.mainTexture;
		Color[] pixels = mainTexture.GetPixels();

		// 書き換え用テクスチャ用配列の作成
		Color[] change_pixels = new Color[pixels.Length];
		for (int i = 0; i < pixels.Length; i++) {
			Color pixel = pixels[i];

			blue = sldB.value;
			red = sldR.value;
			green = sldG.value;

			if (pixel.b>= blue && pixel.r>= red && pixel.g>= green ) {

				change_pixels.SetValue(Color.clear, i);

			} else {

				Color change_pixel = new Color(pixel.r, pixel.g, pixel.b);
				change_pixels.SetValue(change_pixel, i);


			}
		}

		// 書き換え用テクスチャの生成
		Texture2D change_texture = new Texture2D (mainTexture.width, mainTexture.height, TextureFormat.RGBA32, false);
		change_texture.filterMode = FilterMode.Point;
		change_texture.SetPixels (change_pixels);
		change_texture.Apply();

		m.SetFloat ("_Mode", 2);//テクスチャのモードをfadeにする
		m.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
		m.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
		m.SetInt ("_ZWrite", 0);
		m.DisableKeyword ("_ALPHALEST_ON");
		m.EnableKeyword ("_ALPHABLEND_ON");
		m.DisableKeyword ("_ALPHAPREMULTIPLY_ON");


		byte[] TexData = change_texture.EncodeToPNG ();
		var args = new object[] { TexData, mainTexture.width, mainTexture.height };
		if(InputView.isMine){
			InputView.RPC ("TrimProcess", PhotonTargets.All,args);	//追加
		}

	}

	float CreateCount = 0f;				//何回クリックしたかカウント
	float FlowerPosX = 0f;				//花を生成する場所

	//以下clearから切り取った文
	[PunRPC]
	private void TrimProcess(byte[] textureByteData, int width, int height, PhotonMessageInfo info){

		var ShareTexture = new Texture2D (width, height);
		ShareTexture.LoadImage (textureByteData);


		// テクスチャを貼り替える
		After.GetComponent<Renderer> ().material.mainTexture = ShareTexture;
		Debug.Log ("Afterに貼り付け");

		CreateCount++;					//クリック回数カウント
		FlowerPosX = 5 * CreateCount;	//花を生成する場所を設定

		GameObject CreatedFlower = PhotonNetwork.Instantiate ("Flower", new Vector3 (FlowerPosX, 0f, -30f), Quaternion.Euler (90, 0, 0), 0);
		CreatedFlower.name = "Flower" + CreateCount;

		GameObject.Find ("Flower"+CreateCount).GetComponent<Renderer> ().material.mainTexture = ShareTexture;

	}



}
