﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFlower : Photon.MonoBehaviour {

	float CreateCount = 0f;				//何回クリックしたかカウント
	float FlowerPosX = 0f;				//花を生成する場所

	public GameObject CreateButton;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
			
	}

	public void OnClick(){
		
		CreateCount++;					//クリック回数カウント
		FlowerPosX = 5 * CreateCount;	//花を生成する場所を設定

		GameObject CreatedFlower = PhotonNetwork.Instantiate ("Flower", new Vector3 (FlowerPosX, 0f, -50f), Quaternion.Euler (90, 0, 0), 0);
		CreatedFlower.name = "Flower" + CreateCount;

		//画像処理後のPlaneのテクスチャを生成した花に貼り付け
		Texture2D targetTexture = (Texture2D)GameObject.Find ("After").GetComponent<Renderer> ().material.mainTexture;
		GameObject.Find ("Flower"+CreateCount).GetComponent<Renderer> ().material.mainTexture = targetTexture;

		CreateButton.SetActive (false);



	}


		
}
