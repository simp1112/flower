﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// goViewMode
struct  goViewMode_t1799806040  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject goViewMode::collectModeCanvas
	GameObject_t1756533147 * ___collectModeCanvas_2;
	// UnityEngine.GameObject goViewMode::viewModeCanvas
	GameObject_t1756533147 * ___viewModeCanvas_3;
	// UnityEngine.GameObject goViewMode::MainCamera
	GameObject_t1756533147 * ___MainCamera_4;

public:
	inline static int32_t get_offset_of_collectModeCanvas_2() { return static_cast<int32_t>(offsetof(goViewMode_t1799806040, ___collectModeCanvas_2)); }
	inline GameObject_t1756533147 * get_collectModeCanvas_2() const { return ___collectModeCanvas_2; }
	inline GameObject_t1756533147 ** get_address_of_collectModeCanvas_2() { return &___collectModeCanvas_2; }
	inline void set_collectModeCanvas_2(GameObject_t1756533147 * value)
	{
		___collectModeCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___collectModeCanvas_2, value);
	}

	inline static int32_t get_offset_of_viewModeCanvas_3() { return static_cast<int32_t>(offsetof(goViewMode_t1799806040, ___viewModeCanvas_3)); }
	inline GameObject_t1756533147 * get_viewModeCanvas_3() const { return ___viewModeCanvas_3; }
	inline GameObject_t1756533147 ** get_address_of_viewModeCanvas_3() { return &___viewModeCanvas_3; }
	inline void set_viewModeCanvas_3(GameObject_t1756533147 * value)
	{
		___viewModeCanvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___viewModeCanvas_3, value);
	}

	inline static int32_t get_offset_of_MainCamera_4() { return static_cast<int32_t>(offsetof(goViewMode_t1799806040, ___MainCamera_4)); }
	inline GameObject_t1756533147 * get_MainCamera_4() const { return ___MainCamera_4; }
	inline GameObject_t1756533147 ** get_address_of_MainCamera_4() { return &___MainCamera_4; }
	inline void set_MainCamera_4(GameObject_t1756533147 * value)
	{
		___MainCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___MainCamera_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
