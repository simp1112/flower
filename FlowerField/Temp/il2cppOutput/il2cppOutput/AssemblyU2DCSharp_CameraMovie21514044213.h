﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.WebCamTexture
struct WebCamTexture_t1079476942;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraMovie2
struct  CameraMovie2_t1514044213  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 CameraMovie2::Width
	int32_t ___Width_2;
	// System.Int32 CameraMovie2::Height
	int32_t ___Height_3;
	// System.Int32 CameraMovie2::FPS
	int32_t ___FPS_4;
	// UnityEngine.WebCamTexture CameraMovie2::webcamTexture
	WebCamTexture_t1079476942 * ___webcamTexture_5;
	// UnityEngine.Color32[] CameraMovie2::color32
	Color32U5BU5D_t30278651* ___color32_6;
	// UnityEngine.GameObject CameraMovie2::Input
	GameObject_t1756533147 * ___Input_7;
	// UnityEngine.GameObject CameraMovie2::Output
	GameObject_t1756533147 * ___Output_8;
	// UnityEngine.GameObject CameraMovie2::After
	GameObject_t1756533147 * ___After_9;

public:
	inline static int32_t get_offset_of_Width_2() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___Width_2)); }
	inline int32_t get_Width_2() const { return ___Width_2; }
	inline int32_t* get_address_of_Width_2() { return &___Width_2; }
	inline void set_Width_2(int32_t value)
	{
		___Width_2 = value;
	}

	inline static int32_t get_offset_of_Height_3() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___Height_3)); }
	inline int32_t get_Height_3() const { return ___Height_3; }
	inline int32_t* get_address_of_Height_3() { return &___Height_3; }
	inline void set_Height_3(int32_t value)
	{
		___Height_3 = value;
	}

	inline static int32_t get_offset_of_FPS_4() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___FPS_4)); }
	inline int32_t get_FPS_4() const { return ___FPS_4; }
	inline int32_t* get_address_of_FPS_4() { return &___FPS_4; }
	inline void set_FPS_4(int32_t value)
	{
		___FPS_4 = value;
	}

	inline static int32_t get_offset_of_webcamTexture_5() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___webcamTexture_5)); }
	inline WebCamTexture_t1079476942 * get_webcamTexture_5() const { return ___webcamTexture_5; }
	inline WebCamTexture_t1079476942 ** get_address_of_webcamTexture_5() { return &___webcamTexture_5; }
	inline void set_webcamTexture_5(WebCamTexture_t1079476942 * value)
	{
		___webcamTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___webcamTexture_5, value);
	}

	inline static int32_t get_offset_of_color32_6() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___color32_6)); }
	inline Color32U5BU5D_t30278651* get_color32_6() const { return ___color32_6; }
	inline Color32U5BU5D_t30278651** get_address_of_color32_6() { return &___color32_6; }
	inline void set_color32_6(Color32U5BU5D_t30278651* value)
	{
		___color32_6 = value;
		Il2CppCodeGenWriteBarrier(&___color32_6, value);
	}

	inline static int32_t get_offset_of_Input_7() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___Input_7)); }
	inline GameObject_t1756533147 * get_Input_7() const { return ___Input_7; }
	inline GameObject_t1756533147 ** get_address_of_Input_7() { return &___Input_7; }
	inline void set_Input_7(GameObject_t1756533147 * value)
	{
		___Input_7 = value;
		Il2CppCodeGenWriteBarrier(&___Input_7, value);
	}

	inline static int32_t get_offset_of_Output_8() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___Output_8)); }
	inline GameObject_t1756533147 * get_Output_8() const { return ___Output_8; }
	inline GameObject_t1756533147 ** get_address_of_Output_8() { return &___Output_8; }
	inline void set_Output_8(GameObject_t1756533147 * value)
	{
		___Output_8 = value;
		Il2CppCodeGenWriteBarrier(&___Output_8, value);
	}

	inline static int32_t get_offset_of_After_9() { return static_cast<int32_t>(offsetof(CameraMovie2_t1514044213, ___After_9)); }
	inline GameObject_t1756533147 * get_After_9() const { return ___After_9; }
	inline GameObject_t1756533147 ** get_address_of_After_9() { return &___After_9; }
	inline void set_After_9(GameObject_t1756533147 * value)
	{
		___After_9 = value;
		Il2CppCodeGenWriteBarrier(&___After_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
