﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhotonLogLevel1525257006.h"

// System.String
struct String_t;
// PhotonHandler
struct PhotonHandler_t3957736394;
// NetworkingPeer
struct NetworkingPeer_t3034011222;
// ServerSettings
struct ServerSettings_t1038886298;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t3252793716;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t89994001;
// System.Type
struct Type_t;
// Room
struct Room_t1042398373;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// PhotonNetwork/EventCallback
struct EventCallback_t2860523912;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetwork
struct  PhotonNetwork_t1563132424  : public Il2CppObject
{
public:

public:
};

struct PhotonNetwork_t1563132424_StaticFields
{
public:
	// System.String PhotonNetwork::<gameVersion>k__BackingField
	String_t* ___U3CgameVersionU3Ek__BackingField_1;
	// PhotonHandler PhotonNetwork::photonMono
	PhotonHandler_t3957736394 * ___photonMono_2;
	// NetworkingPeer PhotonNetwork::networkingPeer
	NetworkingPeer_t3034011222 * ___networkingPeer_3;
	// System.Int32 PhotonNetwork::MAX_VIEW_IDS
	int32_t ___MAX_VIEW_IDS_4;
	// ServerSettings PhotonNetwork::PhotonServerSettings
	ServerSettings_t1038886298 * ___PhotonServerSettings_6;
	// System.Boolean PhotonNetwork::InstantiateInRoomOnly
	bool ___InstantiateInRoomOnly_7;
	// PhotonLogLevel PhotonNetwork::logLevel
	int32_t ___logLevel_8;
	// System.Collections.Generic.List`1<FriendInfo> PhotonNetwork::<Friends>k__BackingField
	List_1_t3252793716 * ___U3CFriendsU3Ek__BackingField_9;
	// System.Single PhotonNetwork::precisionForVectorSynchronization
	float ___precisionForVectorSynchronization_10;
	// System.Single PhotonNetwork::precisionForQuaternionSynchronization
	float ___precisionForQuaternionSynchronization_11;
	// System.Single PhotonNetwork::precisionForFloatSynchronization
	float ___precisionForFloatSynchronization_12;
	// System.Boolean PhotonNetwork::UseRpcMonoBehaviourCache
	bool ___UseRpcMonoBehaviourCache_13;
	// System.Boolean PhotonNetwork::UsePrefabCache
	bool ___UsePrefabCache_14;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> PhotonNetwork::PrefabCache
	Dictionary_2_t3671312409 * ___PrefabCache_15;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> PhotonNetwork::SendMonoMessageTargets
	HashSet_1_t89994001 * ___SendMonoMessageTargets_16;
	// System.Type PhotonNetwork::SendMonoMessageTargetType
	Type_t * ___SendMonoMessageTargetType_17;
	// System.Boolean PhotonNetwork::StartRpcsAsCoroutine
	bool ___StartRpcsAsCoroutine_18;
	// System.Boolean PhotonNetwork::isOfflineMode
	bool ___isOfflineMode_19;
	// Room PhotonNetwork::offlineModeRoom
	Room_t1042398373 * ___offlineModeRoom_20;
	// System.Int32 PhotonNetwork::maxConnections
	int32_t ___maxConnections_21;
	// System.Boolean PhotonNetwork::_mAutomaticallySyncScene
	bool ____mAutomaticallySyncScene_22;
	// System.Boolean PhotonNetwork::m_autoCleanUpPlayerObjects
	bool ___m_autoCleanUpPlayerObjects_23;
	// System.Int32 PhotonNetwork::sendInterval
	int32_t ___sendInterval_24;
	// System.Int32 PhotonNetwork::sendIntervalOnSerialize
	int32_t ___sendIntervalOnSerialize_25;
	// System.Boolean PhotonNetwork::m_isMessageQueueRunning
	bool ___m_isMessageQueueRunning_26;
	// System.Boolean PhotonNetwork::UsePreciseTimer
	bool ___UsePreciseTimer_27;
	// System.Diagnostics.Stopwatch PhotonNetwork::startupStopwatch
	Stopwatch_t1380178105 * ___startupStopwatch_28;
	// System.Single PhotonNetwork::BackgroundTimeout
	float ___BackgroundTimeout_29;
	// PhotonNetwork/EventCallback PhotonNetwork::OnEventCall
	EventCallback_t2860523912 * ___OnEventCall_30;
	// System.Int32 PhotonNetwork::lastUsedViewSubId
	int32_t ___lastUsedViewSubId_31;
	// System.Int32 PhotonNetwork::lastUsedViewSubIdStatic
	int32_t ___lastUsedViewSubIdStatic_32;
	// System.Collections.Generic.List`1<System.Int32> PhotonNetwork::manuallyAllocatedViewIds
	List_1_t1440998580 * ___manuallyAllocatedViewIds_33;

public:
	inline static int32_t get_offset_of_U3CgameVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___U3CgameVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CgameVersionU3Ek__BackingField_1() const { return ___U3CgameVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgameVersionU3Ek__BackingField_1() { return &___U3CgameVersionU3Ek__BackingField_1; }
	inline void set_U3CgameVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CgameVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgameVersionU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_photonMono_2() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___photonMono_2)); }
	inline PhotonHandler_t3957736394 * get_photonMono_2() const { return ___photonMono_2; }
	inline PhotonHandler_t3957736394 ** get_address_of_photonMono_2() { return &___photonMono_2; }
	inline void set_photonMono_2(PhotonHandler_t3957736394 * value)
	{
		___photonMono_2 = value;
		Il2CppCodeGenWriteBarrier(&___photonMono_2, value);
	}

	inline static int32_t get_offset_of_networkingPeer_3() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___networkingPeer_3)); }
	inline NetworkingPeer_t3034011222 * get_networkingPeer_3() const { return ___networkingPeer_3; }
	inline NetworkingPeer_t3034011222 ** get_address_of_networkingPeer_3() { return &___networkingPeer_3; }
	inline void set_networkingPeer_3(NetworkingPeer_t3034011222 * value)
	{
		___networkingPeer_3 = value;
		Il2CppCodeGenWriteBarrier(&___networkingPeer_3, value);
	}

	inline static int32_t get_offset_of_MAX_VIEW_IDS_4() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___MAX_VIEW_IDS_4)); }
	inline int32_t get_MAX_VIEW_IDS_4() const { return ___MAX_VIEW_IDS_4; }
	inline int32_t* get_address_of_MAX_VIEW_IDS_4() { return &___MAX_VIEW_IDS_4; }
	inline void set_MAX_VIEW_IDS_4(int32_t value)
	{
		___MAX_VIEW_IDS_4 = value;
	}

	inline static int32_t get_offset_of_PhotonServerSettings_6() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___PhotonServerSettings_6)); }
	inline ServerSettings_t1038886298 * get_PhotonServerSettings_6() const { return ___PhotonServerSettings_6; }
	inline ServerSettings_t1038886298 ** get_address_of_PhotonServerSettings_6() { return &___PhotonServerSettings_6; }
	inline void set_PhotonServerSettings_6(ServerSettings_t1038886298 * value)
	{
		___PhotonServerSettings_6 = value;
		Il2CppCodeGenWriteBarrier(&___PhotonServerSettings_6, value);
	}

	inline static int32_t get_offset_of_InstantiateInRoomOnly_7() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___InstantiateInRoomOnly_7)); }
	inline bool get_InstantiateInRoomOnly_7() const { return ___InstantiateInRoomOnly_7; }
	inline bool* get_address_of_InstantiateInRoomOnly_7() { return &___InstantiateInRoomOnly_7; }
	inline void set_InstantiateInRoomOnly_7(bool value)
	{
		___InstantiateInRoomOnly_7 = value;
	}

	inline static int32_t get_offset_of_logLevel_8() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___logLevel_8)); }
	inline int32_t get_logLevel_8() const { return ___logLevel_8; }
	inline int32_t* get_address_of_logLevel_8() { return &___logLevel_8; }
	inline void set_logLevel_8(int32_t value)
	{
		___logLevel_8 = value;
	}

	inline static int32_t get_offset_of_U3CFriendsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___U3CFriendsU3Ek__BackingField_9)); }
	inline List_1_t3252793716 * get_U3CFriendsU3Ek__BackingField_9() const { return ___U3CFriendsU3Ek__BackingField_9; }
	inline List_1_t3252793716 ** get_address_of_U3CFriendsU3Ek__BackingField_9() { return &___U3CFriendsU3Ek__BackingField_9; }
	inline void set_U3CFriendsU3Ek__BackingField_9(List_1_t3252793716 * value)
	{
		___U3CFriendsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFriendsU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_precisionForVectorSynchronization_10() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___precisionForVectorSynchronization_10)); }
	inline float get_precisionForVectorSynchronization_10() const { return ___precisionForVectorSynchronization_10; }
	inline float* get_address_of_precisionForVectorSynchronization_10() { return &___precisionForVectorSynchronization_10; }
	inline void set_precisionForVectorSynchronization_10(float value)
	{
		___precisionForVectorSynchronization_10 = value;
	}

	inline static int32_t get_offset_of_precisionForQuaternionSynchronization_11() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___precisionForQuaternionSynchronization_11)); }
	inline float get_precisionForQuaternionSynchronization_11() const { return ___precisionForQuaternionSynchronization_11; }
	inline float* get_address_of_precisionForQuaternionSynchronization_11() { return &___precisionForQuaternionSynchronization_11; }
	inline void set_precisionForQuaternionSynchronization_11(float value)
	{
		___precisionForQuaternionSynchronization_11 = value;
	}

	inline static int32_t get_offset_of_precisionForFloatSynchronization_12() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___precisionForFloatSynchronization_12)); }
	inline float get_precisionForFloatSynchronization_12() const { return ___precisionForFloatSynchronization_12; }
	inline float* get_address_of_precisionForFloatSynchronization_12() { return &___precisionForFloatSynchronization_12; }
	inline void set_precisionForFloatSynchronization_12(float value)
	{
		___precisionForFloatSynchronization_12 = value;
	}

	inline static int32_t get_offset_of_UseRpcMonoBehaviourCache_13() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___UseRpcMonoBehaviourCache_13)); }
	inline bool get_UseRpcMonoBehaviourCache_13() const { return ___UseRpcMonoBehaviourCache_13; }
	inline bool* get_address_of_UseRpcMonoBehaviourCache_13() { return &___UseRpcMonoBehaviourCache_13; }
	inline void set_UseRpcMonoBehaviourCache_13(bool value)
	{
		___UseRpcMonoBehaviourCache_13 = value;
	}

	inline static int32_t get_offset_of_UsePrefabCache_14() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___UsePrefabCache_14)); }
	inline bool get_UsePrefabCache_14() const { return ___UsePrefabCache_14; }
	inline bool* get_address_of_UsePrefabCache_14() { return &___UsePrefabCache_14; }
	inline void set_UsePrefabCache_14(bool value)
	{
		___UsePrefabCache_14 = value;
	}

	inline static int32_t get_offset_of_PrefabCache_15() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___PrefabCache_15)); }
	inline Dictionary_2_t3671312409 * get_PrefabCache_15() const { return ___PrefabCache_15; }
	inline Dictionary_2_t3671312409 ** get_address_of_PrefabCache_15() { return &___PrefabCache_15; }
	inline void set_PrefabCache_15(Dictionary_2_t3671312409 * value)
	{
		___PrefabCache_15 = value;
		Il2CppCodeGenWriteBarrier(&___PrefabCache_15, value);
	}

	inline static int32_t get_offset_of_SendMonoMessageTargets_16() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___SendMonoMessageTargets_16)); }
	inline HashSet_1_t89994001 * get_SendMonoMessageTargets_16() const { return ___SendMonoMessageTargets_16; }
	inline HashSet_1_t89994001 ** get_address_of_SendMonoMessageTargets_16() { return &___SendMonoMessageTargets_16; }
	inline void set_SendMonoMessageTargets_16(HashSet_1_t89994001 * value)
	{
		___SendMonoMessageTargets_16 = value;
		Il2CppCodeGenWriteBarrier(&___SendMonoMessageTargets_16, value);
	}

	inline static int32_t get_offset_of_SendMonoMessageTargetType_17() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___SendMonoMessageTargetType_17)); }
	inline Type_t * get_SendMonoMessageTargetType_17() const { return ___SendMonoMessageTargetType_17; }
	inline Type_t ** get_address_of_SendMonoMessageTargetType_17() { return &___SendMonoMessageTargetType_17; }
	inline void set_SendMonoMessageTargetType_17(Type_t * value)
	{
		___SendMonoMessageTargetType_17 = value;
		Il2CppCodeGenWriteBarrier(&___SendMonoMessageTargetType_17, value);
	}

	inline static int32_t get_offset_of_StartRpcsAsCoroutine_18() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___StartRpcsAsCoroutine_18)); }
	inline bool get_StartRpcsAsCoroutine_18() const { return ___StartRpcsAsCoroutine_18; }
	inline bool* get_address_of_StartRpcsAsCoroutine_18() { return &___StartRpcsAsCoroutine_18; }
	inline void set_StartRpcsAsCoroutine_18(bool value)
	{
		___StartRpcsAsCoroutine_18 = value;
	}

	inline static int32_t get_offset_of_isOfflineMode_19() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___isOfflineMode_19)); }
	inline bool get_isOfflineMode_19() const { return ___isOfflineMode_19; }
	inline bool* get_address_of_isOfflineMode_19() { return &___isOfflineMode_19; }
	inline void set_isOfflineMode_19(bool value)
	{
		___isOfflineMode_19 = value;
	}

	inline static int32_t get_offset_of_offlineModeRoom_20() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___offlineModeRoom_20)); }
	inline Room_t1042398373 * get_offlineModeRoom_20() const { return ___offlineModeRoom_20; }
	inline Room_t1042398373 ** get_address_of_offlineModeRoom_20() { return &___offlineModeRoom_20; }
	inline void set_offlineModeRoom_20(Room_t1042398373 * value)
	{
		___offlineModeRoom_20 = value;
		Il2CppCodeGenWriteBarrier(&___offlineModeRoom_20, value);
	}

	inline static int32_t get_offset_of_maxConnections_21() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___maxConnections_21)); }
	inline int32_t get_maxConnections_21() const { return ___maxConnections_21; }
	inline int32_t* get_address_of_maxConnections_21() { return &___maxConnections_21; }
	inline void set_maxConnections_21(int32_t value)
	{
		___maxConnections_21 = value;
	}

	inline static int32_t get_offset_of__mAutomaticallySyncScene_22() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ____mAutomaticallySyncScene_22)); }
	inline bool get__mAutomaticallySyncScene_22() const { return ____mAutomaticallySyncScene_22; }
	inline bool* get_address_of__mAutomaticallySyncScene_22() { return &____mAutomaticallySyncScene_22; }
	inline void set__mAutomaticallySyncScene_22(bool value)
	{
		____mAutomaticallySyncScene_22 = value;
	}

	inline static int32_t get_offset_of_m_autoCleanUpPlayerObjects_23() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___m_autoCleanUpPlayerObjects_23)); }
	inline bool get_m_autoCleanUpPlayerObjects_23() const { return ___m_autoCleanUpPlayerObjects_23; }
	inline bool* get_address_of_m_autoCleanUpPlayerObjects_23() { return &___m_autoCleanUpPlayerObjects_23; }
	inline void set_m_autoCleanUpPlayerObjects_23(bool value)
	{
		___m_autoCleanUpPlayerObjects_23 = value;
	}

	inline static int32_t get_offset_of_sendInterval_24() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___sendInterval_24)); }
	inline int32_t get_sendInterval_24() const { return ___sendInterval_24; }
	inline int32_t* get_address_of_sendInterval_24() { return &___sendInterval_24; }
	inline void set_sendInterval_24(int32_t value)
	{
		___sendInterval_24 = value;
	}

	inline static int32_t get_offset_of_sendIntervalOnSerialize_25() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___sendIntervalOnSerialize_25)); }
	inline int32_t get_sendIntervalOnSerialize_25() const { return ___sendIntervalOnSerialize_25; }
	inline int32_t* get_address_of_sendIntervalOnSerialize_25() { return &___sendIntervalOnSerialize_25; }
	inline void set_sendIntervalOnSerialize_25(int32_t value)
	{
		___sendIntervalOnSerialize_25 = value;
	}

	inline static int32_t get_offset_of_m_isMessageQueueRunning_26() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___m_isMessageQueueRunning_26)); }
	inline bool get_m_isMessageQueueRunning_26() const { return ___m_isMessageQueueRunning_26; }
	inline bool* get_address_of_m_isMessageQueueRunning_26() { return &___m_isMessageQueueRunning_26; }
	inline void set_m_isMessageQueueRunning_26(bool value)
	{
		___m_isMessageQueueRunning_26 = value;
	}

	inline static int32_t get_offset_of_UsePreciseTimer_27() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___UsePreciseTimer_27)); }
	inline bool get_UsePreciseTimer_27() const { return ___UsePreciseTimer_27; }
	inline bool* get_address_of_UsePreciseTimer_27() { return &___UsePreciseTimer_27; }
	inline void set_UsePreciseTimer_27(bool value)
	{
		___UsePreciseTimer_27 = value;
	}

	inline static int32_t get_offset_of_startupStopwatch_28() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___startupStopwatch_28)); }
	inline Stopwatch_t1380178105 * get_startupStopwatch_28() const { return ___startupStopwatch_28; }
	inline Stopwatch_t1380178105 ** get_address_of_startupStopwatch_28() { return &___startupStopwatch_28; }
	inline void set_startupStopwatch_28(Stopwatch_t1380178105 * value)
	{
		___startupStopwatch_28 = value;
		Il2CppCodeGenWriteBarrier(&___startupStopwatch_28, value);
	}

	inline static int32_t get_offset_of_BackgroundTimeout_29() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___BackgroundTimeout_29)); }
	inline float get_BackgroundTimeout_29() const { return ___BackgroundTimeout_29; }
	inline float* get_address_of_BackgroundTimeout_29() { return &___BackgroundTimeout_29; }
	inline void set_BackgroundTimeout_29(float value)
	{
		___BackgroundTimeout_29 = value;
	}

	inline static int32_t get_offset_of_OnEventCall_30() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___OnEventCall_30)); }
	inline EventCallback_t2860523912 * get_OnEventCall_30() const { return ___OnEventCall_30; }
	inline EventCallback_t2860523912 ** get_address_of_OnEventCall_30() { return &___OnEventCall_30; }
	inline void set_OnEventCall_30(EventCallback_t2860523912 * value)
	{
		___OnEventCall_30 = value;
		Il2CppCodeGenWriteBarrier(&___OnEventCall_30, value);
	}

	inline static int32_t get_offset_of_lastUsedViewSubId_31() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___lastUsedViewSubId_31)); }
	inline int32_t get_lastUsedViewSubId_31() const { return ___lastUsedViewSubId_31; }
	inline int32_t* get_address_of_lastUsedViewSubId_31() { return &___lastUsedViewSubId_31; }
	inline void set_lastUsedViewSubId_31(int32_t value)
	{
		___lastUsedViewSubId_31 = value;
	}

	inline static int32_t get_offset_of_lastUsedViewSubIdStatic_32() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___lastUsedViewSubIdStatic_32)); }
	inline int32_t get_lastUsedViewSubIdStatic_32() const { return ___lastUsedViewSubIdStatic_32; }
	inline int32_t* get_address_of_lastUsedViewSubIdStatic_32() { return &___lastUsedViewSubIdStatic_32; }
	inline void set_lastUsedViewSubIdStatic_32(int32_t value)
	{
		___lastUsedViewSubIdStatic_32 = value;
	}

	inline static int32_t get_offset_of_manuallyAllocatedViewIds_33() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1563132424_StaticFields, ___manuallyAllocatedViewIds_33)); }
	inline List_1_t1440998580 * get_manuallyAllocatedViewIds_33() const { return ___manuallyAllocatedViewIds_33; }
	inline List_1_t1440998580 ** get_address_of_manuallyAllocatedViewIds_33() { return &___manuallyAllocatedViewIds_33; }
	inline void set_manuallyAllocatedViewIds_33(List_1_t1440998580 * value)
	{
		___manuallyAllocatedViewIds_33 = value;
		Il2CppCodeGenWriteBarrier(&___manuallyAllocatedViewIds_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
