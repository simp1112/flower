﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhotonView899863581.h"
#include "AssemblyU2DCSharp_PhotonPingManager2532484147.h"
#include "AssemblyU2DCSharp_PhotonPingManager_U3CPingSocketU1588997924.h"
#include "AssemblyU2DCSharp_PunRPC2663265338.h"
#include "AssemblyU2DCSharp_Room1042398373.h"
#include "AssemblyU2DCSharp_RoomInfo4257638335.h"
#include "AssemblyU2DCSharp_Region1621871372.h"
#include "AssemblyU2DCSharp_ServerSettings1038886298.h"
#include "AssemblyU2DCSharp_ServerSettings_HostingOption2206970210.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView66394406.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterType3067826674.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeTy2623161407.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedP2669670816.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedL3412027534.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesLayerS1519976543.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesParame2780798560.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetLayerSy4246936449.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetParamet2250450980.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetLayerSy3368855023.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetParamet3485588458.h"
#include "AssemblyU2DCSharp_PhotonRigidbody2DView4188964322.h"
#include "AssemblyU2DCSharp_PhotonRigidbodyView2617830816.h"
#include "AssemblyU2DCSharp_PhotonTransformView3716933429.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionContr1910445639.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel1204247907.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel2067165795.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel1167321193.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationContr1985514502.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel3365719546.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel1626792532.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleControl1333660648.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel4204778372.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel_In3260964674.h"
#include "AssemblyU2DCSharp_ConnectAndJoinRandom2056670600.h"
#include "AssemblyU2DCSharp_CullArea3831440099.h"
#include "AssemblyU2DCSharp_CellTree3485148236.h"
#include "AssemblyU2DCSharp_CellTreeNode1707173264.h"
#include "AssemblyU2DCSharp_CellTreeNode_ENodeType1774954820.h"
#include "AssemblyU2DCSharp_InRoomChat534585606.h"
#include "AssemblyU2DCSharp_InputToEvent861782367.h"
#include "AssemblyU2DCSharp_ManualPhotonViewAllocator870609218.h"
#include "AssemblyU2DCSharp_MoveByKeys3680896920.h"
#include "AssemblyU2DCSharp_NetworkCullingHandler2843961906.h"
#include "AssemblyU2DCSharp_OnClickDestroy3163187831.h"
#include "AssemblyU2DCSharp_OnClickDestroy_U3CDestroyRpcU3Ec2941137122.h"
#include "AssemblyU2DCSharp_OnClickInstantiate2713345687.h"
#include "AssemblyU2DCSharp_OnJoinedInstantiate3153799124.h"
#include "AssemblyU2DCSharp_OnStartDelete757340584.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_PlayerR1284878072.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_PlayerR3550649017.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_PlayerR2646956530.h"
#include "AssemblyU2DCSharp_PickupItem3927383039.h"
#include "AssemblyU2DCSharp_PickupItemSimple3043267201.h"
#include "AssemblyU2DCSharp_PickupItemSyncer3520525439.h"
#include "AssemblyU2DCSharp_PointedAtGameObjectInfo1351968391.h"
#include "AssemblyU2DCSharp_PunPlayerScores851215673.h"
#include "AssemblyU2DCSharp_ScoreExtensions710763316.h"
#include "AssemblyU2DCSharp_PunTeams2511225963.h"
#include "AssemblyU2DCSharp_PunTeams_Team3635662189.h"
#include "AssemblyU2DCSharp_TeamExtensions863231085.h"
#include "AssemblyU2DCSharp_PunTurnManager1878040655.h"
#include "AssemblyU2DCSharp_TurnExtensions377830785.h"
#include "AssemblyU2DCSharp_QuitOnEscapeOrBack1892590029.h"
#include "AssemblyU2DCSharp_ShowStatusWhenConnecting4249672599.h"
#include "AssemblyU2DCSharp_SmoothSyncMovement4000196920.h"
#include "AssemblyU2DCSharp_SupportLogger2429337533.h"
#include "AssemblyU2DCSharp_SupportLogging3462084344.h"
#include "AssemblyU2DCSharp_ExitGames_Client_DemoParticle_Ti1687498427.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_ButtonI3964493381.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_TextBut3509515466.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_TextTog2505131529.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2271943107.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3457972569.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2300720745.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha1592542841.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3994538960.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3589262562.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3121459608.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cus2359342945.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Aut1914595372.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Param62576016.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Err1075336687.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2365316599.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2575310985.h"
#include "AssemblyU2DCSharp_CameraMovie21514044213.h"
#include "AssemblyU2DCSharp_ChangeMode921630831.h"
#include "AssemblyU2DCSharp_CreateFlower3418943335.h"
#include "AssemblyU2DCSharp_PhotonSystem2765200621.h"
#include "AssemblyU2DCSharp_TrimFlower2442156591.h"
#include "AssemblyU2DCSharp_goInputMode2968674023.h"
#include "AssemblyU2DCSharp_goViewMode1799806040.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637719.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (PhotonView_t899863581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[23] = 
{
	PhotonView_t899863581::get_offset_of_ownerId_3(),
	PhotonView_t899863581::get_offset_of_group_4(),
	PhotonView_t899863581::get_offset_of_mixedModeIsReliable_5(),
	PhotonView_t899863581::get_offset_of_OwnerShipWasTransfered_6(),
	PhotonView_t899863581::get_offset_of_prefixBackup_7(),
	PhotonView_t899863581::get_offset_of_instantiationDataField_8(),
	PhotonView_t899863581::get_offset_of_lastOnSerializeDataSent_9(),
	PhotonView_t899863581::get_offset_of_lastOnSerializeDataReceived_10(),
	PhotonView_t899863581::get_offset_of_synchronization_11(),
	PhotonView_t899863581::get_offset_of_onSerializeTransformOption_12(),
	PhotonView_t899863581::get_offset_of_onSerializeRigidBodyOption_13(),
	PhotonView_t899863581::get_offset_of_ownershipTransfer_14(),
	PhotonView_t899863581::get_offset_of_ObservedComponents_15(),
	PhotonView_t899863581::get_offset_of_m_OnSerializeMethodInfos_16(),
	PhotonView_t899863581::get_offset_of_viewIdField_17(),
	PhotonView_t899863581::get_offset_of_instantiationId_18(),
	PhotonView_t899863581::get_offset_of_currentMasterID_19(),
	PhotonView_t899863581::get_offset_of_didAwake_20(),
	PhotonView_t899863581::get_offset_of_isRuntimeInstantiated_21(),
	PhotonView_t899863581::get_offset_of_removedFromLocalViewList_22(),
	PhotonView_t899863581::get_offset_of_RpcMonoBehaviours_23(),
	PhotonView_t899863581::get_offset_of_OnSerializeMethodInfo_24(),
	PhotonView_t899863581::get_offset_of_failedToFindOnSerialize_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (PhotonPingManager_t2532484147), -1, sizeof(PhotonPingManager_t2532484147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2201[6] = 
{
	PhotonPingManager_t2532484147::get_offset_of_UseNative_0(),
	PhotonPingManager_t2532484147_StaticFields::get_offset_of_Attempts_1(),
	PhotonPingManager_t2532484147_StaticFields::get_offset_of_IgnoreInitialAttempt_2(),
	PhotonPingManager_t2532484147_StaticFields::get_offset_of_MaxMilliseconsPerPing_3(),
	0,
	PhotonPingManager_t2532484147::get_offset_of_PingsRunning_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CPingSocketU3Ec__Iterator0_t1588997924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[15] = 
{
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_region_0(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CpingU3E__1_1(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CrttSumU3E__0_2(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CreplyCountU3E__0_3(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CregionAddressU3E__0_4(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CindexOfColonU3E__0_5(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CindexOfProtocolU3E__0_6(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CiU3E__2_7(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CovertimeU3E__3_8(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CswU3E__3_9(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CrttU3E__3_10(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24this_11(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24current_12(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24disposing_13(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (PunRPC_t2663265338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (Room_t1042398373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[1] = 
{
	Room_t1042398373::get_offset_of_U3CPropertiesListedInLobbyU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (RoomInfo_t4257638335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[12] = 
{
	RoomInfo_t4257638335::get_offset_of_U3CremovedFromListU3Ek__BackingField_0(),
	RoomInfo_t4257638335::get_offset_of_customPropertiesField_1(),
	RoomInfo_t4257638335::get_offset_of_maxPlayersField_2(),
	RoomInfo_t4257638335::get_offset_of_expectedUsersField_3(),
	RoomInfo_t4257638335::get_offset_of_openField_4(),
	RoomInfo_t4257638335::get_offset_of_visibleField_5(),
	RoomInfo_t4257638335::get_offset_of_autoCleanUpField_6(),
	RoomInfo_t4257638335::get_offset_of_nameField_7(),
	RoomInfo_t4257638335::get_offset_of_masterClientIdField_8(),
	RoomInfo_t4257638335::get_offset_of_U3CserverSideMasterClientU3Ek__BackingField_9(),
	RoomInfo_t4257638335::get_offset_of_U3CPlayerCountU3Ek__BackingField_10(),
	RoomInfo_t4257638335::get_offset_of_U3CIsLocalClientInsideU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (Region_t1621871372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[4] = 
{
	Region_t1621871372::get_offset_of_Code_0(),
	Region_t1621871372::get_offset_of_Cluster_1(),
	Region_t1621871372::get_offset_of_HostAndPort_2(),
	Region_t1621871372::get_offset_of_Ping_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (ServerSettings_t1038886298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[17] = 
{
	ServerSettings_t1038886298::get_offset_of_AppID_2(),
	ServerSettings_t1038886298::get_offset_of_VoiceAppID_3(),
	ServerSettings_t1038886298::get_offset_of_ChatAppID_4(),
	ServerSettings_t1038886298::get_offset_of_HostType_5(),
	ServerSettings_t1038886298::get_offset_of_PreferredRegion_6(),
	ServerSettings_t1038886298::get_offset_of_EnabledRegions_7(),
	ServerSettings_t1038886298::get_offset_of_Protocol_8(),
	ServerSettings_t1038886298::get_offset_of_ServerAddress_9(),
	ServerSettings_t1038886298::get_offset_of_ServerPort_10(),
	ServerSettings_t1038886298::get_offset_of_VoiceServerPort_11(),
	ServerSettings_t1038886298::get_offset_of_JoinLobby_12(),
	ServerSettings_t1038886298::get_offset_of_EnableLobbyStatistics_13(),
	ServerSettings_t1038886298::get_offset_of_PunLogging_14(),
	ServerSettings_t1038886298::get_offset_of_NetworkLogging_15(),
	ServerSettings_t1038886298::get_offset_of_RunInBackground_16(),
	ServerSettings_t1038886298::get_offset_of_RpcList_17(),
	ServerSettings_t1038886298::get_offset_of_DisableAutoOpenWizard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (HostingOption_t2206970210)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2208[6] = 
{
	HostingOption_t2206970210::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (PhotonAnimatorView_t66394406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[11] = 
{
	PhotonAnimatorView_t66394406::get_offset_of_m_Animator_2(),
	PhotonAnimatorView_t66394406::get_offset_of_m_StreamQueue_3(),
	PhotonAnimatorView_t66394406::get_offset_of_ShowLayerWeightsInspector_4(),
	PhotonAnimatorView_t66394406::get_offset_of_ShowParameterInspector_5(),
	PhotonAnimatorView_t66394406::get_offset_of_m_SynchronizeParameters_6(),
	PhotonAnimatorView_t66394406::get_offset_of_m_SynchronizeLayers_7(),
	PhotonAnimatorView_t66394406::get_offset_of_m_ReceiverPosition_8(),
	PhotonAnimatorView_t66394406::get_offset_of_m_LastDeserializeTime_9(),
	PhotonAnimatorView_t66394406::get_offset_of_m_WasSynchronizeTypeChanged_10(),
	PhotonAnimatorView_t66394406::get_offset_of_m_PhotonView_11(),
	PhotonAnimatorView_t66394406::get_offset_of_m_raisedDiscreteTriggersCache_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (ParameterType_t3067826674)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	ParameterType_t3067826674::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (SynchronizeType_t2623161407)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[4] = 
{
	SynchronizeType_t2623161407::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (SynchronizedParameter_t2669670816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[3] = 
{
	SynchronizedParameter_t2669670816::get_offset_of_Type_0(),
	SynchronizedParameter_t2669670816::get_offset_of_SynchronizeType_1(),
	SynchronizedParameter_t2669670816::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (SynchronizedLayer_t3412027534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[2] = 
{
	SynchronizedLayer_t3412027534::get_offset_of_SynchronizeType_0(),
	SynchronizedLayer_t3412027534::get_offset_of_LayerIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t1519976543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[1] = 
{
	U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t1519976543::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2780798560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[1] = 
{
	U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2780798560::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4246936449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[1] = 
{
	U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4246936449::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t2250450980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t2250450980::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CSetLayerSynchronizedU3Ec__AnonStorey4_t3368855023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[1] = 
{
	U3CSetLayerSynchronizedU3Ec__AnonStorey4_t3368855023::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3485588458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[1] = 
{
	U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3485588458::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (PhotonRigidbody2DView_t4188964322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[3] = 
{
	PhotonRigidbody2DView_t4188964322::get_offset_of_m_SynchronizeVelocity_2(),
	PhotonRigidbody2DView_t4188964322::get_offset_of_m_SynchronizeAngularVelocity_3(),
	PhotonRigidbody2DView_t4188964322::get_offset_of_m_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (PhotonRigidbodyView_t2617830816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[3] = 
{
	PhotonRigidbodyView_t2617830816::get_offset_of_m_SynchronizeVelocity_2(),
	PhotonRigidbodyView_t2617830816::get_offset_of_m_SynchronizeAngularVelocity_3(),
	PhotonRigidbodyView_t2617830816::get_offset_of_m_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (PhotonTransformView_t3716933429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[9] = 
{
	PhotonTransformView_t3716933429::get_offset_of_m_PositionModel_2(),
	PhotonTransformView_t3716933429::get_offset_of_m_RotationModel_3(),
	PhotonTransformView_t3716933429::get_offset_of_m_ScaleModel_4(),
	PhotonTransformView_t3716933429::get_offset_of_m_PositionControl_5(),
	PhotonTransformView_t3716933429::get_offset_of_m_RotationControl_6(),
	PhotonTransformView_t3716933429::get_offset_of_m_ScaleControl_7(),
	PhotonTransformView_t3716933429::get_offset_of_m_PhotonView_8(),
	PhotonTransformView_t3716933429::get_offset_of_m_ReceivedNetworkUpdate_9(),
	PhotonTransformView_t3716933429::get_offset_of_m_firstTake_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (PhotonTransformViewPositionControl_t1910445639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[8] = 
{
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_Model_0(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_CurrentSpeed_1(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_LastSerializeTime_2(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_SynchronizedSpeed_3(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_SynchronizedTurnSpeed_4(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_NetworkPosition_5(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_OldNetworkPositions_6(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_UpdatedPositionAfterOnSerialize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (PhotonTransformViewPositionModel_t1204247907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[14] = 
{
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_TeleportEnabled_1(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_TeleportIfDistanceGreaterThan_2(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateOption_3(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateMoveTowardsSpeed_4(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateLerpSpeed_5(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateMoveTowardsAcceleration_6(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateMoveTowardsDeceleration_7(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateSpeedCurve_8(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateOption_9(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateSpeed_10(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateIncludingRoundTripTime_11(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateNumberOfStoredPositions_12(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_DrawErrorGizmo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (InterpolateOptions_t2067165795)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[6] = 
{
	InterpolateOptions_t2067165795::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (ExtrapolateOptions_t1167321193)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[5] = 
{
	ExtrapolateOptions_t1167321193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (PhotonTransformViewRotationControl_t1985514502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[2] = 
{
	PhotonTransformViewRotationControl_t1985514502::get_offset_of_m_Model_0(),
	PhotonTransformViewRotationControl_t1985514502::get_offset_of_m_NetworkRotation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (PhotonTransformViewRotationModel_t3365719546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[4] = 
{
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_InterpolateOption_1(),
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_InterpolateRotateTowardsSpeed_2(),
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_InterpolateLerpSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (InterpolateOptions_t1626792532)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[4] = 
{
	InterpolateOptions_t1626792532::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (PhotonTransformViewScaleControl_t1333660648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[2] = 
{
	PhotonTransformViewScaleControl_t1333660648::get_offset_of_m_Model_0(),
	PhotonTransformViewScaleControl_t1333660648::get_offset_of_m_NetworkScale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (PhotonTransformViewScaleModel_t4204778372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[4] = 
{
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_InterpolateOption_1(),
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_InterpolateMoveTowardsSpeed_2(),
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_InterpolateLerpSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (InterpolateOptions_t3260964674)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2232[4] = 
{
	InterpolateOptions_t3260964674::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (ConnectAndJoinRandom_t2056670600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[3] = 
{
	ConnectAndJoinRandom_t2056670600::get_offset_of_AutoConnect_3(),
	ConnectAndJoinRandom_t2056670600::get_offset_of_Version_4(),
	ConnectAndJoinRandom_t2056670600::get_offset_of_ConnectInUpdate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (CullArea_t3831440099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[16] = 
{
	0,
	0,
	CullArea_t3831440099::get_offset_of_FIRST_GROUP_ID_4(),
	CullArea_t3831440099::get_offset_of_SUBDIVISION_FIRST_LEVEL_ORDER_5(),
	CullArea_t3831440099::get_offset_of_SUBDIVISION_SECOND_LEVEL_ORDER_6(),
	CullArea_t3831440099::get_offset_of_SUBDIVISION_THIRD_LEVEL_ORDER_7(),
	CullArea_t3831440099::get_offset_of_Center_8(),
	CullArea_t3831440099::get_offset_of_Size_9(),
	CullArea_t3831440099::get_offset_of_Subdivisions_10(),
	CullArea_t3831440099::get_offset_of_NumberOfSubdivisions_11(),
	CullArea_t3831440099::get_offset_of_U3CCellCountU3Ek__BackingField_12(),
	CullArea_t3831440099::get_offset_of_U3CCellTreeU3Ek__BackingField_13(),
	CullArea_t3831440099::get_offset_of_U3CMapU3Ek__BackingField_14(),
	CullArea_t3831440099::get_offset_of_YIsUpAxis_15(),
	CullArea_t3831440099::get_offset_of_RecreateCellHierarchy_16(),
	CullArea_t3831440099::get_offset_of_idCounter_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (CellTree_t3485148236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[1] = 
{
	CellTree_t3485148236::get_offset_of_U3CRootNodeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (CellTreeNode_t1707173264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[9] = 
{
	CellTreeNode_t1707173264::get_offset_of_Id_0(),
	CellTreeNode_t1707173264::get_offset_of_Center_1(),
	CellTreeNode_t1707173264::get_offset_of_Size_2(),
	CellTreeNode_t1707173264::get_offset_of_TopLeft_3(),
	CellTreeNode_t1707173264::get_offset_of_BottomRight_4(),
	CellTreeNode_t1707173264::get_offset_of_NodeType_5(),
	CellTreeNode_t1707173264::get_offset_of_Parent_6(),
	CellTreeNode_t1707173264::get_offset_of_Childs_7(),
	CellTreeNode_t1707173264::get_offset_of_maxDistance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (ENodeType_t1774954820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	ENodeType_t1774954820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (InRoomChat_t534585606), -1, sizeof(InRoomChat_t534585606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2238[7] = 
{
	InRoomChat_t534585606::get_offset_of_GuiRect_3(),
	InRoomChat_t534585606::get_offset_of_IsVisible_4(),
	InRoomChat_t534585606::get_offset_of_AlignBottom_5(),
	InRoomChat_t534585606::get_offset_of_messages_6(),
	InRoomChat_t534585606::get_offset_of_inputLine_7(),
	InRoomChat_t534585606::get_offset_of_scrollPos_8(),
	InRoomChat_t534585606_StaticFields::get_offset_of_ChatRPC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (InputToEvent_t861782367), -1, sizeof(InputToEvent_t861782367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2239[8] = 
{
	InputToEvent_t861782367::get_offset_of_lastGo_2(),
	InputToEvent_t861782367_StaticFields::get_offset_of_inputHitPos_3(),
	InputToEvent_t861782367::get_offset_of_DetectPointedAtGameObject_4(),
	InputToEvent_t861782367_StaticFields::get_offset_of_U3CgoPointedAtU3Ek__BackingField_5(),
	InputToEvent_t861782367::get_offset_of_pressedPosition_6(),
	InputToEvent_t861782367::get_offset_of_currentPos_7(),
	InputToEvent_t861782367::get_offset_of_Dragging_8(),
	InputToEvent_t861782367::get_offset_of_m_Camera_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (ManualPhotonViewAllocator_t870609218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[1] = 
{
	ManualPhotonViewAllocator_t870609218::get_offset_of_Prefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (MoveByKeys_t3680896920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[7] = 
{
	MoveByKeys_t3680896920::get_offset_of_Speed_3(),
	MoveByKeys_t3680896920::get_offset_of_JumpForce_4(),
	MoveByKeys_t3680896920::get_offset_of_JumpTimeout_5(),
	MoveByKeys_t3680896920::get_offset_of_isSprite_6(),
	MoveByKeys_t3680896920::get_offset_of_jumpingTime_7(),
	MoveByKeys_t3680896920::get_offset_of_body_8(),
	MoveByKeys_t3680896920::get_offset_of_body2d_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (NetworkCullingHandler_t2843961906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[7] = 
{
	NetworkCullingHandler_t2843961906::get_offset_of_orderIndex_2(),
	NetworkCullingHandler_t2843961906::get_offset_of_cullArea_3(),
	NetworkCullingHandler_t2843961906::get_offset_of_previousActiveCells_4(),
	NetworkCullingHandler_t2843961906::get_offset_of_activeCells_5(),
	NetworkCullingHandler_t2843961906::get_offset_of_pView_6(),
	NetworkCullingHandler_t2843961906::get_offset_of_lastPosition_7(),
	NetworkCullingHandler_t2843961906::get_offset_of_currentPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (OnClickDestroy_t3163187831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[1] = 
{
	OnClickDestroy_t3163187831::get_offset_of_DestroyByRpc_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (U3CDestroyRpcU3Ec__Iterator0_t2941137122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[4] = 
{
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24this_0(),
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24current_1(),
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24disposing_2(),
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (OnClickInstantiate_t2713345687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[4] = 
{
	OnClickInstantiate_t2713345687::get_offset_of_Prefab_2(),
	OnClickInstantiate_t2713345687::get_offset_of_InstantiateType_3(),
	OnClickInstantiate_t2713345687::get_offset_of_InstantiateTypeNames_4(),
	OnClickInstantiate_t2713345687::get_offset_of_showGui_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (OnJoinedInstantiate_t3153799124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[3] = 
{
	OnJoinedInstantiate_t3153799124::get_offset_of_SpawnPosition_2(),
	OnJoinedInstantiate_t3153799124::get_offset_of_PositionOffset_3(),
	OnJoinedInstantiate_t3153799124::get_offset_of_PrefabsToInstantiate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (OnStartDelete_t757340584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (PlayerRoomIndexing_t1284878072), -1, sizeof(PlayerRoomIndexing_t1284878072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2248[8] = 
{
	PlayerRoomIndexing_t1284878072_StaticFields::get_offset_of_instance_3(),
	PlayerRoomIndexing_t1284878072::get_offset_of_OnRoomIndexingChanged_4(),
	0,
	PlayerRoomIndexing_t1284878072::get_offset_of__playerIds_6(),
	PlayerRoomIndexing_t1284878072::get_offset_of__indexes_7(),
	PlayerRoomIndexing_t1284878072::get_offset_of__indexesLUT_8(),
	PlayerRoomIndexing_t1284878072::get_offset_of__indexesPool_9(),
	PlayerRoomIndexing_t1284878072::get_offset_of__p_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (RoomIndexingChanged_t3550649017), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (PlayerRoomIndexingExtensions_t2646956530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (PickupItem_t3927383039), -1, sizeof(PickupItem_t3927383039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2251[7] = 
{
	PickupItem_t3927383039::get_offset_of_SecondsBeforeRespawn_3(),
	PickupItem_t3927383039::get_offset_of_PickupOnTrigger_4(),
	PickupItem_t3927383039::get_offset_of_PickupIsMine_5(),
	PickupItem_t3927383039::get_offset_of_OnPickedUpCall_6(),
	PickupItem_t3927383039::get_offset_of_SentPickup_7(),
	PickupItem_t3927383039::get_offset_of_TimeOfRespawn_8(),
	PickupItem_t3927383039_StaticFields::get_offset_of_DisabledPickupItems_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (PickupItemSimple_t3043267201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[3] = 
{
	PickupItemSimple_t3043267201::get_offset_of_SecondsBeforeRespawn_3(),
	PickupItemSimple_t3043267201::get_offset_of_PickupOnCollide_4(),
	PickupItemSimple_t3043267201::get_offset_of_SentPickup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (PickupItemSyncer_t3520525439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[2] = 
{
	PickupItemSyncer_t3520525439::get_offset_of_IsWaitingForPickupInit_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (PointedAtGameObjectInfo_t1351968391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (PunPlayerScores_t851215673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (ScoreExtensions_t710763316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (PunTeams_t2511225963), -1, sizeof(PunTeams_t2511225963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[2] = 
{
	PunTeams_t2511225963_StaticFields::get_offset_of_PlayersPerTeam_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (Team_t3635662189)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2258[4] = 
{
	Team_t3635662189::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TeamExtensions_t863231085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (PunTurnManager_t1878040655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[7] = 
{
	PunTurnManager_t1878040655::get_offset_of_TurnDuration_3(),
	PunTurnManager_t1878040655::get_offset_of_TurnManagerListener_4(),
	PunTurnManager_t1878040655::get_offset_of_finishedPlayers_5(),
	0,
	0,
	0,
	PunTurnManager_t1878040655::get_offset_of__isOverCallProcessed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (TurnExtensions_t377830785), -1, sizeof(TurnExtensions_t377830785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	TurnExtensions_t377830785_StaticFields::get_offset_of_TurnPropKey_0(),
	TurnExtensions_t377830785_StaticFields::get_offset_of_TurnStartPropKey_1(),
	TurnExtensions_t377830785_StaticFields::get_offset_of_FinishedTurnPropKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (QuitOnEscapeOrBack_t1892590029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (ShowStatusWhenConnecting_t4249672599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[1] = 
{
	ShowStatusWhenConnecting_t4249672599::get_offset_of_Skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (SmoothSyncMovement_t4000196920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[3] = 
{
	SmoothSyncMovement_t4000196920::get_offset_of_SmoothingDelay_3(),
	SmoothSyncMovement_t4000196920::get_offset_of_correctPlayerPos_4(),
	SmoothSyncMovement_t4000196920::get_offset_of_correctPlayerRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (SupportLogger_t2429337533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[1] = 
{
	SupportLogger_t2429337533::get_offset_of_LogTrafficStats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (SupportLogging_t3462084344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	SupportLogging_t3462084344::get_offset_of_LogTrafficStats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (TimeKeeper_t1687498427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[4] = 
{
	TimeKeeper_t1687498427::get_offset_of_lastExecutionTime_0(),
	TimeKeeper_t1687498427::get_offset_of_shouldExecute_1(),
	TimeKeeper_t1687498427::get_offset_of_U3CIntervalU3Ek__BackingField_2(),
	TimeKeeper_t1687498427::get_offset_of_U3CIsEnabledU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ButtonInsideScrollList_t3964493381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[1] = 
{
	ButtonInsideScrollList_t3964493381::get_offset_of_scrollRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (TextButtonTransition_t3509515466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[3] = 
{
	TextButtonTransition_t3509515466::get_offset_of__text_2(),
	TextButtonTransition_t3509515466::get_offset_of_NormalColor_3(),
	TextButtonTransition_t3509515466::get_offset_of_HoverColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (TextToggleIsOnTransition_t2505131529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[7] = 
{
	TextToggleIsOnTransition_t2505131529::get_offset_of_toggle_2(),
	TextToggleIsOnTransition_t2505131529::get_offset_of__text_3(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_NormalOnColor_4(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_NormalOffColor_5(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_HoverOnColor_6(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_HoverOffColor_7(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_isHover_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (ChatChannel_t2271943107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[5] = 
{
	ChatChannel_t2271943107::get_offset_of_Name_0(),
	ChatChannel_t2271943107::get_offset_of_Senders_1(),
	ChatChannel_t2271943107::get_offset_of_Messages_2(),
	ChatChannel_t2271943107::get_offset_of_MessageLimit_3(),
	ChatChannel_t2271943107::get_offset_of_U3CIsPrivateU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (ChatClient_t3457972569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[20] = 
{
	0,
	ChatClient_t3457972569::get_offset_of_U3CNameServerAddressU3Ek__BackingField_1(),
	ChatClient_t3457972569::get_offset_of_U3CFrontendAddressU3Ek__BackingField_2(),
	ChatClient_t3457972569::get_offset_of_chatRegion_3(),
	ChatClient_t3457972569::get_offset_of_U3CStateU3Ek__BackingField_4(),
	ChatClient_t3457972569::get_offset_of_U3CDisconnectedCauseU3Ek__BackingField_5(),
	ChatClient_t3457972569::get_offset_of_U3CAppVersionU3Ek__BackingField_6(),
	ChatClient_t3457972569::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	ChatClient_t3457972569::get_offset_of_U3CAuthValuesU3Ek__BackingField_8(),
	ChatClient_t3457972569::get_offset_of_MessageLimit_9(),
	ChatClient_t3457972569::get_offset_of_PublicChannels_10(),
	ChatClient_t3457972569::get_offset_of_PrivateChannels_11(),
	ChatClient_t3457972569::get_offset_of_PublicChannelsUnsubscribing_12(),
	ChatClient_t3457972569::get_offset_of_listener_13(),
	ChatClient_t3457972569::get_offset_of_chatPeer_14(),
	0,
	ChatClient_t3457972569::get_offset_of_didAuthenticate_16(),
	ChatClient_t3457972569::get_offset_of_msDeltaForServiceCalls_17(),
	ChatClient_t3457972569::get_offset_of_msTimestampOfLastServiceCall_18(),
	ChatClient_t3457972569::get_offset_of_U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ChatDisconnectCause_t2300720745)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2274[12] = 
{
	ChatDisconnectCause_t2300720745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (ChatEventCode_t1592542841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (ChatOperationCode_t3994538960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (ChatParameterCode_t3589262562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (ChatPeer_t3121459608), -1, sizeof(ChatPeer_t3121459608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	0,
	0,
	ChatPeer_t3121459608_StaticFields::get_offset_of_ProtocolToNameServerPort_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (CustomAuthenticationType_t2359342945)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[8] = 
{
	CustomAuthenticationType_t2359342945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (AuthenticationValues_t1914595372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[5] = 
{
	AuthenticationValues_t1914595372::get_offset_of_authType_0(),
	AuthenticationValues_t1914595372::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t1914595372::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t1914595372::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t1914595372::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (ParameterCode_t62576016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (ErrorCode_t1075336687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (ChatState_t2365316599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2283[13] = 
{
	ChatState_t2365316599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (ChatUserStatus_t2575310985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (CameraMovie2_t1514044213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[8] = 
{
	CameraMovie2_t1514044213::get_offset_of_Width_2(),
	CameraMovie2_t1514044213::get_offset_of_Height_3(),
	CameraMovie2_t1514044213::get_offset_of_FPS_4(),
	CameraMovie2_t1514044213::get_offset_of_webcamTexture_5(),
	CameraMovie2_t1514044213::get_offset_of_color32_6(),
	CameraMovie2_t1514044213::get_offset_of_Input_7(),
	CameraMovie2_t1514044213::get_offset_of_Output_8(),
	CameraMovie2_t1514044213::get_offset_of_After_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (ChangeMode_t921630831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[4] = 
{
	ChangeMode_t921630831::get_offset_of_Camera_2(),
	ChangeMode_t921630831::get_offset_of_inputModeCanvas_3(),
	ChangeMode_t921630831::get_offset_of_viewModeCanvas_4(),
	ChangeMode_t921630831::get_offset_of_lookingInputMode_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (CreateFlower_t3418943335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[3] = 
{
	CreateFlower_t3418943335::get_offset_of_CreateCount_3(),
	CreateFlower_t3418943335::get_offset_of_FlowerPosX_4(),
	CreateFlower_t3418943335::get_offset_of_CreateButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (PhotonSystem_t2765200621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[3] = 
{
	PhotonSystem_t2765200621::get_offset_of_readyText_3(),
	PhotonSystem_t2765200621::get_offset_of_goInputModeButton_4(),
	PhotonSystem_t2765200621::get_offset_of_goViewModeButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (TrimFlower2_t442156591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[14] = 
{
	TrimFlower2_t442156591::get_offset_of_After_3(),
	TrimFlower2_t442156591::get_offset_of_m_4(),
	TrimFlower2_t442156591::get_offset_of_red_5(),
	TrimFlower2_t442156591::get_offset_of_blue_6(),
	TrimFlower2_t442156591::get_offset_of_green_7(),
	TrimFlower2_t442156591::get_offset_of_sldR_8(),
	TrimFlower2_t442156591::get_offset_of_sldB_9(),
	TrimFlower2_t442156591::get_offset_of_sldG_10(),
	TrimFlower2_t442156591::get_offset_of_RT_11(),
	TrimFlower2_t442156591::get_offset_of_BT_12(),
	TrimFlower2_t442156591::get_offset_of_GT_13(),
	TrimFlower2_t442156591::get_offset_of_InputView_14(),
	TrimFlower2_t442156591::get_offset_of_CreateCount_15(),
	TrimFlower2_t442156591::get_offset_of_FlowerPosX_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (goInputMode_t2968674023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[3] = 
{
	goInputMode_t2968674023::get_offset_of_inputImageCanvas_2(),
	goInputMode_t2968674023::get_offset_of_collectModeCanvas_3(),
	goInputMode_t2968674023::get_offset_of_viewModeCanvas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (goViewMode_t1799806040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[3] = 
{
	goViewMode_t1799806040::get_offset_of_collectModeCanvas_2(),
	goViewMode_t1799806040::get_offset_of_viewModeCanvas_3(),
	goViewMode_t1799806040::get_offset_of_MainCamera_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2293[3] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (U24ArrayTypeU3D48_t2375206766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
