﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Photon_MonoBehaviour3914164484.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Text
struct Text_t356221433;
// PhotonView
struct PhotonView_t899863581;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrimFlower2
struct  TrimFlower2_t442156591  : public MonoBehaviour_t3914164484
{
public:
	// UnityEngine.GameObject TrimFlower2::After
	GameObject_t1756533147 * ___After_3;
	// UnityEngine.Material TrimFlower2::m
	Material_t193706927 * ___m_4;
	// System.Single TrimFlower2::red
	float ___red_5;
	// System.Single TrimFlower2::blue
	float ___blue_6;
	// System.Single TrimFlower2::green
	float ___green_7;
	// UnityEngine.UI.Slider TrimFlower2::sldR
	Slider_t297367283 * ___sldR_8;
	// UnityEngine.UI.Slider TrimFlower2::sldB
	Slider_t297367283 * ___sldB_9;
	// UnityEngine.UI.Slider TrimFlower2::sldG
	Slider_t297367283 * ___sldG_10;
	// UnityEngine.UI.Text TrimFlower2::RT
	Text_t356221433 * ___RT_11;
	// UnityEngine.UI.Text TrimFlower2::BT
	Text_t356221433 * ___BT_12;
	// UnityEngine.UI.Text TrimFlower2::GT
	Text_t356221433 * ___GT_13;
	// PhotonView TrimFlower2::InputView
	PhotonView_t899863581 * ___InputView_14;
	// System.Single TrimFlower2::CreateCount
	float ___CreateCount_15;
	// System.Single TrimFlower2::FlowerPosX
	float ___FlowerPosX_16;

public:
	inline static int32_t get_offset_of_After_3() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___After_3)); }
	inline GameObject_t1756533147 * get_After_3() const { return ___After_3; }
	inline GameObject_t1756533147 ** get_address_of_After_3() { return &___After_3; }
	inline void set_After_3(GameObject_t1756533147 * value)
	{
		___After_3 = value;
		Il2CppCodeGenWriteBarrier(&___After_3, value);
	}

	inline static int32_t get_offset_of_m_4() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___m_4)); }
	inline Material_t193706927 * get_m_4() const { return ___m_4; }
	inline Material_t193706927 ** get_address_of_m_4() { return &___m_4; }
	inline void set_m_4(Material_t193706927 * value)
	{
		___m_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_4, value);
	}

	inline static int32_t get_offset_of_red_5() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___red_5)); }
	inline float get_red_5() const { return ___red_5; }
	inline float* get_address_of_red_5() { return &___red_5; }
	inline void set_red_5(float value)
	{
		___red_5 = value;
	}

	inline static int32_t get_offset_of_blue_6() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___blue_6)); }
	inline float get_blue_6() const { return ___blue_6; }
	inline float* get_address_of_blue_6() { return &___blue_6; }
	inline void set_blue_6(float value)
	{
		___blue_6 = value;
	}

	inline static int32_t get_offset_of_green_7() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___green_7)); }
	inline float get_green_7() const { return ___green_7; }
	inline float* get_address_of_green_7() { return &___green_7; }
	inline void set_green_7(float value)
	{
		___green_7 = value;
	}

	inline static int32_t get_offset_of_sldR_8() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___sldR_8)); }
	inline Slider_t297367283 * get_sldR_8() const { return ___sldR_8; }
	inline Slider_t297367283 ** get_address_of_sldR_8() { return &___sldR_8; }
	inline void set_sldR_8(Slider_t297367283 * value)
	{
		___sldR_8 = value;
		Il2CppCodeGenWriteBarrier(&___sldR_8, value);
	}

	inline static int32_t get_offset_of_sldB_9() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___sldB_9)); }
	inline Slider_t297367283 * get_sldB_9() const { return ___sldB_9; }
	inline Slider_t297367283 ** get_address_of_sldB_9() { return &___sldB_9; }
	inline void set_sldB_9(Slider_t297367283 * value)
	{
		___sldB_9 = value;
		Il2CppCodeGenWriteBarrier(&___sldB_9, value);
	}

	inline static int32_t get_offset_of_sldG_10() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___sldG_10)); }
	inline Slider_t297367283 * get_sldG_10() const { return ___sldG_10; }
	inline Slider_t297367283 ** get_address_of_sldG_10() { return &___sldG_10; }
	inline void set_sldG_10(Slider_t297367283 * value)
	{
		___sldG_10 = value;
		Il2CppCodeGenWriteBarrier(&___sldG_10, value);
	}

	inline static int32_t get_offset_of_RT_11() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___RT_11)); }
	inline Text_t356221433 * get_RT_11() const { return ___RT_11; }
	inline Text_t356221433 ** get_address_of_RT_11() { return &___RT_11; }
	inline void set_RT_11(Text_t356221433 * value)
	{
		___RT_11 = value;
		Il2CppCodeGenWriteBarrier(&___RT_11, value);
	}

	inline static int32_t get_offset_of_BT_12() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___BT_12)); }
	inline Text_t356221433 * get_BT_12() const { return ___BT_12; }
	inline Text_t356221433 ** get_address_of_BT_12() { return &___BT_12; }
	inline void set_BT_12(Text_t356221433 * value)
	{
		___BT_12 = value;
		Il2CppCodeGenWriteBarrier(&___BT_12, value);
	}

	inline static int32_t get_offset_of_GT_13() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___GT_13)); }
	inline Text_t356221433 * get_GT_13() const { return ___GT_13; }
	inline Text_t356221433 ** get_address_of_GT_13() { return &___GT_13; }
	inline void set_GT_13(Text_t356221433 * value)
	{
		___GT_13 = value;
		Il2CppCodeGenWriteBarrier(&___GT_13, value);
	}

	inline static int32_t get_offset_of_InputView_14() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___InputView_14)); }
	inline PhotonView_t899863581 * get_InputView_14() const { return ___InputView_14; }
	inline PhotonView_t899863581 ** get_address_of_InputView_14() { return &___InputView_14; }
	inline void set_InputView_14(PhotonView_t899863581 * value)
	{
		___InputView_14 = value;
		Il2CppCodeGenWriteBarrier(&___InputView_14, value);
	}

	inline static int32_t get_offset_of_CreateCount_15() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___CreateCount_15)); }
	inline float get_CreateCount_15() const { return ___CreateCount_15; }
	inline float* get_address_of_CreateCount_15() { return &___CreateCount_15; }
	inline void set_CreateCount_15(float value)
	{
		___CreateCount_15 = value;
	}

	inline static int32_t get_offset_of_FlowerPosX_16() { return static_cast<int32_t>(offsetof(TrimFlower2_t442156591, ___FlowerPosX_16)); }
	inline float get_FlowerPosX_16() const { return ___FlowerPosX_16; }
	inline float* get_address_of_FlowerPosX_16() { return &___FlowerPosX_16; }
	inline void set_FlowerPosX_16(float value)
	{
		___FlowerPosX_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
