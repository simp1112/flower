﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_LoadBalancingPeer3951471977.h"
#include "AssemblyU2DCSharp_AuthModeOption918236656.h"
#include "AssemblyU2DCSharp_EncryptionMode2259972134.h"
#include "AssemblyU2DCSharp_ServerConnection1155372161.h"
#include "AssemblyU2DCSharp_ClientState3876498872.h"
#include "AssemblyU2DCSharp_JoinType4282379012.h"
#include "AssemblyU2DCSharp_CloudRegionCode989201940.h"

// System.String
struct String_t;
// AuthenticationValues
struct AuthenticationValues_t1430961670;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t2531697585;
// TypedLobby
struct TypedLobby_t3014596312;
// System.Collections.Generic.List`1<TypedLobbyInfo>
struct List_1_t3471576424;
// System.Collections.Generic.Dictionary`2<System.String,RoomInfo>
struct Dictionary_2_t1877450301;
// RoomInfo[]
struct RoomInfoU5BU5D_t3672612454;
// Room
struct Room_t1042398373;
// PhotonPlayer
struct PhotonPlayer_t4120608827;
// EnterRoomParams
struct EnterRoomParams_t2222988781;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<Region>
struct List_1_t990992504;
// System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>
struct Dictionary_2_t3128434462;
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t818546810;
// System.Collections.Generic.HashSet`1<System.Byte>
struct HashSet_1_t2016565290;
// System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>
struct Dictionary_2_t4202656512;
// PhotonStream
struct PhotonStream_t2436786422;
// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable>
struct Dictionary_2_t3230257;
// IPunPrefabPool
struct IPunPrefabPool_t2617987714;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>
struct Dictionary_2_t342058070;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object[]>
struct Dictionary_2_t2622459769;
// RaiseEventOptions
struct RaiseEventOptions_t565739090;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkingPeer
struct  NetworkingPeer_t3034011222  : public LoadBalancingPeer_t3951471977
{
public:
	// System.String NetworkingPeer::AppId
	String_t* ___AppId_40;
	// AuthenticationValues NetworkingPeer::<AuthValues>k__BackingField
	AuthenticationValues_t1430961670 * ___U3CAuthValuesU3Ek__BackingField_41;
	// System.String NetworkingPeer::tokenCache
	String_t* ___tokenCache_42;
	// AuthModeOption NetworkingPeer::AuthMode
	int32_t ___AuthMode_43;
	// EncryptionMode NetworkingPeer::EncryptionMode
	int32_t ___EncryptionMode_44;
	// System.Boolean NetworkingPeer::<IsUsingNameServer>k__BackingField
	bool ___U3CIsUsingNameServerU3Ek__BackingField_45;
	// System.String NetworkingPeer::<MasterServerAddress>k__BackingField
	String_t* ___U3CMasterServerAddressU3Ek__BackingField_49;
	// System.String NetworkingPeer::<GameServerAddress>k__BackingField
	String_t* ___U3CGameServerAddressU3Ek__BackingField_50;
	// ServerConnection NetworkingPeer::<Server>k__BackingField
	int32_t ___U3CServerU3Ek__BackingField_51;
	// ClientState NetworkingPeer::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_52;
	// System.Boolean NetworkingPeer::IsInitialConnect
	bool ___IsInitialConnect_53;
	// System.Boolean NetworkingPeer::insideLobby
	bool ___insideLobby_54;
	// TypedLobby NetworkingPeer::<lobby>k__BackingField
	TypedLobby_t3014596312 * ___U3ClobbyU3Ek__BackingField_55;
	// System.Collections.Generic.List`1<TypedLobbyInfo> NetworkingPeer::LobbyStatistics
	List_1_t3471576424 * ___LobbyStatistics_56;
	// System.Collections.Generic.Dictionary`2<System.String,RoomInfo> NetworkingPeer::mGameList
	Dictionary_2_t1877450301 * ___mGameList_57;
	// RoomInfo[] NetworkingPeer::mGameListCopy
	RoomInfoU5BU5D_t3672612454* ___mGameListCopy_58;
	// System.String NetworkingPeer::playername
	String_t* ___playername_59;
	// System.Boolean NetworkingPeer::mPlayernameHasToBeUpdated
	bool ___mPlayernameHasToBeUpdated_60;
	// Room NetworkingPeer::currentRoom
	Room_t1042398373 * ___currentRoom_61;
	// PhotonPlayer NetworkingPeer::<LocalPlayer>k__BackingField
	PhotonPlayer_t4120608827 * ___U3CLocalPlayerU3Ek__BackingField_62;
	// System.Int32 NetworkingPeer::<PlayersOnMasterCount>k__BackingField
	int32_t ___U3CPlayersOnMasterCountU3Ek__BackingField_63;
	// System.Int32 NetworkingPeer::<PlayersInRoomsCount>k__BackingField
	int32_t ___U3CPlayersInRoomsCountU3Ek__BackingField_64;
	// System.Int32 NetworkingPeer::<RoomsCount>k__BackingField
	int32_t ___U3CRoomsCountU3Ek__BackingField_65;
	// JoinType NetworkingPeer::lastJoinType
	int32_t ___lastJoinType_66;
	// EnterRoomParams NetworkingPeer::enterRoomParamsCache
	EnterRoomParams_t2222988781 * ___enterRoomParamsCache_67;
	// System.Boolean NetworkingPeer::didAuthenticate
	bool ___didAuthenticate_68;
	// System.String[] NetworkingPeer::friendListRequested
	StringU5BU5D_t1642385972* ___friendListRequested_69;
	// System.Int32 NetworkingPeer::friendListTimestamp
	int32_t ___friendListTimestamp_70;
	// System.Boolean NetworkingPeer::isFetchingFriendList
	bool ___isFetchingFriendList_71;
	// System.Collections.Generic.List`1<Region> NetworkingPeer::<AvailableRegions>k__BackingField
	List_1_t990992504 * ___U3CAvailableRegionsU3Ek__BackingField_72;
	// CloudRegionCode NetworkingPeer::<CloudRegion>k__BackingField
	int32_t ___U3CCloudRegionU3Ek__BackingField_73;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer> NetworkingPeer::mActors
	Dictionary_2_t3128434462 * ___mActors_74;
	// PhotonPlayer[] NetworkingPeer::mOtherPlayerListCopy
	PhotonPlayerU5BU5D_t818546810* ___mOtherPlayerListCopy_75;
	// PhotonPlayer[] NetworkingPeer::mPlayerListCopy
	PhotonPlayerU5BU5D_t818546810* ___mPlayerListCopy_76;
	// System.Boolean NetworkingPeer::hasSwitchedMC
	bool ___hasSwitchedMC_77;
	// System.Collections.Generic.HashSet`1<System.Byte> NetworkingPeer::allowedReceivingGroups
	HashSet_1_t2016565290 * ___allowedReceivingGroups_78;
	// System.Collections.Generic.HashSet`1<System.Byte> NetworkingPeer::blockSendingGroups
	HashSet_1_t2016565290 * ___blockSendingGroups_79;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhotonView> NetworkingPeer::photonViewList
	Dictionary_2_t4202656512 * ___photonViewList_80;
	// PhotonStream NetworkingPeer::readStream
	PhotonStream_t2436786422 * ___readStream_81;
	// PhotonStream NetworkingPeer::pStream
	PhotonStream_t2436786422 * ___pStream_82;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable> NetworkingPeer::dataPerGroupReliable
	Dictionary_2_t3230257 * ___dataPerGroupReliable_83;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable> NetworkingPeer::dataPerGroupUnreliable
	Dictionary_2_t3230257 * ___dataPerGroupUnreliable_84;
	// System.Int16 NetworkingPeer::currentLevelPrefix
	int16_t ___currentLevelPrefix_85;
	// System.Boolean NetworkingPeer::loadingLevelAndPausedNetwork
	bool ___loadingLevelAndPausedNetwork_86;
	// IPunPrefabPool NetworkingPeer::ObjectPool
	Il2CppObject * ___ObjectPool_89;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>> NetworkingPeer::monoRPCMethodsCache
	Dictionary_2_t342058070 * ___monoRPCMethodsCache_91;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NetworkingPeer::rpcShortcuts
	Dictionary_2_t3986656710 * ___rpcShortcuts_92;
	// System.String NetworkingPeer::cachedServerAddress
	String_t* ___cachedServerAddress_94;
	// System.String NetworkingPeer::cachedApplicationName
	String_t* ___cachedApplicationName_95;
	// ServerConnection NetworkingPeer::cachedProtocolType
	int32_t ___cachedProtocolType_96;
	// System.Boolean NetworkingPeer::_isReconnecting
	bool ____isReconnecting_97;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object[]> NetworkingPeer::tempInstantiationData
	Dictionary_2_t2622459769 * ___tempInstantiationData_98;
	// RaiseEventOptions NetworkingPeer::options
	RaiseEventOptions_t565739090 * ___options_100;

public:
	inline static int32_t get_offset_of_AppId_40() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___AppId_40)); }
	inline String_t* get_AppId_40() const { return ___AppId_40; }
	inline String_t** get_address_of_AppId_40() { return &___AppId_40; }
	inline void set_AppId_40(String_t* value)
	{
		___AppId_40 = value;
		Il2CppCodeGenWriteBarrier(&___AppId_40, value);
	}

	inline static int32_t get_offset_of_U3CAuthValuesU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CAuthValuesU3Ek__BackingField_41)); }
	inline AuthenticationValues_t1430961670 * get_U3CAuthValuesU3Ek__BackingField_41() const { return ___U3CAuthValuesU3Ek__BackingField_41; }
	inline AuthenticationValues_t1430961670 ** get_address_of_U3CAuthValuesU3Ek__BackingField_41() { return &___U3CAuthValuesU3Ek__BackingField_41; }
	inline void set_U3CAuthValuesU3Ek__BackingField_41(AuthenticationValues_t1430961670 * value)
	{
		___U3CAuthValuesU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAuthValuesU3Ek__BackingField_41, value);
	}

	inline static int32_t get_offset_of_tokenCache_42() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___tokenCache_42)); }
	inline String_t* get_tokenCache_42() const { return ___tokenCache_42; }
	inline String_t** get_address_of_tokenCache_42() { return &___tokenCache_42; }
	inline void set_tokenCache_42(String_t* value)
	{
		___tokenCache_42 = value;
		Il2CppCodeGenWriteBarrier(&___tokenCache_42, value);
	}

	inline static int32_t get_offset_of_AuthMode_43() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___AuthMode_43)); }
	inline int32_t get_AuthMode_43() const { return ___AuthMode_43; }
	inline int32_t* get_address_of_AuthMode_43() { return &___AuthMode_43; }
	inline void set_AuthMode_43(int32_t value)
	{
		___AuthMode_43 = value;
	}

	inline static int32_t get_offset_of_EncryptionMode_44() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___EncryptionMode_44)); }
	inline int32_t get_EncryptionMode_44() const { return ___EncryptionMode_44; }
	inline int32_t* get_address_of_EncryptionMode_44() { return &___EncryptionMode_44; }
	inline void set_EncryptionMode_44(int32_t value)
	{
		___EncryptionMode_44 = value;
	}

	inline static int32_t get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CIsUsingNameServerU3Ek__BackingField_45)); }
	inline bool get_U3CIsUsingNameServerU3Ek__BackingField_45() const { return ___U3CIsUsingNameServerU3Ek__BackingField_45; }
	inline bool* get_address_of_U3CIsUsingNameServerU3Ek__BackingField_45() { return &___U3CIsUsingNameServerU3Ek__BackingField_45; }
	inline void set_U3CIsUsingNameServerU3Ek__BackingField_45(bool value)
	{
		___U3CIsUsingNameServerU3Ek__BackingField_45 = value;
	}

	inline static int32_t get_offset_of_U3CMasterServerAddressU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CMasterServerAddressU3Ek__BackingField_49)); }
	inline String_t* get_U3CMasterServerAddressU3Ek__BackingField_49() const { return ___U3CMasterServerAddressU3Ek__BackingField_49; }
	inline String_t** get_address_of_U3CMasterServerAddressU3Ek__BackingField_49() { return &___U3CMasterServerAddressU3Ek__BackingField_49; }
	inline void set_U3CMasterServerAddressU3Ek__BackingField_49(String_t* value)
	{
		___U3CMasterServerAddressU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMasterServerAddressU3Ek__BackingField_49, value);
	}

	inline static int32_t get_offset_of_U3CGameServerAddressU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CGameServerAddressU3Ek__BackingField_50)); }
	inline String_t* get_U3CGameServerAddressU3Ek__BackingField_50() const { return ___U3CGameServerAddressU3Ek__BackingField_50; }
	inline String_t** get_address_of_U3CGameServerAddressU3Ek__BackingField_50() { return &___U3CGameServerAddressU3Ek__BackingField_50; }
	inline void set_U3CGameServerAddressU3Ek__BackingField_50(String_t* value)
	{
		___U3CGameServerAddressU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameServerAddressU3Ek__BackingField_50, value);
	}

	inline static int32_t get_offset_of_U3CServerU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CServerU3Ek__BackingField_51)); }
	inline int32_t get_U3CServerU3Ek__BackingField_51() const { return ___U3CServerU3Ek__BackingField_51; }
	inline int32_t* get_address_of_U3CServerU3Ek__BackingField_51() { return &___U3CServerU3Ek__BackingField_51; }
	inline void set_U3CServerU3Ek__BackingField_51(int32_t value)
	{
		___U3CServerU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CStateU3Ek__BackingField_52)); }
	inline int32_t get_U3CStateU3Ek__BackingField_52() const { return ___U3CStateU3Ek__BackingField_52; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_52() { return &___U3CStateU3Ek__BackingField_52; }
	inline void set_U3CStateU3Ek__BackingField_52(int32_t value)
	{
		___U3CStateU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_IsInitialConnect_53() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___IsInitialConnect_53)); }
	inline bool get_IsInitialConnect_53() const { return ___IsInitialConnect_53; }
	inline bool* get_address_of_IsInitialConnect_53() { return &___IsInitialConnect_53; }
	inline void set_IsInitialConnect_53(bool value)
	{
		___IsInitialConnect_53 = value;
	}

	inline static int32_t get_offset_of_insideLobby_54() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___insideLobby_54)); }
	inline bool get_insideLobby_54() const { return ___insideLobby_54; }
	inline bool* get_address_of_insideLobby_54() { return &___insideLobby_54; }
	inline void set_insideLobby_54(bool value)
	{
		___insideLobby_54 = value;
	}

	inline static int32_t get_offset_of_U3ClobbyU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3ClobbyU3Ek__BackingField_55)); }
	inline TypedLobby_t3014596312 * get_U3ClobbyU3Ek__BackingField_55() const { return ___U3ClobbyU3Ek__BackingField_55; }
	inline TypedLobby_t3014596312 ** get_address_of_U3ClobbyU3Ek__BackingField_55() { return &___U3ClobbyU3Ek__BackingField_55; }
	inline void set_U3ClobbyU3Ek__BackingField_55(TypedLobby_t3014596312 * value)
	{
		___U3ClobbyU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClobbyU3Ek__BackingField_55, value);
	}

	inline static int32_t get_offset_of_LobbyStatistics_56() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___LobbyStatistics_56)); }
	inline List_1_t3471576424 * get_LobbyStatistics_56() const { return ___LobbyStatistics_56; }
	inline List_1_t3471576424 ** get_address_of_LobbyStatistics_56() { return &___LobbyStatistics_56; }
	inline void set_LobbyStatistics_56(List_1_t3471576424 * value)
	{
		___LobbyStatistics_56 = value;
		Il2CppCodeGenWriteBarrier(&___LobbyStatistics_56, value);
	}

	inline static int32_t get_offset_of_mGameList_57() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___mGameList_57)); }
	inline Dictionary_2_t1877450301 * get_mGameList_57() const { return ___mGameList_57; }
	inline Dictionary_2_t1877450301 ** get_address_of_mGameList_57() { return &___mGameList_57; }
	inline void set_mGameList_57(Dictionary_2_t1877450301 * value)
	{
		___mGameList_57 = value;
		Il2CppCodeGenWriteBarrier(&___mGameList_57, value);
	}

	inline static int32_t get_offset_of_mGameListCopy_58() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___mGameListCopy_58)); }
	inline RoomInfoU5BU5D_t3672612454* get_mGameListCopy_58() const { return ___mGameListCopy_58; }
	inline RoomInfoU5BU5D_t3672612454** get_address_of_mGameListCopy_58() { return &___mGameListCopy_58; }
	inline void set_mGameListCopy_58(RoomInfoU5BU5D_t3672612454* value)
	{
		___mGameListCopy_58 = value;
		Il2CppCodeGenWriteBarrier(&___mGameListCopy_58, value);
	}

	inline static int32_t get_offset_of_playername_59() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___playername_59)); }
	inline String_t* get_playername_59() const { return ___playername_59; }
	inline String_t** get_address_of_playername_59() { return &___playername_59; }
	inline void set_playername_59(String_t* value)
	{
		___playername_59 = value;
		Il2CppCodeGenWriteBarrier(&___playername_59, value);
	}

	inline static int32_t get_offset_of_mPlayernameHasToBeUpdated_60() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___mPlayernameHasToBeUpdated_60)); }
	inline bool get_mPlayernameHasToBeUpdated_60() const { return ___mPlayernameHasToBeUpdated_60; }
	inline bool* get_address_of_mPlayernameHasToBeUpdated_60() { return &___mPlayernameHasToBeUpdated_60; }
	inline void set_mPlayernameHasToBeUpdated_60(bool value)
	{
		___mPlayernameHasToBeUpdated_60 = value;
	}

	inline static int32_t get_offset_of_currentRoom_61() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___currentRoom_61)); }
	inline Room_t1042398373 * get_currentRoom_61() const { return ___currentRoom_61; }
	inline Room_t1042398373 ** get_address_of_currentRoom_61() { return &___currentRoom_61; }
	inline void set_currentRoom_61(Room_t1042398373 * value)
	{
		___currentRoom_61 = value;
		Il2CppCodeGenWriteBarrier(&___currentRoom_61, value);
	}

	inline static int32_t get_offset_of_U3CLocalPlayerU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CLocalPlayerU3Ek__BackingField_62)); }
	inline PhotonPlayer_t4120608827 * get_U3CLocalPlayerU3Ek__BackingField_62() const { return ___U3CLocalPlayerU3Ek__BackingField_62; }
	inline PhotonPlayer_t4120608827 ** get_address_of_U3CLocalPlayerU3Ek__BackingField_62() { return &___U3CLocalPlayerU3Ek__BackingField_62; }
	inline void set_U3CLocalPlayerU3Ek__BackingField_62(PhotonPlayer_t4120608827 * value)
	{
		___U3CLocalPlayerU3Ek__BackingField_62 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLocalPlayerU3Ek__BackingField_62, value);
	}

	inline static int32_t get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CPlayersOnMasterCountU3Ek__BackingField_63)); }
	inline int32_t get_U3CPlayersOnMasterCountU3Ek__BackingField_63() const { return ___U3CPlayersOnMasterCountU3Ek__BackingField_63; }
	inline int32_t* get_address_of_U3CPlayersOnMasterCountU3Ek__BackingField_63() { return &___U3CPlayersOnMasterCountU3Ek__BackingField_63; }
	inline void set_U3CPlayersOnMasterCountU3Ek__BackingField_63(int32_t value)
	{
		___U3CPlayersOnMasterCountU3Ek__BackingField_63 = value;
	}

	inline static int32_t get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CPlayersInRoomsCountU3Ek__BackingField_64)); }
	inline int32_t get_U3CPlayersInRoomsCountU3Ek__BackingField_64() const { return ___U3CPlayersInRoomsCountU3Ek__BackingField_64; }
	inline int32_t* get_address_of_U3CPlayersInRoomsCountU3Ek__BackingField_64() { return &___U3CPlayersInRoomsCountU3Ek__BackingField_64; }
	inline void set_U3CPlayersInRoomsCountU3Ek__BackingField_64(int32_t value)
	{
		___U3CPlayersInRoomsCountU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_U3CRoomsCountU3Ek__BackingField_65() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CRoomsCountU3Ek__BackingField_65)); }
	inline int32_t get_U3CRoomsCountU3Ek__BackingField_65() const { return ___U3CRoomsCountU3Ek__BackingField_65; }
	inline int32_t* get_address_of_U3CRoomsCountU3Ek__BackingField_65() { return &___U3CRoomsCountU3Ek__BackingField_65; }
	inline void set_U3CRoomsCountU3Ek__BackingField_65(int32_t value)
	{
		___U3CRoomsCountU3Ek__BackingField_65 = value;
	}

	inline static int32_t get_offset_of_lastJoinType_66() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___lastJoinType_66)); }
	inline int32_t get_lastJoinType_66() const { return ___lastJoinType_66; }
	inline int32_t* get_address_of_lastJoinType_66() { return &___lastJoinType_66; }
	inline void set_lastJoinType_66(int32_t value)
	{
		___lastJoinType_66 = value;
	}

	inline static int32_t get_offset_of_enterRoomParamsCache_67() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___enterRoomParamsCache_67)); }
	inline EnterRoomParams_t2222988781 * get_enterRoomParamsCache_67() const { return ___enterRoomParamsCache_67; }
	inline EnterRoomParams_t2222988781 ** get_address_of_enterRoomParamsCache_67() { return &___enterRoomParamsCache_67; }
	inline void set_enterRoomParamsCache_67(EnterRoomParams_t2222988781 * value)
	{
		___enterRoomParamsCache_67 = value;
		Il2CppCodeGenWriteBarrier(&___enterRoomParamsCache_67, value);
	}

	inline static int32_t get_offset_of_didAuthenticate_68() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___didAuthenticate_68)); }
	inline bool get_didAuthenticate_68() const { return ___didAuthenticate_68; }
	inline bool* get_address_of_didAuthenticate_68() { return &___didAuthenticate_68; }
	inline void set_didAuthenticate_68(bool value)
	{
		___didAuthenticate_68 = value;
	}

	inline static int32_t get_offset_of_friendListRequested_69() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___friendListRequested_69)); }
	inline StringU5BU5D_t1642385972* get_friendListRequested_69() const { return ___friendListRequested_69; }
	inline StringU5BU5D_t1642385972** get_address_of_friendListRequested_69() { return &___friendListRequested_69; }
	inline void set_friendListRequested_69(StringU5BU5D_t1642385972* value)
	{
		___friendListRequested_69 = value;
		Il2CppCodeGenWriteBarrier(&___friendListRequested_69, value);
	}

	inline static int32_t get_offset_of_friendListTimestamp_70() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___friendListTimestamp_70)); }
	inline int32_t get_friendListTimestamp_70() const { return ___friendListTimestamp_70; }
	inline int32_t* get_address_of_friendListTimestamp_70() { return &___friendListTimestamp_70; }
	inline void set_friendListTimestamp_70(int32_t value)
	{
		___friendListTimestamp_70 = value;
	}

	inline static int32_t get_offset_of_isFetchingFriendList_71() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___isFetchingFriendList_71)); }
	inline bool get_isFetchingFriendList_71() const { return ___isFetchingFriendList_71; }
	inline bool* get_address_of_isFetchingFriendList_71() { return &___isFetchingFriendList_71; }
	inline void set_isFetchingFriendList_71(bool value)
	{
		___isFetchingFriendList_71 = value;
	}

	inline static int32_t get_offset_of_U3CAvailableRegionsU3Ek__BackingField_72() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CAvailableRegionsU3Ek__BackingField_72)); }
	inline List_1_t990992504 * get_U3CAvailableRegionsU3Ek__BackingField_72() const { return ___U3CAvailableRegionsU3Ek__BackingField_72; }
	inline List_1_t990992504 ** get_address_of_U3CAvailableRegionsU3Ek__BackingField_72() { return &___U3CAvailableRegionsU3Ek__BackingField_72; }
	inline void set_U3CAvailableRegionsU3Ek__BackingField_72(List_1_t990992504 * value)
	{
		___U3CAvailableRegionsU3Ek__BackingField_72 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAvailableRegionsU3Ek__BackingField_72, value);
	}

	inline static int32_t get_offset_of_U3CCloudRegionU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___U3CCloudRegionU3Ek__BackingField_73)); }
	inline int32_t get_U3CCloudRegionU3Ek__BackingField_73() const { return ___U3CCloudRegionU3Ek__BackingField_73; }
	inline int32_t* get_address_of_U3CCloudRegionU3Ek__BackingField_73() { return &___U3CCloudRegionU3Ek__BackingField_73; }
	inline void set_U3CCloudRegionU3Ek__BackingField_73(int32_t value)
	{
		___U3CCloudRegionU3Ek__BackingField_73 = value;
	}

	inline static int32_t get_offset_of_mActors_74() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___mActors_74)); }
	inline Dictionary_2_t3128434462 * get_mActors_74() const { return ___mActors_74; }
	inline Dictionary_2_t3128434462 ** get_address_of_mActors_74() { return &___mActors_74; }
	inline void set_mActors_74(Dictionary_2_t3128434462 * value)
	{
		___mActors_74 = value;
		Il2CppCodeGenWriteBarrier(&___mActors_74, value);
	}

	inline static int32_t get_offset_of_mOtherPlayerListCopy_75() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___mOtherPlayerListCopy_75)); }
	inline PhotonPlayerU5BU5D_t818546810* get_mOtherPlayerListCopy_75() const { return ___mOtherPlayerListCopy_75; }
	inline PhotonPlayerU5BU5D_t818546810** get_address_of_mOtherPlayerListCopy_75() { return &___mOtherPlayerListCopy_75; }
	inline void set_mOtherPlayerListCopy_75(PhotonPlayerU5BU5D_t818546810* value)
	{
		___mOtherPlayerListCopy_75 = value;
		Il2CppCodeGenWriteBarrier(&___mOtherPlayerListCopy_75, value);
	}

	inline static int32_t get_offset_of_mPlayerListCopy_76() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___mPlayerListCopy_76)); }
	inline PhotonPlayerU5BU5D_t818546810* get_mPlayerListCopy_76() const { return ___mPlayerListCopy_76; }
	inline PhotonPlayerU5BU5D_t818546810** get_address_of_mPlayerListCopy_76() { return &___mPlayerListCopy_76; }
	inline void set_mPlayerListCopy_76(PhotonPlayerU5BU5D_t818546810* value)
	{
		___mPlayerListCopy_76 = value;
		Il2CppCodeGenWriteBarrier(&___mPlayerListCopy_76, value);
	}

	inline static int32_t get_offset_of_hasSwitchedMC_77() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___hasSwitchedMC_77)); }
	inline bool get_hasSwitchedMC_77() const { return ___hasSwitchedMC_77; }
	inline bool* get_address_of_hasSwitchedMC_77() { return &___hasSwitchedMC_77; }
	inline void set_hasSwitchedMC_77(bool value)
	{
		___hasSwitchedMC_77 = value;
	}

	inline static int32_t get_offset_of_allowedReceivingGroups_78() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___allowedReceivingGroups_78)); }
	inline HashSet_1_t2016565290 * get_allowedReceivingGroups_78() const { return ___allowedReceivingGroups_78; }
	inline HashSet_1_t2016565290 ** get_address_of_allowedReceivingGroups_78() { return &___allowedReceivingGroups_78; }
	inline void set_allowedReceivingGroups_78(HashSet_1_t2016565290 * value)
	{
		___allowedReceivingGroups_78 = value;
		Il2CppCodeGenWriteBarrier(&___allowedReceivingGroups_78, value);
	}

	inline static int32_t get_offset_of_blockSendingGroups_79() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___blockSendingGroups_79)); }
	inline HashSet_1_t2016565290 * get_blockSendingGroups_79() const { return ___blockSendingGroups_79; }
	inline HashSet_1_t2016565290 ** get_address_of_blockSendingGroups_79() { return &___blockSendingGroups_79; }
	inline void set_blockSendingGroups_79(HashSet_1_t2016565290 * value)
	{
		___blockSendingGroups_79 = value;
		Il2CppCodeGenWriteBarrier(&___blockSendingGroups_79, value);
	}

	inline static int32_t get_offset_of_photonViewList_80() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___photonViewList_80)); }
	inline Dictionary_2_t4202656512 * get_photonViewList_80() const { return ___photonViewList_80; }
	inline Dictionary_2_t4202656512 ** get_address_of_photonViewList_80() { return &___photonViewList_80; }
	inline void set_photonViewList_80(Dictionary_2_t4202656512 * value)
	{
		___photonViewList_80 = value;
		Il2CppCodeGenWriteBarrier(&___photonViewList_80, value);
	}

	inline static int32_t get_offset_of_readStream_81() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___readStream_81)); }
	inline PhotonStream_t2436786422 * get_readStream_81() const { return ___readStream_81; }
	inline PhotonStream_t2436786422 ** get_address_of_readStream_81() { return &___readStream_81; }
	inline void set_readStream_81(PhotonStream_t2436786422 * value)
	{
		___readStream_81 = value;
		Il2CppCodeGenWriteBarrier(&___readStream_81, value);
	}

	inline static int32_t get_offset_of_pStream_82() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___pStream_82)); }
	inline PhotonStream_t2436786422 * get_pStream_82() const { return ___pStream_82; }
	inline PhotonStream_t2436786422 ** get_address_of_pStream_82() { return &___pStream_82; }
	inline void set_pStream_82(PhotonStream_t2436786422 * value)
	{
		___pStream_82 = value;
		Il2CppCodeGenWriteBarrier(&___pStream_82, value);
	}

	inline static int32_t get_offset_of_dataPerGroupReliable_83() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___dataPerGroupReliable_83)); }
	inline Dictionary_2_t3230257 * get_dataPerGroupReliable_83() const { return ___dataPerGroupReliable_83; }
	inline Dictionary_2_t3230257 ** get_address_of_dataPerGroupReliable_83() { return &___dataPerGroupReliable_83; }
	inline void set_dataPerGroupReliable_83(Dictionary_2_t3230257 * value)
	{
		___dataPerGroupReliable_83 = value;
		Il2CppCodeGenWriteBarrier(&___dataPerGroupReliable_83, value);
	}

	inline static int32_t get_offset_of_dataPerGroupUnreliable_84() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___dataPerGroupUnreliable_84)); }
	inline Dictionary_2_t3230257 * get_dataPerGroupUnreliable_84() const { return ___dataPerGroupUnreliable_84; }
	inline Dictionary_2_t3230257 ** get_address_of_dataPerGroupUnreliable_84() { return &___dataPerGroupUnreliable_84; }
	inline void set_dataPerGroupUnreliable_84(Dictionary_2_t3230257 * value)
	{
		___dataPerGroupUnreliable_84 = value;
		Il2CppCodeGenWriteBarrier(&___dataPerGroupUnreliable_84, value);
	}

	inline static int32_t get_offset_of_currentLevelPrefix_85() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___currentLevelPrefix_85)); }
	inline int16_t get_currentLevelPrefix_85() const { return ___currentLevelPrefix_85; }
	inline int16_t* get_address_of_currentLevelPrefix_85() { return &___currentLevelPrefix_85; }
	inline void set_currentLevelPrefix_85(int16_t value)
	{
		___currentLevelPrefix_85 = value;
	}

	inline static int32_t get_offset_of_loadingLevelAndPausedNetwork_86() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___loadingLevelAndPausedNetwork_86)); }
	inline bool get_loadingLevelAndPausedNetwork_86() const { return ___loadingLevelAndPausedNetwork_86; }
	inline bool* get_address_of_loadingLevelAndPausedNetwork_86() { return &___loadingLevelAndPausedNetwork_86; }
	inline void set_loadingLevelAndPausedNetwork_86(bool value)
	{
		___loadingLevelAndPausedNetwork_86 = value;
	}

	inline static int32_t get_offset_of_ObjectPool_89() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___ObjectPool_89)); }
	inline Il2CppObject * get_ObjectPool_89() const { return ___ObjectPool_89; }
	inline Il2CppObject ** get_address_of_ObjectPool_89() { return &___ObjectPool_89; }
	inline void set_ObjectPool_89(Il2CppObject * value)
	{
		___ObjectPool_89 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectPool_89, value);
	}

	inline static int32_t get_offset_of_monoRPCMethodsCache_91() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___monoRPCMethodsCache_91)); }
	inline Dictionary_2_t342058070 * get_monoRPCMethodsCache_91() const { return ___monoRPCMethodsCache_91; }
	inline Dictionary_2_t342058070 ** get_address_of_monoRPCMethodsCache_91() { return &___monoRPCMethodsCache_91; }
	inline void set_monoRPCMethodsCache_91(Dictionary_2_t342058070 * value)
	{
		___monoRPCMethodsCache_91 = value;
		Il2CppCodeGenWriteBarrier(&___monoRPCMethodsCache_91, value);
	}

	inline static int32_t get_offset_of_rpcShortcuts_92() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___rpcShortcuts_92)); }
	inline Dictionary_2_t3986656710 * get_rpcShortcuts_92() const { return ___rpcShortcuts_92; }
	inline Dictionary_2_t3986656710 ** get_address_of_rpcShortcuts_92() { return &___rpcShortcuts_92; }
	inline void set_rpcShortcuts_92(Dictionary_2_t3986656710 * value)
	{
		___rpcShortcuts_92 = value;
		Il2CppCodeGenWriteBarrier(&___rpcShortcuts_92, value);
	}

	inline static int32_t get_offset_of_cachedServerAddress_94() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___cachedServerAddress_94)); }
	inline String_t* get_cachedServerAddress_94() const { return ___cachedServerAddress_94; }
	inline String_t** get_address_of_cachedServerAddress_94() { return &___cachedServerAddress_94; }
	inline void set_cachedServerAddress_94(String_t* value)
	{
		___cachedServerAddress_94 = value;
		Il2CppCodeGenWriteBarrier(&___cachedServerAddress_94, value);
	}

	inline static int32_t get_offset_of_cachedApplicationName_95() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___cachedApplicationName_95)); }
	inline String_t* get_cachedApplicationName_95() const { return ___cachedApplicationName_95; }
	inline String_t** get_address_of_cachedApplicationName_95() { return &___cachedApplicationName_95; }
	inline void set_cachedApplicationName_95(String_t* value)
	{
		___cachedApplicationName_95 = value;
		Il2CppCodeGenWriteBarrier(&___cachedApplicationName_95, value);
	}

	inline static int32_t get_offset_of_cachedProtocolType_96() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___cachedProtocolType_96)); }
	inline int32_t get_cachedProtocolType_96() const { return ___cachedProtocolType_96; }
	inline int32_t* get_address_of_cachedProtocolType_96() { return &___cachedProtocolType_96; }
	inline void set_cachedProtocolType_96(int32_t value)
	{
		___cachedProtocolType_96 = value;
	}

	inline static int32_t get_offset_of__isReconnecting_97() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ____isReconnecting_97)); }
	inline bool get__isReconnecting_97() const { return ____isReconnecting_97; }
	inline bool* get_address_of__isReconnecting_97() { return &____isReconnecting_97; }
	inline void set__isReconnecting_97(bool value)
	{
		____isReconnecting_97 = value;
	}

	inline static int32_t get_offset_of_tempInstantiationData_98() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___tempInstantiationData_98)); }
	inline Dictionary_2_t2622459769 * get_tempInstantiationData_98() const { return ___tempInstantiationData_98; }
	inline Dictionary_2_t2622459769 ** get_address_of_tempInstantiationData_98() { return &___tempInstantiationData_98; }
	inline void set_tempInstantiationData_98(Dictionary_2_t2622459769 * value)
	{
		___tempInstantiationData_98 = value;
		Il2CppCodeGenWriteBarrier(&___tempInstantiationData_98, value);
	}

	inline static int32_t get_offset_of_options_100() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222, ___options_100)); }
	inline RaiseEventOptions_t565739090 * get_options_100() const { return ___options_100; }
	inline RaiseEventOptions_t565739090 ** get_address_of_options_100() { return &___options_100; }
	inline void set_options_100(RaiseEventOptions_t565739090 * value)
	{
		___options_100 = value;
		Il2CppCodeGenWriteBarrier(&___options_100, value);
	}
};

struct NetworkingPeer_t3034011222_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> NetworkingPeer::ProtocolToNameServerPort
	Dictionary_2_t2531697585 * ___ProtocolToNameServerPort_48;
	// System.Boolean NetworkingPeer::UsePrefabCache
	bool ___UsePrefabCache_88;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> NetworkingPeer::PrefabCache
	Dictionary_2_t3671312409 * ___PrefabCache_90;
	// System.String NetworkingPeer::OnPhotonInstantiateString
	String_t* ___OnPhotonInstantiateString_93;
	// System.Int32 NetworkingPeer::ObjectsInOneUpdate
	int32_t ___ObjectsInOneUpdate_99;

public:
	inline static int32_t get_offset_of_ProtocolToNameServerPort_48() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222_StaticFields, ___ProtocolToNameServerPort_48)); }
	inline Dictionary_2_t2531697585 * get_ProtocolToNameServerPort_48() const { return ___ProtocolToNameServerPort_48; }
	inline Dictionary_2_t2531697585 ** get_address_of_ProtocolToNameServerPort_48() { return &___ProtocolToNameServerPort_48; }
	inline void set_ProtocolToNameServerPort_48(Dictionary_2_t2531697585 * value)
	{
		___ProtocolToNameServerPort_48 = value;
		Il2CppCodeGenWriteBarrier(&___ProtocolToNameServerPort_48, value);
	}

	inline static int32_t get_offset_of_UsePrefabCache_88() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222_StaticFields, ___UsePrefabCache_88)); }
	inline bool get_UsePrefabCache_88() const { return ___UsePrefabCache_88; }
	inline bool* get_address_of_UsePrefabCache_88() { return &___UsePrefabCache_88; }
	inline void set_UsePrefabCache_88(bool value)
	{
		___UsePrefabCache_88 = value;
	}

	inline static int32_t get_offset_of_PrefabCache_90() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222_StaticFields, ___PrefabCache_90)); }
	inline Dictionary_2_t3671312409 * get_PrefabCache_90() const { return ___PrefabCache_90; }
	inline Dictionary_2_t3671312409 ** get_address_of_PrefabCache_90() { return &___PrefabCache_90; }
	inline void set_PrefabCache_90(Dictionary_2_t3671312409 * value)
	{
		___PrefabCache_90 = value;
		Il2CppCodeGenWriteBarrier(&___PrefabCache_90, value);
	}

	inline static int32_t get_offset_of_OnPhotonInstantiateString_93() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222_StaticFields, ___OnPhotonInstantiateString_93)); }
	inline String_t* get_OnPhotonInstantiateString_93() const { return ___OnPhotonInstantiateString_93; }
	inline String_t** get_address_of_OnPhotonInstantiateString_93() { return &___OnPhotonInstantiateString_93; }
	inline void set_OnPhotonInstantiateString_93(String_t* value)
	{
		___OnPhotonInstantiateString_93 = value;
		Il2CppCodeGenWriteBarrier(&___OnPhotonInstantiateString_93, value);
	}

	inline static int32_t get_offset_of_ObjectsInOneUpdate_99() { return static_cast<int32_t>(offsetof(NetworkingPeer_t3034011222_StaticFields, ___ObjectsInOneUpdate_99)); }
	inline int32_t get_ObjectsInOneUpdate_99() const { return ___ObjectsInOneUpdate_99; }
	inline int32_t* get_address_of_ObjectsInOneUpdate_99() { return &___ObjectsInOneUpdate_99; }
	inline void set_ObjectsInOneUpdate_99(int32_t value)
	{
		___ObjectsInOneUpdate_99 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
