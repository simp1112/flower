﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass40_1
struct U3CU3Ec__DisplayClass40_1_t1369263270;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t1369263269  : public Il2CppObject
{
public:
	// System.Byte[] ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass40_0::inBufferCopy
	ByteU5BU5D_t3397334013* ___inBufferCopy_0;
	// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass40_1 ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass40_0::CS$<>8__locals1
	U3CU3Ec__DisplayClass40_1_t1369263270 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_inBufferCopy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t1369263269, ___inBufferCopy_0)); }
	inline ByteU5BU5D_t3397334013* get_inBufferCopy_0() const { return ___inBufferCopy_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_inBufferCopy_0() { return &___inBufferCopy_0; }
	inline void set_inBufferCopy_0(ByteU5BU5D_t3397334013* value)
	{
		___inBufferCopy_0 = value;
		Il2CppCodeGenWriteBarrier(&___inBufferCopy_0, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t1369263269, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass40_1_t1369263270 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass40_1_t1369263270 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass40_1_t1369263270 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E8__locals1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
